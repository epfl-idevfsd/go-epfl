# Contributing


♥ We love pull requests from everyone !


When contributing to this repository, please first discuss the change you wish
to make via issue, email, or any other method with the owners of this repository
before making a change. 


## So all code changes happen through Merge Requests

Merge requests are the best way to propose changes to the codebase. We actively
welcome your pull requests:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. If you've added code that need documentation, update the documentation.
4. Be sure to test your modifications.
5. Write a [good commit message](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).
6. Be sure to change the application version if needed.
7. Issue that merge request!


## Report bugs

We use Gitlab issues to track public bugs. Report a bug by [opening a new
issue](https://gitlab.com/epfl-isasfsd/go-epfl/-/issues/new). Please use the 
"Bug" template.


## Feature request

As for bugs reports, feature request are also handled by [opening a new
issue](https://gitlab.com/epfl-isasfsd/go-epfl/-/issues/new) on Gitlab. Please 
use the "Feature Request" template.
