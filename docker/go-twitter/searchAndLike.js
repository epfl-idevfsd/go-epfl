const fs = require('fs')

// https://nodejs.org/api/querystring.html
const querystring = require('querystring')

// https://github.com/FeedHive/twitter-api-client
const { TwitterClient } = require('twitter-api-client')
const twitterClient = new TwitterClient({
  apiKey: process.env.GOEPFL_TWBOT_API_KEY,
  apiSecret: process.env.GOEPFL_TWBOT_API_SECRET,
  accessToken: process.env.GOEPFL_TWBOT_ACCESS_TOKEN,
  accessTokenSecret: process.env.GOEPFL_TWBOT_TOKEN_SECRET,
  //disableCache: true,
})

// All credit to Matt H. via https://stackoverflow.com/a/20392392/960623
const tryParseJSON = (jsonString) => {
  try {
      var o = JSON.parse(jsonString)

      // Handle non-exception-throwing cases:
      // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
      // but... JSON.parse(null) returns null, and typeof null === "object", 
      // so we must check for that, too. Thankfully, null is falsey, so this suffices:
      if (o && typeof o === "object") {
          return o
      }
  }
  catch (e) { }

  return false
}

const goSearch = async (q = 'go.epfl.ch', result_type = 'recent', count = 15, max_id, since_id) => {
  // https://github.com/FeedHive/twitter-api-client/blob/master/REFERENCES.md#twitterclienttweetssearchparameters
  return await twitterClient.tweets.search({
    q,
    result_type,
    count,
    max_id,
    since_id,
  })
}

// in order to avoid to query same tweets again and again, we store the highest
// tweet id as stating point for the next run.
const getLastId = (lastId = 0, log) => {
  let cached
  try {
    //cached = require('./cache.json').lastId
    const cache = fs.readFileSync('./cache.json', { encoding:'utf8', flag:'r' })
    cached = JSON.parse(cache).lastId
  } catch (e) {
    cached = 0
  }
  // console.log(`cache ${cached} vs last ${lastId}`)
  if (lastId > cached || cached == 0) {
    let data = JSON.stringify({ lastId }, null, 2)
    fs.writeFile('./cache.json', data, function (err) {
      if (err) throw err
      log.debug(`./cache.json saved with new lastId = ${lastId}`)
    })
    return lastId
  } else {
    return cached
  }
}

const searchAndLikeTweets = async (log) => {
  let max_id
  let since_id = getLastId(0, log)
  log.verbose(`Starting since_id: ${since_id}`)
  do {
    data = await goSearch('go.epfl.ch', 'recent', 15, max_id, since_id)
    // update the max_id to fetch next page if needed
    if (data.search_metadata.next_results) {
      max_id = querystring.parse(data.search_metadata.next_results.substring(1)).max_id
    }

    if (!data.statuses.length) {
      log.verbose(`  ↳ no data`)
    } else {
      for (let tweet of data.statuses) {
        // cache the highest Tweet ID, for the next run
        getLastId(tweet.id_str, log)
        // console.log(tweet)
        try {
          let like = await twitterClient.tweets.favoritesCreate({
            id: tweet.id_str,
          })
          // in case of retweet, we still like the original tweet...
          if (like.retweeted_status.text) {
            log.warn(`→ https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`)
            log.warn(` ❤️  ${like.retweeted_status.text}`)
          } else if (like.text) {
            log.warn(`→ https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`)
            log.warn(` ❤️  ${like.text}`)
          } else {
            log.error(`A weird case that some has to take care of...:\n${tweet}\n${like}`)
          }
          log.verbose('Liking %O:', like)
        } catch (e) {
          if (e.statusCode && e.statusCode == '429') {
            log.error(` ✖ [${e.statusCode}] Too Many Requests, please cool down...`)
          } else {
            let error = tryParseJSON(e)
            if (error) {
              if (error.statusCode && error.errors[0] && error.errors[0].message) {
                if (e.statusCode && e.statusCode == '403' && error.errors[0].code == '139') {
                  // You have already favorited this status.
                  log.warn(`→ https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`)
                  log.warn(` ⤳ You have already favorited this status`)
                } else {
                  log.error(` ✖ [${error.statusCode}] ${error.errors[0].message}`)
                }
              } else if (error.statusCode) {
                log.error(` ✖ [${e.statusCode}]`)
              }
            } else {
              log.verbose('Error e: %O', e)
            }
          }
        }
      }
    }
  } while (data.search_metadata.next_results)
}

exports.searchAndLikeTweets = searchAndLikeTweets
