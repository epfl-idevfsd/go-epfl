// https://www.npmjs.com/package/node-cron
const cron = require('node-cron')

const fs = require('fs')

// https://github.com/winstonjs/winston
const { createLogger, format, transports } = require('winston')
const { combine, timestamp, printf } = format
const DailyRotateFile = require('winston-daily-rotate-file')
const logFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} [${level}] ${message}`
})
const logPath = './logs'
const logSuffix = '_goepflbot.log'
// error: 0, warn: 1, info: 2, http: 3, verbose: 4, debug: 5, silly: 6
const loglevel = process.env.NODE_ENV == 'production' ? 'info' : 'debug'
const logFileToKeep = 7

const log = createLogger({
  level: loglevel,
  format: combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.splat(),
    logFormat
  ),
  transports: [
    new transports.Console({
      format: combine(
        logFormat,
        format.colorize({
          all: true,
        })
      ),
    }),
    new transports.DailyRotateFile({
      filename: `%DATE%${logSuffix}`,
      dirname: `./${logPath}`,
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '5m',
      maxFiles: logFileToKeep,
    }),
  ],
})

const searchAndLikeTweets = require('./searchAndLike.js').searchAndLikeTweets

if (
  !process.env.GOEPFL_TWBOT_API_KEY ||
  !process.env.GOEPFL_TWBOT_API_SECRET ||
  !process.env.GOEPFL_TWBOT_ACCESS_TOKEN ||
  !process.env.GOEPFL_TWBOT_TOKEN_SECRET
) {
  log.error(`Please check your GOEPFL_TWBOT_* env variables`)
  process.exit(1)
}

const start = () => {
  log.info(`⇆ New cron scheduled run...`)
  searchAndLikeTweets(log)
}

const run_with_cron = true
if (run_with_cron) {
  // At minute 17 and 42 past every hour from 6 through 22.
  cron.schedule('17,42 6-22 * * *', () => {
    start()
  })
} else {
  start()
}
