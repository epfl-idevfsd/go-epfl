import mysql.connector
from mysql.connector import MySQLConnection, Error

class MySQLCursorDict(mysql.connector.cursor.MySQLCursor):
  def fetchone(self):
    row = self._fetch_row()
    if row:
      return dict(zip(self.column_names, self._row_to_python(row)))
    return None

class MySqlDB():
    def __init__(self, config):
        self.conn   = MySQLConnection(**config)
        self.cursor = self.conn.cursor()

    def query(self, query):
        self.cursor.execute(query)

    def close(self):
        self.cursor.close()
        self.conn.close()

    def commit(self):
        self.conn.commit()

    def fetchmany(self, nb_rows):
        return self.cursor.fetchmany(nb_rows)

    @property
    def columns(self):
        return tuple( [d[0] for d in self.cursor.description] )

    def fetchall(self):
        result = [dict(zip(self.columns, row)) for row in self.cursor.fetchall()]
        return result

    def fetchone(self):
        row = self.cursor.fetchone()
        if row is None:
            return None
        else:
            return dict(zip(self.columns, row))

if __name__ == '__main__':
    config = {
      'user': 'user',
      'password': 'password',
      'host': '127.0.0.1',
      'database': 'go',
      'raise_on_warnings': True
    }
    db = MySqlDB(config)
    db.query("SELECT * FROM phurl_urls;")
    result = db.fetchone()
    db.close()
    result != None
