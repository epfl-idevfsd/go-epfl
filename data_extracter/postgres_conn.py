import psycopg2

class PostgresDB():
    def __init__(self, config):
        self.conn   = psycopg2.connect(**config)
        self.cursor =  self.conn.cursor()

    def query(self, query, values=None):
        self.cursor.execute(query, values)

    def close(self):
        self.cursor.close()
        self.conn.close()

    def commit(self):
        self.conn.commit()

    def fetchmany(self, nb_rows):
        return self.cursor.fetchmany(nb_rows)

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def insert_commit(self, query, values):
        self.insert(query, values)
        self.commit()
        return self.cursor.fetchone()[0]

    def insert(self, query, values):
        self.query(query + ' returning id', values)

    def commit(self):
        self.conn.commit()

    def lastinsertid(self):
        return self.cursor.lastrowid

if __name__ == '__main__':
    db = PostgresDB()
    db.query("SELECT * FROM aliases;")
    result = db.fetchone()
    db.close()
    result != None

