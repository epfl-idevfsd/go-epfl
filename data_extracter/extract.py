import sys

class Extract:
    def urls(self, db):
        db.query("SELECT * FROM phurl_urls")
        urls = db.fetchall()
        return self.prepare_aliases(urls)

    def logs(self, db):
        db.query("SELECT * FROM phurl_log")
        while True:
            next_row = db.fetchone()
            if next_row is None:
                return
            else:
                yield self.prepare_log(next_row)

    def prepare_aliases(self, urls):
        aliases = []
        for url in urls:
            alias = {}
            alias['alias'] = url['alias'].decode("utf-8")
            alias['code'] = url['code'].decode("utf-8")
            alias['only_code'] = (False, True)[alias['alias'] == '']
            alias['created_at'] = str(url['date_added'])
            alias['url'] = url['url'].decode("utf-8")
            alias['hidden'] = url['private']
            aliases.append(alias)
        return aliases

    def prepare_logs(self, logs):
        new_logs = []
        for log in logs:
            new_logs.append(self.prepare_log(log))
        return new_logs

    def prepare_log(self, log):
            new_log = {}
            new_log['click_time'] = log['click_time']
            new_log['referrer'] = log['referrer']
            new_log['user_agent'] = log['user_agent']
            new_log['ip_address'] = log['ip_address']
            new_log['iso_code'] = log['country_code']
            new_log['alias'] = log['shorturl']
            return new_log

    def url_id(self, db, url):
        query = "SELECT id FROM urls WHERE url=%s"
        db.query(query, [url])
        id = db.fetchone()
        if id == None: return None
        return ''.join(map(str, id))

    def alias_details(self, db, alias):
        if not hasattr(self, "_aliases"):
            query = "SELECT a.alias, a.id, u.url FROM aliases a LEFT JOIN urls u ON a.url_id = u.id"
            db.query(query, [alias])
            self._aliases = { line[0]:[line[1], line[2]] for line in db.fetchall()}
        return self._aliases.get(alias, None)

    def code_details(self, db, code):
        query = "SELECT a.id, u.url FROM aliases a LEFT JOIN urls u ON a.url_id = u.id WHERE code=%s"
        db.query(query, [code])
        return db.fetchone()

    def last_urls(self, db, last_date):
        query = "SELECT * FROM phurl_urls WHERE date_added > \"" + last_date + "\""
        db.query(query)
        urls = db.fetchall()
        return self.prepare_aliases(urls)

    def last_logs(self, db, last_date):
        query = "SELECT * FROM phurl_log WHERE click_time > "
        date = "\"" + last_date.rsplit('+')[0] + "\""
        db.query(query + date)
        while True:
            next_row = db.fetchone()
            if next_row is None:
                return
            else:
                yield self.prepare_log(next_row)

    def last_log_date(self, db):
        db.query("SELECT click_time FROM logs ORDER BY click_time DESC LIMIT 1")
        return str(db.fetchone()[0])

    def last_alias_date(self, db):
        db.query("SELECT created_at FROM aliases ORDER BY created_at DESC LIMIT 1")
        return db.fetchone()
