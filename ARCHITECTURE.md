<!--
  Checkout out https://matklad.github.io/2021/02/06/ARCHITECTURE.md.html
-->

# Architecture

## Tech Stack

Go EPFL uses the [Laravel] framework and a [PostgreSQL] database. Everything is 
ran in [Docker] containers and exposed with [Traefik]. Observability and 
monitoring are backed by [Prometheus].


## 🦅 Bird's eye view

![go.epfl.ch's architecture diagram](./docs/architecture/goepfl.png)


### ① Traefik

All external HTTP requests are handled by [Traefik], which also takes care of
redirecting http (:80) to https (:443). [Traefik] serves the SSL certificat 
provided by [Let's encrypt].


### ② Main app

The main application is divided into 4 containers.

  * The webserver, [NGINX], handle the requests and serve files. If the request
    call a PHP file, it proxy it to [PHP-FPM].
  * [PHP-FPM] receives, interpretes, renders and returns the PHP related requests.
  * URLs, alias, users and logs are stored in a [PostgreSQL] database.
  * The workspace is a utility container that is connected to the 3 others. It
    has tools as [Artisan], comfort cli (ping, netstat, etc.) and is used to 
    run commands on the application ([just]).


### ③ Monitoring

All metrics are collected by [Prometheus]. The following exporters are used:

  * https://github.com/nginxinc/nginx-prometheus-exporter
  * https://github.com/prometheus-community/postgres_exporter
  * https://github.com/prometheus/node_exporter
  * https://github.com/google/cadvisor

In addition, [Traefik] and [Prometheus] have their own metrics that can be used
within Prometheus too.

Note: [Prometheus] is used by Camptocamp's alertmanager to send outage to EPFL's
ServiceNow instance.


### ④ Others

[goepflbot] is a Twitter bot that likes every tweets that contain a 
[go.epfl.ch](https://go.epfl.ch)'s link. 



[EPFL]: https://www.epfl.ch/
[Laravel]: https://laravel.com
[Artisan]: https://laravel.com/docs/8.x/artisan
[PostgreSQL]: https://www.postgresql.org/
[Docker]: https://docker.com
[Traefik]: https://traefik.io
[Let's encrypt]: https://letsencrypt.org
[Prometheus]: https://prometheus.io/
[NGINX]: https://www.nginx.com/
[PHP-FPM]: https://www.php.net/manual/en/install.fpm.php
[just]: https://github.com/casey/just
[goepflbot]: https://twitter.com/goepflbot
