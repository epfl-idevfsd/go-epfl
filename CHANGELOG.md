# CHANGELOG
<!-- git log --pretty="%aI %h - %s (%an)"  -->

## 1.9.0 (2025-01-16)

* Poor's man anti-spam in https://go.epfl.ch/contact update with new EPFL's
  President
* New magic redirects for ServiceNow's TASK and PRJ. Also WPNxxx (ISAS-FSD's
  Jira)
* Introduce the `https://go.epfl.ch/me` magic link, that redirect to your EPFL's
  people page (if logged in)
* More explicit "publishing": `hidden` or on the browse page, RSS feed and
  Telegram channel. (Note that was pushed on 2023-11-17 but never released
  because of "freeze" 🥶)

### Details

6d6cc5d - [fix] Less spam? (Nicolas Borboën)
43e9f76 - [fix] 2025's President (Nicolas Borboën)
0ebb44c - [bump] v1.9.0 (Nicolas Borboën)
fc31116 - [feature] Magic redirect for WPN (Nicolas Borboën)
eac9844 - [feature] Magic redirect for lexes (Nicolas Borboën)
4ae4d57 - [feature] Magic redirect for TASK.... (Nicolas Borboën)
a34db5f - [feature] more explicit publishing explanation (Nicolas Borboën)
dec3ee4 - [feature] https://go.epfl.ch/me (Nicolas Borboën)
3441ae5 - [feature] Magic redirect for PRJxxx (Nicolas Borboën)


## 1.8.0 (2023-07-06)

* Magic redirects for Moodle courses and ServiceNow's CIM
* Mainly maintenance stuff

### Details

* fedb9f3 - [doc] FAQ entry for moodle magic redirects (Nicolas Borboën)
* c171c77 - [feature](INC0549889) Moodle magic redirect (Nicolas Borboën)
* 990df4f - [feature] Moodle magic redicect (Nicolas Borboën)
* b805688 - [feature] magic-redirect for CIM (Continuous IMprovement) (Nicolas Borboën)
* cde4b3b - [fix] GitLab magic issue mail (Nicolas Borboën)
* eb64802 - [fix] github page URL `epfl-idevelop` to `epfl-si` (Nicolas Borboën)
* a49e369 - [fix] git url `idevfsd` to `isasfsd` (Nicolas Borboën)
* 515b222 - [doc] CHANGELOG for v1.7.0 (Nicolas Borboën)
* cf7bb92 - [fix] React router LICENSES (Nicolas Borboën)
* 04b5f97 - [fix] QR code view (follow-up e1e8378) (Nicolas Borboën)



## 1.7.0 (2022-12-07)

* Introduce Service Now magic redirects, for INCIDENT (INCxxxxxx), CHANGE 
  (CHGxxxxxx), CTASK, OUTAGE (OUTxxxxxx), KnowledgeBase (KBxxxxxx), RITM, IDEA, 
  PRB, SVC, RSK and AUD.
* The API endpoint `aliases` now accept params for sorting the results.
* Any alias's owner can now add other owners. For now, only Go's admin can
  delete an owner.
* The whole metrics and monitoring have been updated.
* QR code for Go's URL are now configurable (https://go.epfl.ch/qr/{alias})
* Backoffice has now a loader and has been update to [React Router](https://reactrouter.com) v6

### Details

  * 326d50d - [fix] Missing props (SaphireVert)
  * eb5bbdb - [feature] Interface improvement (SaphireVert)
  * eef3d6f - [feature] Bump to react-router v6 (SaphireVert)
  * f5316ed - [kindoffix] because service now :( (Nicolas Borboën)
  * b920e9c - [feature] magic URL /bKB000000 (ponsfrilus)
  * d0b35db - [fix] typo (SaphireVert)
  * d17ee36 - [feature] loading spinner (SaphireVert)
  * 121415c - [feature] add admin SaphireVert in seeder (SaphireVert)
  * 71dc23d - [fix] admin button (SaphireVert)
  * 14dc620 - [feature] magic redirect for audits (Nicolas Borboën)
  * 400c16a - [feature] magic redirect for risks (Nicolas Borboën)
  * 6d6c7b3 - [feature] magic redirect for services (Nicolas Borboën)
  * c0b926b - [ops] sync (Nicolas Borboën)
  * 114fd08 - [git] ignore ops executables (Nicolas Borboën)
  * 907abd8 - [fix] avoid form with same names (Nicolas Borboën)
  * cf9c9f9 - [feature] add "From API" info in email (Nicolas Borboën)
  * c90530c - [fix] phpinfo() (Nicolas Borboën)
  * 1af567d - [fix] s/idev-fsd/go (Nicolas Borboën)
  * 6f480f7 - [hotfix] Outage uses `u_number`... (Nicolas Borboën)
  * 81b44d2 - [feat] QR image hotlink (Azecko)
  * 9169503 - [feat] QR code customizer (Azecko)
  * 24c5f05 - [feat] QRCode helper (Azecko)
  * 58e0813 - [fix] /prometheus/ redirection (Nicolas Borboën)
  * da7a933 - [fix] do not expose unnecessary ports (Nicolas Borboën)
  * 36b6330 - [fix] containers's names (Nicolas Borboën)
  * 798656c - [fix] monitoring (nginx and php_fpm) (Nicolas Borboën)
  * 14e0d49 - [tmp] only dev is needed, files to be removed (Nicolas Borboën)
  * ae60792 - [fix] hidden icon (Nicolas Borboën)
  * 1fa8158 - [seed] Add a very long URL (Nicolas Borboën)
  * 5937171 - [fix] Permissions seeder (Nicolas Borboën)
  * 29ba468 - [fix] Restcountries API url (Sami Panseri)
  * 6a21ed6 - [feature] Upgrade to Traefik 2.5 (Nicolas Borboën)
  * fc5a192 - [fix] Aliases' rules (Nicolas Borboën)
  * ddd815f - [doc] magic-redirect (Nicolas Borboën)
  * c32ab7d - [chore] refactoring and beautifullage (Nicolas Borboën)
  * 97783f3 - [feat] nagic-redirect for CTASK, RITM, IDEA and PRB (Nicolas Borboën)
  * 1af3e91 - [doc] magic-redirect (Nicolas Borboën)
  * 4995208 - [feat] magic-redirect for OUT and KB (Nicolas Borboën)
  * 2816eaf - [fix] php-fpm-status (Nicolas Borboën)
  * 0bf6e45 - [fix] exporter names (Nicolas Borboën)
  * 1b1add9 - [add] Alias owners and `add owners` button (saphirevert)
  * 79a4cb4 - [doc] API/getAliases sort (Nicolas Borboën)
  * 387deb0 - [feature] API/getAliases sort (Nicolas Borboën)
  * 939dc30 - [feature] CHGxxxxxx auto-redirect (Nicolas Borboën)
  * 21607c3 - [fix] Enforce INCxxxxxx aliases (Nicolas Borboën)
  * fa85e89 - [feature] Allow to redirect EPFL's incident (Nicolas Borboën)


## v1.6.5 (2021-08-19)

* This version has been deployed automatically with [Ansible](https://www.ansible.com/) — see the [go-ops](https://gitlab.com/epfl-idevfsd/go-ops) repository.
* Fancy new logos (and favicon)! Check out this [design](https://go.epfl.ch/design) page.
* [humans.txt](https://go.epfl.ch/humans.txt) and [is it any good?](https://gitlab.com/epfl-idevfsd/go-epfl/-/tree/master#is-it-any-good).
* Work on aliases management.
* PostgreSQL performances optimization and tunning.

### Details

  * 1efb766 - [fix] Stop assuming the group name is the same as the user name (Dominique Quatravaux)
  * f55fd9d - [fix] There is already a group 33 in the upstream image (Dominique Quatravaux)
  * 3fbdf5d - [fix] There is already a www-data user + group in the upstream image (Dominique Quatravaux)
  * 63d1bb0 - Merge branch 'feature/66-delete_button' into 'master' (SaphireVert)
  * ff6b6f0 - [add] delete alias buttons in backoffice and user profile (saphirevert)
  * b2e7e70 - [fix] go-twitter name in package-lock.json (Nicolas Borboën)
  * 6e47a8a - [doc] Logo in FAQ (Nicolas Borboën)
  * 70f1b9e - [doc] typo (Nicolas Borboën)
  * 18487df - [doc] is it any good ? (Nicolas Borboën)
  * 7af98f3 - Merge branch 'feature/tune-postgres' into 'master' (ponsfrilus)
  * aeccdb7 - Merge branch 'feature/new-logo' into 'master' (ponsfrilus)
  * 300f007 - [refactore] /logo becomes /design (Nicolas Borboën)
  * 117fed6 - [add] remplacement of old favicon (Nicolas Borboën)
  * 478604a - [add] favicon (https://realfavicongenerator.net/) (Nicolas Borboën)
  * 56a21c6 - [add] § about the logo (Nicolas Borboën)
  * 43e5ea9 - [add] logo in the footer (Nicolas Borboën)
  * 42ebe60 - [add] log on homepage (guest only) (Nicolas Borboën)
  * 9c59d4d - [add] /logo page (Nicolas Borboën)
  * 1bf43b7 - [add] Logos (Nicolas Borboën)
  * b82690b - [tuning] PostgreSQL (Nicolas Borboën)
  * 986a544 - [doc] Is it any good? Yes. (Nicolas Borboën)
  * 4d83270 - [refactor] view `v_aliases` rewritten without sub-queries (Nicolas Borboën)
  * 09cd1a9 - [add] IMMUTABLE in truncateURL SQL function (Nicolas Borboën)
  * 53a62df - [add] indexes on aliases (Nicolas Borboën)
  * 02735af - [fix] Add `DROP x IF EXISTS` in migrations (Nicolas Borboën)
  * 4f725e8 - [doc] Is it any good? Yes. (Nicolas Borboën)
  * fe3ca3f - Merge branch 'add/add_zf_for_admin' into 'master' (ponsfrilus)
  * aaa54b6 - [add] czufferey as admin (Nicolas Borboën)
  * 458ffd3 - Merge branch 'feature/seo' into 'master' (ponsfrilus)
  * a66ea40 - [feature] Some SEO and information (ponsfrilus)
  * c5506db - Merge branch 'refactor/seeder-scripts' into 'master' (ponsfrilus)
  * 7989caf - Merge branch 'upgrade/php-and-nvm' into 'master' (ponsfrilus)
  * 01cf690 - [ocd] (Nicolas Borboën)
  * 4e5cbf4 - [upgrade] nvm (Nicolas Borboën)
  * 88cf60f - [fix] create the data cache directory (Nicolas Borboën)
  * 2e3c528 - Merge branch 'doc/fix-dev-steps' into 'master' (ponsfrilus)
  * 730f314 - [refactor] Seeders now use Models (Nicolas Borboën)
  * 66b920d - [fix] go-app/.env (Nicolas Borboën)
  * 6f253af - [upgrade] to PHP 7.4 (Nicolas Borboën)
  * 9f67132 - [fix] URL in Telegram message (Nicolas Borboën)
  * 9662c49 - [hotfix] do not publish træfik (Nicolas Borboën)
  * ed18d8e - [fix] run  at startup (Nicolas Borboën)
  * 00bc45b - [fix] do not expose cadvisor (Nicolas Borboën)
  * 4959123 - [todo] fix just fix-perms (Nicolas Borboën)
  * 8248339 - Merge branch 'feature/go-twitter' into 'master' (ponsfrilus)
  * 1b1ba4f - [feature] add go_twitter_bot to docker-compose (Nicolas Borboën)
  * ed045b9 - [feature] go-epfl twitter bot (Nicolas Borboën)
  * eb3596d - Merge branch 'doc/about-thanks' into 'master' (ponsfrilus)
  * 598eaec - Merge branch 'doc/update-tg-channel' into 'master' (ponsfrilus)
  * e37504b - [thanks] @GregLeBarbar (Nicolas Borboën)
  * b5156bb - [feature] add TG channel info (Nicolas Borboën)
  * b400980 - [doc] CHANGELOG v1.6.0 (Nicolas Borboën)
  * 382c553 - Merge branch 'feature/final-try-react' into 'master' (ponsfrilus)
  * 4aa8fd5 - Merge branch 'janitor/update-nginx-prometheus-exporter' into 'master' (ponsfrilus)
  * f3556c7 - [update] prometheus-nginx-exporter update (Nicolas Borboën)
  * dca04d8 - Merge branch 'master' into feature/final-try-react (Nicolas Borboën)
  * cb65ff0 - [fix] Hidden checkbox in admin alias edition (Nicolas Borboën)
  * 5d2416e - [feature] add "no-flags" info + formatting (Nicolas Borboën)
  * 0f6325d - [feature] complete users list (Nicolas Borboën)
  * db98202 - [feature] filter HTTP status code from API (Nicolas Borboën)
  * bfa99f1 - [feature] add admin's home link (Nicolas Borboën)
  * 25b2ce7 - [feature] filter alias without owners (Nicolas Borboën)
  * ca62e7f - [feature] filter on aliases' owner (Nicolas Borboën)
  * 1573b2a - [css] Update svg icon (Nicolas Borboën)
  * 11b7d4d - [fix] full list of aliases when admin (hidden) (Nicolas Borboën)
  * 933c63a - [ocd] (Nicolas Borboën)
  * c1324fd - [feature] filter on «status» query string (Nicolas Borboën)
  * d061ae5 - [fix] allow more path in admin route for react (Nicolas Borboën)
  * a43a1e0 - [feature] split admin pages (Nicolas Borboën)
  * 3860825 - Merge branch 'feature/final-try-react' of gitlab.com:epfl-idevfsd/go-epfl into feature/final-try-react (Grégory Charmier)
  * caeb972 - [fix] check if token is null (Grégory Charmier)
  * 2a841e9 - [update] Update npm packages (Nicolas Borboën)
  * ec5efe0 - [feature] Laravel Admin is accessible to /admin-php (Grégory Charmier)
  * 1dd7dfe - [clean] delete unused method (Grégory Charmier)
  * 086d7ea - Merge branch 'master' into feature/final-try-react (Grégory Charmier)
  * 67b8e14 - Merge branch 'fix/58-tootltip-popover' into 'master' (GregLeBarbar)
  * 0cd16da - Merge branch 'feature/76-hits-details-in-API' into 'master' (GregLeBarbar)
  * 1ade308 - [clean] Delete an unused file (Grégory Charmier)
  * 42c16a2 - [config] Prettier to format js files (Grégory Charmier)
  * 9056243 - [feature] Admin REST API improved (Grégory Charmier)
  * 23fe907 - [feature] Admin backend reactified (Grégory Charmier)
  * b51d097 - [fix] popover (Nicolas Borboën)
  * 4538097 - [feature] add CSV exporter (Nicolas Borboën)
  * aa1dca1 - [fix] add alias_id and go_url in LogResource (Nicolas Borboën)
  * 5f643c5 - [refactor] Alias details in API (Nicolas Borboën)
  * 29382bf - [refactor] API's clicks endpoint (Nicolas Borboën)
  * 2772904 - [FT.] Add logs details in API (getClicks) (Nicolas Borboën)
  * 81db16c - [HotFix] Truncated URL in profile (Nicolas Borboën)
  * 9ede28b - [config] Define tabulations number for php and js (Grégory Charmier)
  * bc46567 - [feature] Several improvements to the Rest API (Grégory Charmier)
  * d54d474 - [feature] Several improvements to the admin area in React (Grégory Charmier)
  * 3b14bb4 - [fix] Fix postgres version (Grégory Charmier)


## v1.6.0 (2021-04-19)

### React
Mainly, this version brings [React](https://reactjs.org) to the backoffice.
The merge request (!23) of the branch 'feature/final-try-react' details all the
work.

Now the backoffice is 85% « Reactified », and is using new API endpoints
that have been developed for this usage (e.g. `/users`, `/admins`,
`/blacklist-items`).
The `/alias` endpoint have been improved to provide information about `/flags` (
`report` and `reliability`) and more information about alias's owners.

While the React backend is the new default, the «old» Laravel (PHP) backend is 
still accessible.

### Security
Early this month, a security audit has been performed and it has identified a 
series of security flaws. This version fixes them all.

### API
For the public API, the endpoint `/clicks` has been added and provide a list
of «hits» that an alias has received. In addition, one can export a CSV file of
these values. Head to [https://go.epfl.ch/api/v1/](https://go.epfl.ch/api/v1/)
for details.

### UX
When creating an alias, the `TTL` and `Hidden` checkboxes have now an
explaination popover.

### Details

  * f3556c7 - [update] prometheus-nginx-exporter update (Nicolas Borboën)
  * cb65ff0 - [fix] Hidden checkbox in admin alias edition (Nicolas Borboën)
  * 5d2416e - [feature] add "no-flags" info + formatting (Nicolas Borboën)
  * 0f6325d - [feature] complete users list (Nicolas Borboën)
  * db98202 - [feature] filter HTTP status code from API (Nicolas Borboën)
  * bfa99f1 - [feature] add admin's home link (Nicolas Borboën)
  * 25b2ce7 - [feature] filter alias without owners (Nicolas Borboën)
  * ca62e7f - [feature] filter on aliases' owner (Nicolas Borboën)
  * 1573b2a - [css] Update svg icon (Nicolas Borboën)
  * c1324fd - [feature] filter on «status» query string (Nicolas Borboën)
  * a43a1e0 - [feature] split admin pages (Nicolas Borboën)
  * ec5efe0 - [feature] Laravel Admin is accessible to /admin-php (Grégory Charmier)
  * 9056243 - [feature] Admin REST API improved (Grégory Charmier)
  * 23fe907 - [feature] Admin backend reactified (Grégory Charmier)
  * 4538097 - [feature] add CSV exporter (Nicolas Borboën)
  * 5f643c5 - [refactor] Alias details in API (Nicolas Borboën)
  * 29382bf - [refactor] API's clicks endpoint (Nicolas Borboën)
  * bc46567 - [feature] Several improvements to the Rest API (Grégory Charmier)
  * d54d474 - [feature] Several improvements to the admin area in React (Grégory Charmier)
  * d60e3df - [security] Ensure that redirect_uri has not been forged (Grégory Charmier)
  * 9945336 - [security] encodeURI within datatable (Nicolas Borboën)
  * da933fe - [security] limit the alias's allowed chars (Nicolas Borboën)
  * 48d6cd8 - [security] fix alias length to 84 chars (Nicolas Borboën)
  * 8cebc57 - [security] API token improvements (Grégory Charmier)
  * a7266eb - [security] Hide PHP version in headers... (Nicolas Borboën)
  * e60cd73 - [security] Use Secure Cookie by default (Nicolas Borboën)
  * 04901f2 - [security] HSTS headers (Nicolas Borboën)
  * b259304 - [feature] Manage loading in User Blacklist (Grégory Charmier)
  * 1167bd7 - [feature] add 'react-js-pagination' 'react-router-dom' 'jest' libraries (Grégory Charmier)
  * a291cef - [feature] Add a temp template for react dev (Grégory Charmier)
  * 94a12e5 - [feature] Extend API REST (Grégory Charmier)
  * 150b664 - [feature] Implement Admin backoffice (Grégory Charmier)
  * 47bafb8 - [feature] Makes it possible to work together React Router and Laravel Router (Grégory Charmier)
  * 745c743 - [fix] refresh materialized-view command (saphirevert)
  * e39f574 - [feature] install react ! Youpi (Grégory Charmier)
  * cb37924 - [update] postgresql to 13 in dev and workspace (Nicolas Borboën)
  * 8bce7cd - [fix] cut.epfl.ch and short.epfl.ch redirection (Nicolas Borboën)
  * eefa16a - [doc] § changelog, roadmap and contributing added (Nicolas Borboën)
  * 0687a34 - [doc] ARCHITECTURE added (Nicolas Borboën)
  * ba30887 - [fix] Fix mouseover highlighting bug (saphirevert)
  * 1fdcf34 - [fix] missing search param in pagination (saphirevert)

## v1.5.0 (2021-02-18)
- [bug] Mobile menu doesn't display the profile link (#83)
- [bug] Editing alias with infinite validity (#65)
- [bug] Impossible to shorten URL containing a "~" (#60)
- [bug] links in search results are wrong (#90)
- [feature] Accept standards-compliant URLs (#67)
- [feature] Add notification email when an alias is edited (#75)
- [doc] Create CHANGELOG.md, ROADMAP.md and ARCHITECTURE.md (#86)
- [test] Set up functional test with dusk
- [test] Functional test with dusk: create an alias
- [test] Unit tests work sucessfully
- [test] Add unit tests to validate URL
- [monitoring] Prometheus
- [update] Update traefik 1.7 to 2.4 
- [migration] Migrate laravel 5.7 to 8.0
- [update] All php dependencies
- [update] Update phpunit from 6.5 to 9.5.1
- [config] Manage many environments (prod, dev)
- [config] Models folder
- [feature] Set up go-qual.epfl.ch
- [feature] Degraded mode
- [feature] Telegram alerts

## v1.0.beta
