<?php

namespace SocialiteProviders\Tequila;

use SocialiteProviders\Manager\SocialiteWasCalled;

class TequilaExtendSocialite
{
    /**
     * Execute the provider.
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('tequila', __NAMESPACE__.'\Provider');
    }
}
