<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    private $users = [
        'nbo' => [
            'firstname' => 'Nicolas',
            'lastname' => 'Borboën',
            'email' => 'nicolas.borboen@epfl.ch',
            'sciper' => '169419',
            'username' => 'nborboen',
            'is_admin' => true,
            'token' => ''
        ],
        'dqu' => [
            'firstname' => 'Dominique',
            'lastname' => 'Quatravaux',
            'email' => 'dominique.quatravaux@epfl.ch',
            'sciper' => '243371',
            'username' => 'quatrava',
            'is_admin' => true,
            'token' => ''
        ],
        'gch' => [
            'firstname' => 'Grégory',
            'lastname' => 'Charmier',
            'email' => 'gregory.charmier@epfl.ch',
            'sciper' => '188475',
            'username' => 'charmier',
            'is_admin' => true,
            'token' => ''
        ],
        'jde' => [
            'firstname' => 'Julien',
            'lastname' => 'Delasoie',
            'email' => 'julien.delasoie@epfl.ch',
            'sciper' => '194044',
            'username' => 'delasoie',
            'is_admin' => true,
            'token' => ''
        ],
        'gca' => [
            'firstname' => 'Giovanni',
            'lastname' => 'Cangiani',
            'email' => 'giovanni.cangiani@epfl.ch',
            'sciper' => '121769',
            'username' => 'cangiani',
            'is_admin' => true,
            'token' => ''
        ],
        'czu' => [
            'firstname' => 'Christian',
            'lastname' => 'Zufferey',
            'email' => 'christian.zufferey@epfl.ch',
            'sciper' => '106785',
            'username' => 'czufferey',
            'is_admin' => true,
            'token' => ''
        ],
        'klg' => [
            'firstname' => 'Kermit',
            'lastname' => 'La Grenouille',
            'email' => 'kermit.lagrenouille@epfl.ch',
            'sciper' => '133134',
            'username' => 'lagrenou',
            'is_admin' => false,
            'token' => ''
        ],
        'jyc' => [
            'firstname' => 'Jérôme',
            'lastname' => 'Cosandey',
            'email' => 'jerome.cosandey@epfl.ch',
            'sciper' => '316897',
            'username' => 'jycosand',
            'is_admin' => true,
            'token' => ''
        ],
        // Seed yourself...
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users as $initials => $user) {
            $ret = User::firstOrCreate( ['sciper' => $user['sciper']], $user );
        }
    }
}
