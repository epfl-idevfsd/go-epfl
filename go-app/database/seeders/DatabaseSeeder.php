<?php

use App\Models\Url;

use App\Models\User;
use App\Models\Alias;
use Illuminate\Database\Seeder;
use Database\Seeders\LogSeeder;
use Database\Seeders\UrlSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\AliasSeeder;
use Database\Seeders\BlackListSeeder;
use Database\Seeders\PermissionSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
        // Upgrade Laravel 8:
        // https://stackoverflow.com/a/63902764/960623

        // One random alias linked to a random URL
        Alias::factory()->create([
            'url_id' => Url::factory()->create()->id
        ]);

        // One simple user
        User::factory()->create();

        // One admin user
        User::factory()->ensure_admin()->create();
        */
        $this->call([
          UserSeeder::class,
          UrlSeeder::class,
          AliasSeeder::class,
          LogSeeder::class,
          BlackListSeeder::class,
          PermissionSeeder::class,
      ]);
    }
}
