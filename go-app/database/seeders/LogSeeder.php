<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Alias;
use App\Models\Log;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $alias = Alias::where('alias', 'nbo')->first();
        Log::insert(
            [
                'click_time' => '2021-02-03 11:11:00.000',
                'referrer' => config('app.url') . 'profile',
                'user_agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
                'ip_address' => '0.0.0.0',
                'iso_code' => '0',
                'alias_id' => $alias->id,
                'alias' => $alias->alias,
                'url' => 'http://people.epfl.ch/169419',
            ]
        );

        $alias = Alias::where('alias', 'menu')->first();
        foreach (range(1, 10) as $idx) {
            Log::insert(
                [
                    'click_time' => date("Y-m-d H:i:s"),
                    'referrer' => config('app.url') . 'profile',
                    'user_agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
                    'ip_address' => '0.0.0.0',
                    'iso_code' => '0',
                    'alias_id' => $alias->id,
                    'alias' => $alias->alias,
                    'url' => $alias->url->url,
                ]
            );
        }

        $alias = Alias::where('alias', 'coronavirus')->first();
        foreach (range(1, 15) as $idx) {
            Log::insert(
                [
                    'click_time' => date("Y-m-d H:i:s"),
                    'referrer' => config('app.url') . 'profile',
                    'user_agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
                    'ip_address' => '0.0.0.0',
                    'iso_code' => '0',
                    'alias_id' => $alias->id,
                    'alias' => $alias->alias,
                    'url' => $alias->url->url,
                ]
            );
        }
    }
}
