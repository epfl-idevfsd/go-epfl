<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Alias;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // nbo
        $nbo = User::where('sciper', '169419')->first();
        $aliasNbo = Alias::where('alias', 'nbo')->first();
        Permission::firstOrCreate(
            ['alias_id' => $aliasNbo->id, 'user_id' => $nbo->id],
            [
                'alias_id' => $aliasNbo->id,
                'user_id' => $nbo->id
            ]
        );

        // Allow all admins to be owner of Zombo alias
        $admins = User::where('is_admin', true)->get();
        $aliasZombo = Alias::where('alias', 'zombo')->first();
        $aliasRick = Alias::where('alias', 'rick')->first();
        foreach ($admins as $key => $admin) {
            Permission::firstOrCreate(
                ['alias_id' => $aliasZombo->id, 'user_id' => $admin->id],
                [
                    'alias_id' => $aliasZombo->id,
                    'user_id' => $admin->id
                ]
            );
            Permission::firstOrCreate(
                ['alias_id' => $aliasRick->id, 'user_id' => $admin->id],
                [
                    'alias_id' => $aliasRick->id,
                    'user_id' => $admin->id
                ]
            );
        }

        // Kermit
        $kermit = User::where('sciper', '133134')->first();
        $aliasKermit = Alias::where('alias', 'kermit')->first();
        Permission::firstOrCreate(
            ['alias_id' => $aliasKermit->id, 'user_id' => $kermit->id],
            [
                'alias_id' => $aliasKermit->id,
                'user_id' => $kermit->id
            ]
        );

        // A long one
        $dqu = User::where('sciper', '243371')->first();
        $aliasLong = Alias::where('alias', 'a-long-one')->first();
        Permission::firstOrCreate(
            ['alias_id' => $aliasLong->id, 'user_id' => $dqu->id],
            [
                'alias_id' => $aliasLong->id,
                'user_id' => $dqu->id
            ]
        );

    }
}
