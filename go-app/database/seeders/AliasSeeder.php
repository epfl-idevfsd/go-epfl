<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Url;
use App\Models\Alias;

class AliasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert nbo's Alias
        $urlNbo = Url::where('url', 'http://people.epfl.ch/169419')->first();
        $urlNboRet = Alias::firstOrCreate(
            ['url_id' => $urlNbo->id, 'alias' => 'nbo'],
            [
                'created_at' => '2010-08-18 08:19:19',
                'updated_at' => '2019-07-25 14:15:16',
                'alias' => 'nbo',
                'url_id' => $urlNbo->id,
                'hidden' => false,
                'code' => 'a5',
            ]
        );

        // Insert mag's and magazine's Alias
        $urlMag = Url::where('url', 'https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/')->first();
        $urlMagRet = Alias::firstOrCreate(
            ['url_id' => $urlMag->id, 'alias' => 'mag'],
            [
                'created_at' => '2010-08-18 08:19:19',
                'updated_at' => '2019-07-25 14:15:16',
                'alias' => 'mag',
                'url_id' => $urlMag->id,
                'hidden' => false,
            ]
        );
        $urlMagRet2 = Alias::firstOrCreate(
            ['url_id' => $urlMag->id, 'alias' => 'magazine'],
            [
                'created_at' => '2010-08-18 08:19:19',
                'updated_at' => '2019-07-25 14:15:16',
                'alias' => 'magazine',
                'url_id' => $urlMag->id,
                'hidden' => false,
            ]
        );

        // Insert zombo and zzz's Alias
        $urlZombo = Url::where('url', 'https://zombo.com')->first();
        $urlZomboRet = Alias::firstOrCreate(
            ['url_id' => $urlZombo->id, 'alias' => 'zombo'],
            [
                'created_at' => '2019-09-05 15:43:05',
                'updated_at' => '2019-09-05 15:43:05',
                'alias' => 'zombo',
                'url_id' => $urlZombo->id,
                'hidden' => false
            ]
        );
        $urlZomboRet2 = Alias::firstOrCreate(
            ['url_id' => $urlZombo->id, 'alias' => 'zzz'],
            [
                'created_at' => '2019-09-05 15:43:05',
                'updated_at' => '2019-09-05 15:43:05',
                'alias' => 'zzz',
                'url_id' => $urlZombo->id,
                'hidden' => false
            ]
        );

        // Insert menu's Alias
        $urlMenu = Url::where('url', 'https://menus.epfl.ch/cgi-bin/getMenus?&midisoir=midi')->first();
        $urlMenuRet = Alias::firstOrCreate(
            ['url_id' => $urlMenu->id, 'alias' => 'menu'],
            [
                'created_at' => '2010-01-05 23:33:48',
                'updated_at' => '2019-07-25 14:15:26',
                'alias' => 'menu',
                'url_id' => $urlMenu->id,
                'hidden' => false,
                'code' => 'i'
            ]
        );

        // Insert coronavirus's Alias
        $urlCoronavirus = Url::where('url', 'https://www.epfl.ch/campus/security-safety/sante/coronavirus-covid-19/')->first();
        $urlCoronavirusRet = Alias::firstOrCreate(
            ['url_id' => $urlCoronavirus->id, 'alias' => 'coronavirus'],
            [
                'created_at' => '2020-02-25 12:07:15',
                'updated_at' => '2020-02-25 14:29:19',
                'alias' => 'coronavirus',
                'url_id' => $urlCoronavirus->id,
                'hidden' => false
            ]
        );

        // Insert gofi's Alias
        $urlGofi = Url::where('url', 'http://flashinformatique.epfl.ch/spip.php?article2176')->first();
        $urlGofiRet = Alias::firstOrCreate(
            ['url_id' => $urlGofi->id, 'alias' => 'gofi'],
            [
                'created_at' => '2020-02-25 12:07:15',
                'updated_at' => '2020-02-25 14:29:19',
                'alias' => 'gofi',
                'url_id' => $urlGofi->id,
                'hidden' => false,
                'code' => 'cx'
            ]
        );

        // Never Gonna Give You Up
        $urlRick = Url::where('url', 'https://www.youtube.com/watch?v=dQw4w9WgXcQ')->first();
        $urlRickRet = Alias::firstOrCreate(
            ['url_id' => $urlRick->id, 'alias' => 'rick'],
            [
                'created_at' => '2009-10-25 13:37:42',
                'updated_at' => '2009-10-25 13:37:42',
                'alias' => 'rick',
                'url_id' => $urlRick->id,
                'hidden' => true
            ]
        );
        $urlRickRet2 = Alias::firstOrCreate(
            ['url_id' => $urlRick->id, 'alias' => 'NGGYU'],
            [
                'created_at' => '2009-10-25 13:37:42',
                'updated_at' => '2009-10-25 13:37:42',
                'alias' => 'NGGYU',
                'url_id' => $urlRick->id,
                'hidden' => true
            ]
        );

        // Kermit
        $urlKermit = Url::where('url', 'https://fr.wikipedia.org/wiki/Kermit_la_grenouille')->first();
        $urlKermitRet = Alias::firstOrCreate(
            ['url_id' => $urlKermit->id, 'alias' => 'kermit'],
            [
                'created_at' => '1955-05-09 13:37:42',
                'updated_at' => '1955-05-09 13:37:42',
                'alias' => 'kermit',
                'url_id' => $urlKermit->id,
                'hidden' => true
            ]
        );

        // A long one
        $urlLong = Url::where('url', 'http://a.b.c.d.e.f.g.h.i.j.k.l.m.n.oo.pp.qqq.rrrr.ssssss.tttttttt.uuuuuuuuuuu.vvvvvvvvvvvvvvv.wwwwwwwwwwwwwwwwwwwwww.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy.zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz.me/')->first();
        $urlLongRet = Alias::firstOrCreate(
            ['url_id' => $urlLong->id, 'alias' => 'a-long-one'],
            [
                'created_at' => '2021-11-03 13:37:42',
                'updated_at' => '2021-11-03 13:37:42',
                'alias' => 'a-long-one',
                'url_id' => $urlLong->id,
                'hidden' => false
            ]
        );
    }
}
