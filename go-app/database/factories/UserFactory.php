<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
          'sciper' => $this->faker->unique()->randomNumber(6),
          'firstname' => ($fname = $this->faker->firstName),
          'lastname' => ($lname = $this->faker->unique()->lastName),
          'username' => substr(lcfirst($fname), 0, 1).strtolower($lname),
          'email' => strtolower($fname).'.'.strtolower($lname).'@epfl.ch',
          'remember_token' => str_random(10),
          'token' => 'Bearer fake'
        ];
    }

    /**
     * Indicate that the user is admin.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function ensure_admin()
    {
        return $this->state(function (array $attributes) {
            return [ 'is_admin' => true ];
        });
    }
}
