<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAliasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aliases', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('alias', 255)->unique();
            $table->integer('url_id');
            $table->foreign('url_id')->references('id')->on('urls');
            $table->boolean('hidden');
        });

        Schema::table('aliases', function (Blueprint $table) {
            DB::statement('create index idx_aliases on aliases (url_id, id, alias, created_at, updated_at, hidden);');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aliases', function (Blueprint $table) {
            DB::statement('DROP INDEX IF EXISTS idx_aliases');
        });

        Schema::dropIfExists('aliases');
    }
}
