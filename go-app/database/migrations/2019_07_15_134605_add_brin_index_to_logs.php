<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBrinIndexToLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function (Blueprint $table) {
            DB::statement('CREATE INDEX idx_logs_click_time ON logs '
                         .'USING BRIN (click_time) WITH (pages_per_range = 128);');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            DB::statement('DROP INDEX IF EXISTS idx_log_click_time');
        });
    }
}
