<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewAliases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR replace FUNCTION truncateURL(url text, OUT result text) AS
$$
BEGIN
    IF char_length(url) > 99 THEN
    	result := concat_ws('[...]', substring(url, 0, 50), substring(url from char_length(url)-45 for 45));
    else
    	result := url;
    END IF;
    RETURN;
END;
$$
LANGUAGE plpgsql
IMMUTABLE;");

        DB::statement("CREATE MATERIALIZED VIEW public.v_aliases as select 
        	a.id,
        	a.alias,
        	a.created_at,
        	a.updated_at,
        	a.hidden,
        	a.obsolete,
        	u.url,
        	truncateurl(u.url) AS truncatedurl,
        	u.status_code,
        	count(distinct f.id) as flags,
        	count(distinct l.id) as clicks
        from aliases a
        	left join flags f on a.id = f.object_id and f.object_type = 'App\\Models\\Alias'
        	left join logs l on (l.alias_id = a.id)
        	left join urls u on (u.id = a.url_id)
        group by a.id, f.object_id, l.alias_id, u.id
        order by a.id desc;");

        DB::statement("CREATE UNIQUE INDEX v_aliases_alias ON v_aliases(alias);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS v_aliases");
        DB::statement("DROP INDEX IF EXISTS v_aliases_alias;");
        DB::statement("DROP FUNCTION IF EXISTS truncateURL");
    }
}
