<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTimeTz('click_time');
            $table->text('referrer');
            $table->text('user_agent');
            $table->ipAddress('ip_address');
            $table->string('iso_code', 3);
            $table->integer('alias_id');
            $table->string('alias', 255);
            $table->text('url');
            $table->index('alias_id', 'AliasIdLog');
            $table->index('alias', 'AliasLog');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
