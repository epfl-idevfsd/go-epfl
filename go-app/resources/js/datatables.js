import * as $ from 'jquery'
import 'datatables.net'
import 'datatables.net-bs4'

export default (function () {
  $('.dataTable').DataTable({
    'columnDefs': [{
      'targets': 'no-sort',
      'orderable': false
    }]
  })
  $('.dataTableSimple').DataTable({
    'columnDefs': [{
      'targets': 'no-sort',
      'orderable': false
    }],
    'paging': false,
    'searching': false,
    'info': false
  })
}())
