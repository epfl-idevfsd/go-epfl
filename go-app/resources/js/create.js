$(function () {
  // default state
  if ($("#hidden").is(":checked")) {
    $("#publish-tg").prop("checked", false);
    $("#publish-rss").prop("checked", false);
    $("#publish-browse").prop("checked", false);
  } else {
    $("#publish-tg").prop("checked", true);
    $("#publish-rss").prop("checked", true);
    $("#publish-browse").prop("checked", true);
  }
  // whenever #hidden checkox changes
  $("#hidden").on("change", function () {
    if (this.checked) {
      $("#publish-tg").prop("checked", false);
      $("#publish-rss").prop("checked", false);
      $("#publish-browse").prop("checked", false);
    } else {
      $("#publish-tg").prop("checked", true);
      $("#publish-rss").prop("checked", true);
      $("#publish-browse").prop("checked", true);
    }
  });
  // whenever #publish-tg checkox changes
  $("#publish-tg").on("click", function () {
    if (this.checked) {
      $("#hidden").prop("checked", false);
      $("#publish-rss").prop("checked", true);
      $("#publish-browse").prop("checked", true);
    } else {
      $("#hidden").prop("checked", true);
      $("#publish-rss").prop("checked", false);
      $("#publish-browse").prop("checked", false);
    }
  });
  // whenever #publish-rss checkox changes
  $("#publish-rss").on("click", function () {
    if (this.checked) {
      $("#hidden").prop("checked", false);
      $("#publish-tg").prop("checked", true);
      $("#publish-browse").prop("checked", true);
    } else {
      $("#hidden").prop("checked", true);
      $("#publish-tg").prop("checked", false);
      $("#publish-browse").prop("checked", false);
    }
  });
  // whenever #publish-browse checkox changes
  $("#publish-browse").on("click", function () {
    if (this.checked) {
      $("#hidden").prop("checked", false);
      $("#publish-tg").prop("checked", true);
      $("#publish-rss").prop("checked", true);
    } else {
      $("#hidden").prop("checked", true);
      $("#publish-tg").prop("checked", false);
      $("#publish-rss").prop("checked", false);
    }
  });
});
