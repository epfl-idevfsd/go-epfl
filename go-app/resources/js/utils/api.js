import * as axios from "axios";
import { configuration } from "../config";

const getCurrentUser = () => {
  return getRessource("api/v1/users/current").then((response) => {
    return response.data;
  });
};

const getEntryPoint = (ressourceName) => {
  let apiRestUrl = configuration.appUrl;
  return `${apiRestUrl}${ressourceName}`;
};

const getRessource = (ressourceName) => {
  return axios.get(getEntryPoint(ressourceName));
};

const getRessourceWithToken = (ressourceName, access_token) => {
  const options = {
    headers: {
      Authorization: `${access_token}`,
    },
  };
  return axios.get(getEntryPoint(ressourceName), options);
};

const postRessource = (ressourceName, data, token) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      rejectUnauthorized: false,
      Authorization: `${token}`,
    },
  };
  return axios.post(getEntryPoint(ressourceName), data, options);
};

const deleteRessource = (ressourceName, data) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      rejectUnauthorized: false,
    },
  };
  return axios.delete(getEntryPoint(ressourceName), data, options);
};

export {
  getRessource,
  getRessourceWithToken,
  postRessource,
  deleteRessource,
  getCurrentUser,
};
