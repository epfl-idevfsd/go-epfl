import React, { Fragment, Component } from "react";
import Pagination from "react-js-pagination";
import { getRessource } from "../utils/api";
import { Loading } from "../utils/Messages";
import queryString from 'query-string';
class FlagUnreliable extends Component {
  constructor(props) {
    super();
    let params = queryString.parse(props.location.search)
    this.state = {
      flags: [],
      total: 0,
      currentPage: 1,
      perPage: 10,
      keyword: "",
      statusCode: params.status,
      sortedField: "",
      sortConfig: "ascending",
      loaded: false,
    };
    this.handleSelectEntries = this.handleSelectEntries.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.requestSort = this.requestSort.bind(this);
    this.sortTable = this.sortTable.bind(this);
  }

  handleSelectEntries(event) {
    this.setState({ perPage: event.target.value });
  }

  handleFilter(event) {
    let keyword = event.target.value;
    let keywordLowerCase = keyword.toLowerCase();
    getRessource(`api/v1/alias/flags/reliability/`).then((response) => {
      let filteredFlags = response.data.data;
      if (keywordLowerCase.length > 2) {
        filteredFlags = filteredFlags.filter(function (obj) {
          return (
            ("url" in obj &&
              obj["url"] != null &&
              obj["url"]["status_code"] != null &&
              obj["url"]["status_code"]
                .toString()
                .includes(keywordLowerCase)) ||
            ("url" in obj &&
              obj["url"] != null &&
              obj["url"]["url"] != null &&
              obj["url"]["url"].toLowerCase().includes(keywordLowerCase))
          );
        });
        this.setState({
          flags: filteredFlags,
          keyword: keyword,
          total: filteredFlags.length,
        });
      } else {
        this.setState({
          keyword: keyword,
        });
      }
    });
  }

  getFlagsData(currentPage = this.state.currentPage) {
    const { perPage } = this.state;
    let apiReliabilityURL = `api/v1/alias/flags/reliability?page=${currentPage}&per_page=${perPage}`;

    // filter if status_code is set in URL
    if (this.state.statusCode) {
      apiReliabilityURL += `&status=${this.state.statusCode}`;
    }
    getRessource(apiReliabilityURL).then((response) => {
      this.setState({
        flags: response.data.data,
        total: response.data.meta.total,
        currentPage: response.data.meta.current_page,
        perPage: response.data.meta.per_page,
        loaded: true,
      });
    });
  }

  componentDidMount() {
    this.getFlagsData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.perPage !== prevState.perPage) {
      this.getFlagsData();
    }
    if (this.state.keyword === "" && prevState.keyword !== "") {
      this.getFlagsData();
    }
  }

  requestSort(key) {
    let direction = "ascending";
    if (
      this.state.sortConfig.key === key &&
      this.state.sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    this.setState({ sortConfig: { key, direction } });
  }

  sortTable(sortedFlags) {
    const { sortedField, sortConfig } = this.state;
    if (sortedField !== null) {
      sortedFlags.sort((a, b) => {
        if (a["url"][sortConfig.key] < b["url"][sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a["url"][sortConfig.key] > b["url"][sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
  }

  render() {
    const { flags, total, currentPage, perPage, loaded } = this.state;
    let content;
    if (flags.length === 0 && !loaded) {
      content = <Loading />;
    } else {
      let sortedFlags = [...flags];
      this.sortTable(sortedFlags);

      content = (
        <Fragment>
          <h2 className="tlbx-variant-heading">Unreliable flags</h2>
          <div className="row">
            <div className="col-xl-12">
              <div className="mb-4">
                <h3>Manage URL that have been flagged unreliable</h3>
                <div
                  id="people_wrapper"
                  className="dataTables_wrapper dt-bootstrap4 no-footer"
                >
                  <div className="row">
                    <div className="col-sm-12 col-md-6">
                      <div className="dataTables_length" id="people_length">
                        <label>
                          Show{" "}
                          <select
                            name="people_length"
                            aria-controls="people"
                            className="custom-select custom-select-sm form-control form-control-sm"
                            onChange={this.handleSelectEntries}
                          >
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                          </select>{" "}
                          entries
                        </label>
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-6">
                      <div id="people_filter" className="dataTables_filter">
                        <label>
                          Search:
                          <input
                            type="search"
                            className="form-control form-control-sm"
                            placeholder="Filter by keyword"
                            onChange={this.handleFilter}
                          />
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-12">
                      <table
                        id="flags"
                        className="table dataTableSimple dataTable no-footer"
                      >
                        <thead>
                          <tr>
                            <th>
                              <button
                                type="button"
                                onClick={() => this.requestSort("url")}
                                style={{
                                  background: "transparent",
                                  border: "none",
                                  outline: "none",
                                }}
                              >
                                <strong>URL</strong>
                              </button>
                            </th>
                            <th>
                              <strong>Alias-es</strong>
                            </th>

                            <th>
                              <button
                                type="button"
                                onClick={() => this.requestSort("status_code")}
                                style={{
                                  background: "transparent",
                                  border: "none",
                                  outline: "none",
                                }}
                              >
                                <strong>Status</strong>
                              </button>
                            </th>
                            <th className="no-sort">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          {sortedFlags &&
                            sortedFlags.length > 0 &&
                            sortedFlags.map((flag, index) => (
                              <tr key={index}>
                                {flag.url.aliases &&
                                flag.url.aliases[0] &&
                                flag.url.aliases[0].alias ? (
                                  <Fragment>
                                    <td>
                                      <a href={"/" + flag.url.url}>
                                        {flag.url.url}
                                      </a>
                                    </td>
                                    <td>
                                      {flag.url.aliases.map((alias, index) => (
                                        <Fragment key={index}>
                                          <a href={"/" + alias.alias}>
                                            {alias.alias}
                                          </a>
                                          &nbsp;
                                        </Fragment>
                                      ))}
                                    </td>
                                    <td>{flag.url.status_code}</td>
                                    <td>
                                      {flag.url.aliases.map((alias, index) => (
                                        <Fragment key={index}>
                                          <a
                                            href={
                                              "/admin-php/alias/" + alias.alias
                                            }
                                          >
                                            {alias.alias}
                                          </a>
                                          &nbsp;
                                        </Fragment>
                                      ))}
                                    </td>
                                  </Fragment>
                                ) : null}
                              </tr>
                            ))}
                          {sortedFlags && sortedFlags.length == 0 && (
                            <tr className="odd">
                              <td
                                valign="top"
                                colSpan="4"
                                className="dataTables_empty"
                              >
                                No matching records found
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-12 col-md-5">
                      <div
                        className="dataTables_info"
                        id="people_info"
                        role="status"
                        aria-live="polite"
                      >
                        Showing 1 to {perPage} of {total} entries
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-7">
                      <div className="dataTables_paginate paging_simple_numbers">
                        <Pagination
                          className="pagination"
                          activePage={currentPage}
                          totalItemsCount={total}
                          itemsCountPerPage={perPage}
                          onChange={(currentPage) =>
                            this.getFlagsData(currentPage)
                          }
                          itemClass="paginate_button page-item previous"
                          linkClass="page-link"
                          firstPageText="First"
                          lastPageText="Last"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      );
    }
    return content;
  }
}

export { FlagUnreliable };
