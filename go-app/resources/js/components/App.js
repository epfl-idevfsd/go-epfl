import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Navbar } from "./Navbar";
import { Alias } from "./Alias";
import { FlagReport } from "./FlagReport";
import { FlagUnreliable } from "./FlagUnreliable";
import { User } from "./User";
import { Advanced } from "./Advanced";
import { Blacklist } from "./Blacklist";
import { Backoffice } from "./Backoffice";
import { Administrators } from "./Administrators";
import { configuration } from "../config";
class App extends Component {
  render() {
    configuration.appUrl = this.props.appUrl;
    Object.freeze(configuration);
    return (
      <BrowserRouter>
        <div className="container-fluid">
          <div className="row" style={{ marginTop: 20 }}>
            <div className="col-md-2">
              <Navbar />
            </div>
            <div className="col-md-10">
            <Routes>
              <Route path="/">
                <Route path="admin">
                  <Route index element={<Backoffice />} />
                  <Route path="aliases" >
                    <Route index element={<Alias location={configuration} />} />
                    <Route path="report" element={<FlagReport />} />
                    <Route path="unreliable" element={<FlagUnreliable location={configuration} />} />
                    <Route path="blacklist" element={<Blacklist />} />
                  </Route>
                  <Route path="users" >
                    <Route index element={<User />} />
                    <Route path="administrators" element={<Administrators />} />
                  </Route>
                  <Route path="advanced" element={<Advanced />} />
                </Route>
              </Route>              
            </Routes>
            </div>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;

if (document.getElementById("root")) {
  const propsContainer = document.getElementById("root");
  const props = Object.assign({}, propsContainer.dataset);
  ReactDOM.render(<App {...props} />, document.getElementById("root"));
}
