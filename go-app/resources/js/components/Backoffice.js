import React, { Fragment } from "react";
import { Link } from "react-router-dom";

function Backoffice(props) {
  return (
    <Fragment>
      <h2 className="tlbx-variant-heading">Backoffice</h2>
      <br />
      <p>
        Welcome to go.epfl.ch's backoffice. Please use your rights with
        parsimony.
      </p>
      <h3>Actions</h3>
      <ul>
        <li>
          <Link to="/admin/aliases/">Manage aliases</Link>
          <ul>
            <li>
              <Link to="/admin/aliases/?owner=empty">Aliases without owners</Link>
            </li>
          </ul>
        </li>
        <li>
          Manage flags: <Link to="/admin/flags/report/">reports</Link> or{" "}
          <Link to="/admin/flags/unreliable/">unreliables</Link>
          <ul>
            <li>
              View{" "}
              <Link to="/admin/flags/unreliable/?status=404">404 URLs</Link>
            </li>
          </ul>
        </li>
        <li>
          <Link to="/admin/people/">Manage users</Link> and{" "}
          <Link to="/admin/administrators/">administrators</Link>
        </li>
        <li>
          Manage <Link to="/admin/blacklist/">blacklisted words</Link>
        </li>
        <li>
          Run <Link to="/admin/advanced/">reliability test</Link>
        </li>
      </ul>
      <br />
      <h3>Some ideas</h3>
      <ul>
        <li>
          <a href="#">💡 See URLs that return 404</a>
        </li>
        <li>
          <a href="#">💡 Resolve claimed URL</a>
        </li>
        <li>
          <a href="#">💡 See URLs submitted by a user</a>
        </li>
        <li>
          <a href="#">💡 Transfer authority of user's URLs to another one</a>
        </li>
        <li>
          <a href="#">💡 ...</a>
        </li>
      </ul>
    </Fragment>
  );
}

export { Backoffice };
