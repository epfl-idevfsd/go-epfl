import React, { Fragment, Component } from "react";
import {
  getRessourceWithToken,
  postRessource,
  getCurrentUser,
} from "../utils/api";
import { Loading } from "../utils/Messages";

class Advanced extends Component {
  constructor() {
    super();
    this.state = {
      admins: [],
      addSuccess: false,
      deleteSuccess: false,
      message: "",
      sortConfig: "ascending",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.updateUserMsg = this.updateUserMsg.bind(this);
    this.handleRunReliabilityTest = this.handleRunReliabilityTest.bind(this);
    this.requestSort = this.requestSort.bind(this);
  }

  componentDidMount() {
    this.getAdmins();
  }

  async getAdmins() {
    const { perPage } = this.state;
    let user = await getCurrentUser();
    getRessourceWithToken("api/v1/admins", user.token).then((response) => {
      this.setState({
        admins: response.data.data,
        addSuccess: false,
        deleteSuccess: false,
        message: "",
      });
    });
  }

  updateUserMsg() {
    this.setState({
      addSuccess: false,
      deleteSuccess: false,
      message: "",
    });
  }

  async handleRunReliabilityTest(event) {
    event.preventDefault();
    let user = await getCurrentUser();
    postRessource("api/v1/admin/run-reliability-test", {}, user.token).then(
      (response) => {
        console.log(response);
      }
    );
  }

  async handleDelete(event) {
    event.preventDefault();

    let emailToRemove = event.target.id;
    let user = await getCurrentUser();
    let data = { email: emailToRemove };

    postRessource("api/v1/admin/remove", data, user.token).then((resp) => {
      // Delete admin in current state
      let admins = this.state.admins.filter(
        (admin) => admin.email !== emailToRemove
      );
      this.setState({
        admins: admins,
        addSuccess: false,
        deleteSuccess: true,
        message: "",
      });
    });
  }

  async handleSubmit(event) {
    event.preventDefault();

    let admin_email = event.target.elements.new_admin.value;
    let user = await getCurrentUser();
    let data = { email: admin_email };

    postRessource("api/v1/admin", data, user.token).then((response) => {
      let message = "";
      if (response.data.status == "user-does-not-exist") {
        message = `Ooops, user ${admin_email} not found. Please ask the user to first connect to the go.epfl.ch website in order to being able to add it as an administrator.`;
        this.setState({ message: message });
      } else if (response.data.status == "user-is-already-admin") {
        message = `User ${admin_email} is already admin.`;
        this.setState({ message: message });
      } else {
        let admins = this.state.admins;
        // Add new admin
        admins.push(response.data.admin);
        // Sort by email
        admins.sort((a, b) => (b.email < a.email ? 1 : -1));
        // Set state
        this.setState({
          admins: admins,
          addSuccess: true,
          deleteSuccess: false,
          message: "",
        });
      }
      // Clean input field
      document.getElementById("email-new-admin").value = "";
    });
  }

  requestSort(key) {
    let direction = "ascending";
    if (
      this.state.sortConfig.key === key &&
      this.state.sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    this.setState({ sortConfig: { key, direction } });
  }

  sortTable(sortedUsers) {
    const { sortedField, sortConfig } = this.state;
    if (sortedField !== null) {
      sortedUsers.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
  }

  render() {
    const { admins } = this.state;
    let content;
    if (admins.length === 0) {
      content = <Loading />;
    } else {
      let sortedAdmins = [...admins];
      this.sortTable(sortedAdmins);
      content = (
        <Fragment>
          <h2 className="tlbx-variant-heading">Advanced</h2>
          <h3 className="tlbx-variant-heading">Admins actions</h3>
          <button
            className="btn btn-dark btn-lg m-3"
            onClick={this.handleRunReliabilityTest}
          >
            Run reliability test
          </button>
        </Fragment>
      );
    }
    return content;
  }
}

export { Advanced };
