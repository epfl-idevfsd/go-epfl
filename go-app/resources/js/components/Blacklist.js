import React, { Fragment, Component } from "react";
import {
  getRessourceWithToken,
  postRessource,
  getCurrentUser,
} from "../utils/api";
import { AlertSuccess, AlertWarning, Loading } from "../utils/Messages";

class Blacklist extends Component {
  constructor() {
    super();
    this.state = {
      words: [],
      addSuccess: false,
      deleteSuccess: false,
      sortConfig: "ascending",
      message: "",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.requestSort = this.requestSort.bind(this);
    this.getKeywords = this.getKeywords.bind(this);
  }

  componentDidMount() {
    this.getKeywords();
  }

  async getKeywords() {
    let user = await getCurrentUser();
    //getRessource("api/v1/blacklist-items").then((response) => {
    getRessourceWithToken("api/v1/blacklist-items", user.token).then(
      (response) => {
        this.setState({
          words: response.data.data,
          addSuccess: false,
          deleteSuccess: false,
          message: "",
        });
      }
    );
  }

  async handleDelete(event) {
    event.preventDefault();

    let idKeywordToRemove = event.target.id;
    let user = await getCurrentUser();
    let entrypoint = "api/v1/blacklist-items/" + idKeywordToRemove;

    postRessource(entrypoint, {}, user.token).then((resp) => {
      // Delete word in current state
      let words = this.state.words.filter(
        (word) => word.id !== parseInt(idKeywordToRemove)
      );
      this.setState({ words: words, addSuccess: false, deleteSuccess: true });
    });
  }

  async handleSubmit(event) {
    event.preventDefault();

    let new_keyword = event.target.elements.new_keyword.value;
    let user = await getCurrentUser();
    let data = {
      keyword: new_keyword,
    };

    postRessource("api/v1/blacklist-items", data, user.token).then(
      (response, errors) => {
        if (response.data.status == "keyword-already-exist") {
          this.setState({ message: `Keyword ${new_keyword} already exists.` });
        } else {
          let words = this.state.words;
          // Add new word
          words.push(response.data.keyword);
          // Sort by keyword
          words.sort((a, b) =>
            b.keyword.toLowerCase() < a.keyword.toLowerCase() ? 1 : -1
          );
          // Set state
          this.setState({
            words: words,
            addSuccess: true,
            deleteSuccess: false,
            message: "",
          });
        }
        // Clean input field
        document.getElementById("new-keyword").value = "";
      }
    );
  }

  requestSort(key) {
    let direction = "ascending";
    if (
      this.state.sortConfig.key === key &&
      this.state.sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    this.setState({ sortConfig: { key, direction }, message: "" });
  }

  sortTable(sortedUsers) {
    const { sortedField, sortConfig } = this.state;
    if (sortedField !== null) {
      sortedUsers.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
  }

  render() {
    const { words } = this.state;
    let content;
    if (words.length === 0) {
      content = <Loading />;
    } else {
      let sortedWords = [...words];
      this.sortTable(sortedWords);
      content = (
        <Fragment>
          <h2 className="tlbx-variant-heading">Blacklist</h2>
          <div className="row">
            {this.state.addSuccess ? (
              <AlertSuccess message={"Successfully added a new word !"} />
            ) : null}
            {this.state.deleteSuccess ? (
              <AlertSuccess message={"Successfully deleted a word !"} />
            ) : null}
            {this.state.message ? (
              <AlertWarning message={this.state.message} />
            ) : null}
            <div className="col-xl-12">
              <div className="row">
                <div className="col-xl-5">
                  <h3>Manage the list of blacklisted words</h3>
                </div>
                <div className="col-xl-7 float-right">
                  <form
                    onSubmit={this.handleSubmit}
                    className="form-inline float-right"
                  >
                    <input
                      type="text"
                      name="new_keyword"
                      className="form-control form-control-lg"
                      placeholder="Word to add to the blacklist"
                      id="new-keyword"
                    />
                    <input
                      type="submit"
                      className="btn btn-primary"
                      value="Add to the blacklist"
                    />
                  </form>
                </div>
              </div>
              <table
                id="admins"
                className="table table-boxed NodataTable table-striped"
              >
                <thead>
                  <tr>
                    <th>
                      <button
                        type="button"
                        onClick={() => this.requestSort("keyword")}
                        style={{
                          background: "transparent",
                          border: "none",
                          outline: "none",
                        }}
                      >
                        <strong>Keyword</strong>
                      </button>
                    </th>
                    <th>
                      <button
                        type="button"
                        onClick={() => this.requestSort("created_at")}
                        style={{
                          background: "transparent",
                          border: "none",
                          outline: "none",
                        }}
                      >
                        <strong>Created at</strong>
                      </button>
                    </th>
                    <th>
                      <button
                        type="button"
                        onClick={() => this.requestSort("updated_at")}
                        style={{
                          background: "transparent",
                          border: "none",
                          outline: "none",
                        }}
                      >
                        <strong>Updated at</strong>
                      </button>
                    </th>
                    <th>
                      <strong>Action</strong>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {sortedWords.map((word, index) => (
                    <tr key={index}>
                      <td>{word.keyword}</td>
                      <td>{word.created_at.slice(0, 19)}</td>
                      <td>{word.updated_at.slice(0, 19)}</td>
                      <td>
                        <a
                          href="#"
                          onClick={this.handleDelete}
                          className="btn btn-sm btn-danger"
                          id={word.id}
                        >
                          Delete
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </Fragment>
      );
    }
    return content;
  }
}

export { Blacklist };
