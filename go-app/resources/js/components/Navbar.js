import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <nav
        id="nav-aside"
        className="nav-aside"
        role="navigation"
        aria-describedby="nav-aside-title"
      >
        <h2 className="h5 sr-only-xl"></h2>
        <ul className="nav flex-column">
          <li className="nav-item">
            <Link className="nav-link" to="/admin/">
              <b>Home</b>
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/admin/aliases">
              Aliases
            </Link>
            <ul className="nav flex-column">
              <li className="nav-item">
                <Link className="nav-link" to="/admin/aliases/report/">
                  Report
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/admin/aliases/unreliable/">
                  Unreliable
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/admin/aliases/blacklist/">
                  Blacklist
                </Link>
              </li>
            </ul>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/admin/users/">
              Users
            </Link>
            <ul className="nav flex-column">
              <li className="nav-item">
                <Link className="nav-link" to="/admin/users/administrators/">
                  Administrators
                </Link>
              </li>
            </ul>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/admin/advanced/">
              Advanced
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export { Navbar };
