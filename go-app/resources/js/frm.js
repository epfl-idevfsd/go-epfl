$(document).ready(function(){
  $('.link_form_rm_owner').each((index, element) => {
    $( element ).click(() => {
      console.log($( element ).data( 'owner' ));
      if (confirm(`Do you really want to remove ${$( element ).data( 'owner' )} from ${$( element ).data( 'alias' )}'s owners ?`)) {
        $( element ).closest('form').submit();
      }
    })
  })
});
