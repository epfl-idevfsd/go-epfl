@extends('layouts.app')

@section('content')
    <h1>GO EPFL - QR Code Customizer</h1>
    <br>
    <p>Please enter an alias name in the input field to be able to customize the alias's QR code and choose some fancy attributes!</p>
    <form action="/qr/" method="GET" name="frm_qr_customizer">
        <div class="form-group">
            <label for="aliasInput">Alias name</label>
            <input type="text" class="form-control" name="aliasInput" id="aliasInput" aria-describedby="emailHelp" placeholder="Enter alias name" value="{{ $alias ?? '' }}">
        </div>
        <button class="btn btn-primary submit-button">Submit</button>
    </form>
@endsection