@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">{{ ucfirst($page) }}</h3>

    <p>
        Set up in 2010 by <a href="{{ config('app.url') }}nbo">Nicolas Borboën</a>,
        <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> is a service offered to the
        EPFL user community to shorten URLs. It is the result of the fusion of
        two open source tools, <a
        href="https://code.google.com/archive/p/phurl/">PHURL</a> (PHP URL
        shortening system) and <a href="https://yourls.org/">YoURLs</a> (Your
        Own URL Shortener).
    </p>
    <p>
        You can learn more about this first set up in an article (in french) <a
        href="https://infoscience.epfl.ch/record/167655">here on
        infoscience</a>.
    </p>
    <p>
        After ten years of good and loyal service, it was time to refresh the
        code base and add some users requested features. So in 2019, <a
        href="{{ config('app.url') }}lhu">Loïc Humbert</a> has completely rewritten
        the application with <a href="https://laravel.com/">Laravel</a> during
        his end-of-apprenticeship work. The complete source of his work is
        avalaible on <a href="https://gitlab.com/epfl-isasfsd/go-epfl">GitLab.com</a>.
    </p>
    <p>
        Early 2021, <a href="{{ config('app.url') }}gch">Grégory Charmier</a>
        has completely rewritten the administration interface using the React 
        Javascript framework. A breath of fresh air for Go's admins and the 
        positioning of a modern technology within the application.
    </p>
    <p>
        <a href="{{ config('app.url') }}logo">
            <img src="/logo/GoEPFL_red.svg" width="55px" style="float: right; padding: 1px;" />
        </a>
        In mid-2021, Tania Di Paola and Titouan Veuillet, two Interactive Media 
        Designer (IMD) apprentices worked on a new logo. The result of their 
        work is available on the <a href="{{ config('app.url') }}design">design</a> 
        page.
    </p>
@endsection
