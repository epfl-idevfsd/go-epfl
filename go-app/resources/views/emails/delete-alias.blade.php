{{-- Email template for deletion to admins (HTML) --}}
Dear admins,<br />
<br />
On {{ date('Y-m-d H:i:s') }}, {{$user->firstname}} {{$user->lastname}} has deleted<br />
the alias <b>{{$alias->alias}}</b> for the url <a href="{{$alias->url->url}}">{{$alias->url->url}}</a>.<br />
<br />
Firstname: {{$user->firstname}}<br />
Lastname: {{$user->lastname}}<br />
Sciper: {{$user->sciper}} (<a href="https://people.epfl.ch/{{$user->sciper}}">https://people.epfl.ch/{{$user->sciper}}</a>)<br />
Username: {{$user->username}}<br />
Email: <a href="mailto:{{$user->email}}">{{$user->email}}</a><br />
<br />
URL: <a href="{{config('app.url')}}{{$alias->alias}}">{{$alias->url->url}}</a> (Created on {{$alias->url->created_at}})<br />
Alias: <b>{{$alias->alias}}</b><br />
Clicks: <b>{{$alias->clicks_count}}</b><br />
@if(count($alias->owners) == 0)
Owner: This alias has no owner.<br />
@elseif(count($alias->owners) == 1)
Owner: <a href="mailto:{{ $alias->owners[0]->email }}">{{ $alias->owners[0]->firstname }} {{ $alias->owners[0]->lastname }}</a><br />
@else
Owners:
<ul>
@foreach ($alias->owners as $owner)
  <li><a href="mailto:{{ $owner->email }}">{{ $owner->firstname }} {{ $owner->lastname }}</a></li>
@endforeach
</ul>
@endif
<br />
@include('emails.footer')
