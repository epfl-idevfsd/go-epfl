Dear admins,

On {{$alias->created_at}}, {{$user->firstname}} {{$user->lastname}} has created
the alias *{{$alias->alias}}* for the url *{{$alias->url->url}}*.

   Firstname: {{$user->firstname}}
    Lastname: {{$user->lastname}}
      Sciper: {{$user->sciper}} (https://people.epfl.ch/{{$user->sciper}})
    Username: {{$user->username}}
       Email: {{$user->email}}

    From API: {{$fromAPI ? 'yes' : 'no'}}
         URL: {{$alias->url->url}} (Created on {{$alias->url->created_at}})
       Alias: {{$alias->alias}}
          Go: {{config('app.url')}}{{$alias->alias}} → {{$alias->url->url}}
      Hidden: {{$alias->hidden ? '*yes*' : 'no'}}
Obsolescence: {{$alias->obsolescence_date ?: '∞'}}

      Reveal: {{config('app.url')}}reveal/{{$alias->alias}}
      Report: {{config('app.url')}}report/{{$alias->alias}}
        Edit: {{config('app.url')}}edit/alias/{{$alias->alias}}
        Info: {{config('app.url')}}info/{{$alias->alias}}

@include('emails.footer-txt')
