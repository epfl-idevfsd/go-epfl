@if ($sender == 'guest')
<h3>New message from {{$sender}}</h3>
@else
<h3>New message from {{$sender->username}}</h3>
<p><a href="https://people.epfl.ch/{{$sender->sciper}}">{{$sender->firstname}} {{$sender->lastname}}</a> - <a href="mailto:{{$sender->email}}">{{$sender->email}}</a></p>
@endif

<p>Subject: {{$subject_no_prefix}}</p>

<p>{{$sender_message}}</p>

@include('emails.footer')
