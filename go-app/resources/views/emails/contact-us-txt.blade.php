@if ($sender == 'guest')
New message from {{$sender}}
@else
New message from {{$sender->username}} ({{$sender->firstname}} {{$sender->lastname}} <https://people.epfl.ch/{{$sender->sciper}}> - {{$sender->email}})
@endif

*Subject:* {{$subject_no_prefix}}

{{$sender_message}}

@include('emails.footer-txt')
