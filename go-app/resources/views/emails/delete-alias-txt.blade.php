{{-- Email template for deletion to admins (TXT) --}}
Dear admins,

On {{ date('Y-m-d H:i:s') }}, {{$user->firstname}} {{$user->lastname}} has deleted
the alias *{{$alias->alias}}* for the url *{{$alias->url->url}}*.

   Firstname: {{$user->firstname}}
    Lastname: {{$user->lastname}}
      Sciper: {{$user->sciper}} (https://people.epfl.ch/{{$user->sciper}})
    Username: {{$user->username}}
       Email: {{$user->email}}

         URL: {{$alias->url->url}} (Created on {{$alias->url->created_at}})
       Alias: {{$alias->alias}}
      Clicks: {{$alias->clicks_count}}
@if(count($alias->owners) == 0)
       Owner: This alias has no owner.
@elseif(count($alias->owners) == 1)
       Owner: {{ $alias->owners[0]->firstname }} {{ $alias->owners[0]->lastname }} <{{ $alias->owners[0]->email }}>
@else
      Owners:
@foreach ($alias->owners as $owner)
        • {{ $owner->firstname }} {{ $owner->lastname }} <{{ $owner->email }}>
@endforeach
@endif

@include('emails.footer-txt')
