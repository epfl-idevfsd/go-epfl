@extends('admin.layout')

@section('admin-content')
<a href="/admin/aliases" class="btn btn-primary m-2">
    <svg class="icon" aria-hidden="true" style="vertical-align: initial;">
        <use xlink:href="#icon-arrow-left"></use>
    </svg>
    Back
</a>
<div class="row">
    <div class="col-md-6">
        <h3>Edit</h3>
        @include('partials.edit_alias', [
            'prev_url'          => $url,
            'prev_alias'        => $alias,
            'hidden'            => $hidden,
            'obsolescence_date' => $obsolescence_date,
            'action'            => "/admin-php/update/alias",
            'submitButton'      => 'Update',
            'sticky_inputs'     => 'update-admin'
        ])
    </div>
    <div class="col-md-6">
        <h3>Owners</h3>
        <ul>
            @foreach($owners as $owner)
            <li>
                <form name="phpAdminDeleteOwner_{{$loop->index}}" method="post" action="/admin-php/delete/owner" class="form-inline">
                    @csrf
                    {{$owner}}{!! "&nbsp;" !!}
                    <input type="hidden" name="owner" value="{{{$owner}}}" />
                    <input type="hidden" name="alias" value="{{{$alias}}}" />
                    <a href="#" data-owner="{{{$owner}}}" data-alias="{{{$alias}}}" class="link_form_rm_owner">remove</a>
                </form>
            </li>
            @endforeach
        </ul>
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="/admin-php/update/owner" class="form-inline">
                    @csrf
                    <input type="hidden" name="alias" value="{{{$alias}}}" />
                    <input style="width: 80%;" type="text" name="new_owner" class="form-control form-control-lg" />
                    <input type="submit" class="btn btn-primary" value="Add owner"/>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
