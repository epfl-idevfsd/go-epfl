@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Manage blacklist</h3>
    <br />
    <div class="row">
        <div class="col-xl-8">
            <h5>Words</h5>
            <div class="list-group">
                @foreach($blacklist as $word)
                <div class="list-group-item list-group-item-borderless">
                    <div class="row">
                        <div class="col-md-8">
                            <span>{{$word}}</span>
                        </div>
                        <div class="col-md-3 offset-md-1 text-right">
                            <a href="/admin/blacklist/delete/{{$word}}"
                               data-method="delete"
                               class="jquery-postback btn btn-danger btn-sm">
                                Delete
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-xl-4">
            <h5>New word</h5>
            <form method="post" action="/admin/blacklist/append">
                @csrf
                <div class="form-group mb-2">
                    <input class="form-control form-control-md" required="true" type="text" name="keyword" />
                </div>
                <div class="text-right">
                    <input class="btn btn-secondary btn-sm" type="submit" value="Add" />
                </div>
            </form>
        </div>
    </div>
@endsection
