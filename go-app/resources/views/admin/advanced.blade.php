@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Advanced</h3>
    <br />

    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-3">
                    <h5>Administrators</h5>
                </div>
                <div class="col-xl-9 float-right">
                    <form method="post" action="/admin/add/administrator" class="form-inline float-right">
                        @csrf
                        <input type="text" name="new_admin" class="form-control form-control-lg"
                               placeholder="Email address"/>
                        <input type="submit" class="btn btn-primary" value="Add administrator"/>
                    </form>
                </div>
            </div>

            <ul class="list-group list-group-flush">
                @foreach($administrators as $administrator)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-9">
                                {{$administrator->email}}
                            </div>
                            <div class="col-3 text-right">
                                <a href="#" class="btn btn-sm btn-danger">Delete</a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>

        </div>
    </div>

    <br />
    <div class="row">
        <div class="col-xl-4">
            <h3>Admin actions</h3>
            <a href="/admin/jobs/reliability_test" class="btn btn-dark btn-lg m-3">Run reliability test</a>
        </div>
    </div>
@endsection
