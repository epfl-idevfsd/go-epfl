@extends('layouts.app-full')

@section('content')
    <h3 class="tlbx-variant-heading">Browse links</h3>

    <!-- See https://epfl-si.github.io/elements/#/molecules/tables ! -->
    <div class="table-responsive-md">
        <table id="aliases" class=" NodataTable table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th>Link</th>
                    <th>Alias</th>
                    <th>Clicks</th>
                    <th>Date</th>
                    <th class="no-sort">Info</th>
                    @auth
                    <th class="no-sort">Report</th>
                    @endauth
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div id="materlialized_v">
            <br/><small>The browse page uses a <a href="{{ config('app.url') }}/materialized_view">materialized view</a> that refreshes every 30 minutes — your link may not appears at once.</small>
        </div>
    </div>
@endsection

@section('javascript')
<script>
$(function(){
    let aliases =  {!! $aliases !!};
    let rootURL = '{!! url('/').'/'; !!}';
    let data = aliases.map(function(i) {
        return ['<a href="'+encodeURI(rootURL+i.alias)+'">'+i.truncatedurl+'</a>',
                '<a href="'+encodeURI(rootURL+i.alias)+'">'+i.alias+'</a>',
                i.clicks,
                i.created_at,
                '<a href="'+encodeURI(rootURL+'info/'+i.alias)+'">📈</a>',
                '<a href="'+encodeURI(rootURL+'report/'+i.alias)+'" class="reportConfirm">report</a>' + ' ('+i.flags+')']
    });
    console.log("Loading " + aliases.length + " rows...");

    let noSortColumns = [4]
    @auth
    noSortColumns.push(5)
    @endauth
    $('#aliases').dataTable({
        data: data,
        deferRender: true,
        columnDefs: [{
            // no sorting on info or report
            targets: noSortColumns,
            orderable: false
        }],
        // default sort on date
        order: [[ 3, "desc" ]]
    });

    // Ask for confirmation before report
    var elems = document.getElementsByClassName('reportConfirm');
    var confirmIt = function (e) {
        if (!confirm('Are you sure you want to report this alias? Administrators will be notified.')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
});


</script>
@endsection
