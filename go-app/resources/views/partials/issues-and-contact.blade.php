<section class="mb-4">
    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Contact us</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">
        It will be much appreciated if you can open an issue on <a href="https://gitlab.com/epfl-isasfsd/go-epfl/issues" target="_blank">https://gitlab.com/epfl-isasfsd/go-epfl/issues</a>.<br />
        You can also send an email to <a href="mailto:contact-project+epfl-isasfsd-go-epfl-10607222-issue-@incoming.gitlab.com">contact-project+epfl-isasfsd-go-epfl-10607222-issue-@incoming.gitlab.com</a> to automagically create a request.<br />
        Finally, you can contact the maintainers by email <a href="mailto:go AT groupes DOT epfl DOT SWITZERLAND">go at groupes dot epfl dot switzerland</a>.
    </p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-8 mb-md-0 mb-5 offset-md-2">
            <form id="contact-form" name="contact-form" action="{{ url('/emailconfirm') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="select-recipient">Recipient</label>
                    <select id="select-recipient" name="recipient" class="custom-select">
                        <option value="-">{{ __('Please select...') }}</option>
                        <option value="bug">{{ __('Submit a bug') }}</option>
                        <option value="admin">{{ __('Contact administrators') }}</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="subject" class="">{{ __('Subject') }}</label>
                    <input type="text" id="subject" name="subject" class="form-control">
                </div>

                <div class="form-group">
                    <label for="message">{{ __('Your message') }} (<i>be sure to let us know who you are...</i>)</label>
                    <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                </div>

                <div class="form-group">
                    <label for="subject" class="">{{ __('Who\'s the current president of EPFL?') }}</label>
                    <input type="text" id="question" name="question" class="form-control">
                </div>

                <div class="row justify-content-center mt-4">
                    <input type="submit" value="Send" name="submit" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</section>
