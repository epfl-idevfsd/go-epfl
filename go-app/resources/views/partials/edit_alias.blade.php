<form method="post" action="{{$action ?? '/aliasconfirm'}}" name="frm_alias">
    @csrf

    <input type="hidden" name="original_alias" value="{{{$prev_alias ?? ''}}}"/>

    <div class="form-group mb-2">
        <input type="text"
               required="true"
               class="form-control form-control-lg"
               name="url-{{$sticky_inputs}}"
               id="urlInput"
               placeholder="Existing url, e.g. https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/"
               value="{{old('url-'.$sticky_inputs, $prev_url) ?? ''}}"
        />
    </div>

    <div class="input-group input-group-lg">
        <div class="input-group-prepend">
            <span class="input-group-text" id="vanityURL">{{ config('app.url') }}</span>
        </div>
        <input type="text"
               required="true"
               class="form-control form-control-lg"
               name="alias-{{$sticky_inputs}}"
               id="aliasInput"
               aria-describedby="vanityURL"
               placeholder="Alias to use, e.g. mag"
               value="{{old('alias-'.$sticky_inputs, $prev_alias) ?? ''}}"
        />
        <span class="d-flex flex-wrap align-items-center">
            <a href="#" onclick="randomizeAlias()" >
                <img src="/svg/reload.svg" width="30px" />
            </a>
        </span>
    </div>

    <fieldset class="form-group">

        <div class="row pt-2">
            <legend class="col-form-label col-md-4 col-sm-3 pt-0">
                Hidden&nbsp;
                <button aria-hidden="true" type="button" class="btn-circle" data-toggle="popover" data-content="When an alias is hidden, it will not be displayed on the «browse» page, in the syndication feed nor the Telegram channel.">
                    <svg class="icon" aria-hidden="true">
                        <use xlink:href="#icon-info"></use>
                    </svg>
                    <svg class="icon icon-rotate-270" aria-hidden="true">
                        <use xlink:href="#icon-chevron-right"></use>
                    </svg>
                </button>
                <p class="sr-only">When an alias is hidden, it will not be displayed on the «browse» page, in the syndication feed nor the Telegram channel.</p>
            </legend>
            <div class="col-md-8 col-sm-9">
                <div class="form-check form-check">
                    <input class="form-check-input" type="checkbox" name="hidden-{{$sticky_inputs ?? ''}}" id="hidden" value="hidden"
                        {{old('hidden-'.$sticky_inputs, $hidden) ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="hidden">
                        &nbsp;Make this alias hidden&nbsp;
                    </label>
                    <div class="form-check">
                        <ul style="list-style-type: none;">
                            <li>
                                <input class="form-check-input" type="checkbox" name="publish-browse" id="publish-browse" value="publish-browse" />
                                <label class="form-check-label" for="publish-browse">
                                  Publish on the <a href="{{ config('app.url') }}browse">Browse</a> page
                                </label>
                            </li>
                            <li>
                                <input class="form-check-input" type="checkbox" name="publish-tg" id="publish-tg" value="publish-tg" />
                                <label class="form-check-label" for="publish-tg">
                                  Publish on the <a href="https://t.me/joinchat/T2FsT1X3IpGBS11z">Telegram channel</a>
                                </label>
                            </li>
                            <li>
                                <input class="form-check-input" type="checkbox" name="publish-rss" id="publish-rss" value="publish-rss" />
                                <label class="form-check-label" for="publish-rss">
                                  Publish on the <a href="{{ config('app.url') }}feed">RSS feed</a>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <legend class="col-form-label col-md-4 col-sm-3 pt-0">
                TTL&nbsp;
                <button aria-hidden="true" type="button" class="btn-circle" data-toggle="popover" data-content="TTL means Time To Live, in others terms it will define how long this alias will live before to be deactivated.">
                    <svg class="icon" aria-hidden="true">
                        <use xlink:href="#icon-info"></use>
                    </svg>
                    <svg class="icon icon-rotate-270" aria-hidden="true">
                        <use xlink:href="#icon-chevron-right"></use>
                    </svg>
                </button>
                <p class="sr-only">TTL means Time To Live, in others terms it will define how long this alias will live before to be deactivated.</p>
            </legend>
            <div class="col-md-8 col-sm-9">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl-{{$sticky_inputs}}" id="ttl1" value="6"
                        {{old('ttl-'.$sticky_inputs, $ttl ?? false) == 6 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl1">
                        6 months
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl-{{$sticky_inputs}}" id="ttl2" value="12"
                        {{old('ttl-'.$sticky_inputs, $ttl ?? false) == 12 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl2">
                        1 year
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl-{{$sticky_inputs}}" id="ttl3" value="24"
                        {{old('ttl-'.$sticky_inputs, $ttl ?? false) == 24 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl3">
                        2 years
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl-{{$sticky_inputs}}" id="ttl4" value="0"
                        {{old('ttl-'.$sticky_inputs, $ttl ?? false) == 0 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl4">
                        &infin;
                    </label>
                </div>
                @if (isset($ttl) && isset($obsolescence_date))
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl-{{$sticky_inputs}}" id="ttl5" value="-" checked="true"/>
                    <label class="form-check-label" for="ttl5">
                        will expire at: {{$obsolescence_date}}
                    </label>
                </div>
                @endif
            </div>
        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">{{$submitButton}}</button>
        @if(Request::route()->getName() !== 'home')
            @if(Request::route()->getPrefix() ===  '/admin-php')
                <a href="/admin-php/alias/confirm-delete/{{$prev_alias}}" class="btn-lg btn-info btn btn-block" role="button">Delete alias</a>
            @else
                <a href="/alias/confirm-delete/{{$prev_alias}}" class="btn-lg btn-info btn btn-block" role="button">Delete alias</a>
            @endif
        @endif

    </fieldset>

</form>

<script>

// https://stackoverflow.com/a/26410127
// str byteToHex(uint8 byte)
//   converts a single byte to a hex string
function byteToHex(byte) {
    return ('0' + byte.toString(16)).slice(-2);
}
// str generateId(int len);
//   len - must be an even number (default: 40)
function generateId(len = 40) {
    var arr = new Uint8Array(len / 2);
    window.crypto.getRandomValues(arr);
    return Array.from(arr, byteToHex).join("");
}
function randomizeAlias() {
    $('#aliasInput').val(generateId(6))
    // if (confirm('Are you sure you want to generate a random hash as alias?')) {
    //     $('#aliasInput').val(generateId(6))
    // }
}
</script>
