@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">GO EPFL - New alias created</h3>
    <div class="aliasCreationConfirmation">
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Well done!</h4>
            <input type="hidden" value="{{{ $new_url }}}" id="new_url_hidden"/>
            <p>
                  The URL <a href="{{{ $new_url }}}" target="_blank">{{{ $alias['url']->url }}}</a> has been shortened to
                  <span id="shortened_link" class="font-weight-bold">
                      <a href="{{{ $new_url }}}">{{{ $new_url }}}</a>
                  </span>
                  <button onclick="copyToClipboard('new_url_hidden')" class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="right" title="Copy shortened link">
                      <img src="/svg/Feather-core-copy.svg" alt="Copy shortened URL" onclick="copyToClipboard('new_url_hidden')" width="60%">
                  </button>.
            </p>
            <hr>
            <p class="mb-0 text-muted">
              Whenever you need to, you can edit this alias (or URL) from your profile anytime.
              The direct access to the link of the edit page is <a href="/edit/alias/{{{ $alias['alias'] }}}">{{ config('app.url') }}edit/alias/{{{ $alias['alias'] }}}</a>.
            </p>
        </div>
        @if ($alias['hidden'] == 'true')
        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading">Private ?</h4>
            <p>
                The URL <a href="{{{ $new_url }}}" target="_blank">{{{ $alias['url']->url }}}</a> has been shortened using the "private" option. That means it's not listed in the <a href="/browse">browse</a> page, in the <a href="/feed">RSS</a> feed nor in the <a href="https://t.me/joinchat/T2FsT1X3IpGBS11z">Telegram Channel</a>, but it's still have the same features than another alias.
            </p>
            <hr>
            <p class="mb-0">More details about private aliases can be found in the <a href="/faq/#What-is-a-private-alias">FAQ</a>.</p>
        </div>
        @endif
        <div class="alert alert-info" role="alert">
            <h4 class="alert-heading">Do you know ?</h4>
            <p>
                You can access some basic usage information on this alias in this page: <a href="/info/{{{ $alias['alias'] }}}" target="_blank">{{ config('app.url') }}info/{{{ $alias['alias'] }}}</a>.
            </p>
        </div>
    </div>
<script type="text/javascript">
  // https://stackoverflow.com/a/30905277/960623
  function copyToClipboard(elementId) {
    // Create a "hidden" input
    var aux = document.createElement("input");
    // Assign it the value of the specified element
    aux.setAttribute("value", document.getElementById(elementId).value  );
    // Append it to the body
    document.body.appendChild(aux);
    // Highlight its content
    aux.select();
    // Copy the highlighted text
    document.execCommand("copy");
    // Remove it from the body
    document.body.removeChild(aux);
  }
</script>
@endsection
