@extends('layouts.app')
@section('content')
@if(!env("APP_DEGRADED_MODE"))
    <div class="row">
        <a href="/profile" class="btn btn-primary m-2">
            <svg class="icon" aria-hidden="true">
                <use xlink:href="#icon-arrow-left"></use>
            </svg>
            Back
        </a>
    </div>
    <br>
    <div>
        <h3>Edit</h3>
        <h5>Update alias</h5>
        @include('partials.edit_alias', [
            'prev_url'          => $url,
            'prev_alias'        => $alias,
            'hidden'            => $hidden,
            'ttl'               => $ttl,
            'obsolescence_date' => $obsolescence_date,
            'action'            => '/update/alias',
            'submitButton'      => 'Update',
            'sticky_inputs'     => 'update'
        ])
        <br>
        <div class="owners">
            <h3>Owners</h3>
            @if ( count($owners) == 1 && Auth::user()->email === $owners[0]->email )
                <p>You are the only owner of this alias. Use the form below to share the ownership of this alias with someone else.</p>
            @else
                <ul>
                    @foreach($owners as $owner)
                        <li>{{$owner->email}}</li>
                    @endforeach
                </ul>
            @endif
            <br>
            <p>{{ __('Note: You must fill the field with a valid user account mail address.') }}</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="/update/owner" class="form-inline">
                    @csrf
                    <input type="hidden" name="alias" value="{{{ $alias }}}" />
                    <input placeholder="firstname.lastname@epfl.ch" style="width: 80%;" type="text" name="new_owner" class="form-control form-control-lg" />
                    <input style="width: 20%;" type="submit" class="btn btn-primary" value="Add owner"/>
                </form>
            </div>
        </div>
    </div>
@else
    @include('layouts.partials.degradedmode')
@endif
@endsection
