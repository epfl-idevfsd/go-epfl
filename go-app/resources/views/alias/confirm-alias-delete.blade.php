@extends('layouts.app')
@section('content')
  <h3>Alias Delete</h3>
  <p>
    You are about to delete the alias <b>{{$alias}}</b>.
    @if($aliasObj->clicks_count > 1)
      It has already <b>{{$aliasObj->clicks_count}}</b> clicks.
    @elseif($aliasObj->clicks_count == 1)
      It have clicked only one time.
    @else
      It has never been clicked.
    @endif
  </p>

  @if( count($owners) == 0 ) {{-- case if there is no owner --}}
    <p>This alias has no owner (it happens for old aliases).</p>
  @elseif ( count($owners) == 1 && Auth::user()->email === $owners[0]->email )  {{-- case if the user or an admin want to delete his own alias --}}
    <p>You are the only owner of this alias.</p>
  @elseif( count($owners) == 1 && Auth::user()->is_admin )  {{-- case if an admin is deleting someone else's alias --}}
    <p><a href="mailto:{{ $owners[0]->email }}">{{ $owners[0]->firstname }} {{ $owners[0]->lastname }}</a> is the only owner of this alias.</p>
  @elseif(( count($owners) > 1 ))  {{-- case if there is more that one owner → display them all --}}
    <p>
      This alias is owned by the following users:
      <ul>
        @foreach ($owners as $owner)
          <li>
            <a href="mailto:{{ $owner->email }}">{{ $owner->firstname }} {{ $owner->lastname }}</a>
          </li>
        @endforeach
      </ul>
      They will receive an email notification of this deletion.
    </p>
  @endif
  <p>If you really want to delete this alias, please confirm below.</p>
  @if(Request::route()->getPrefix() === '/admin-php')
    <form method="post" action="/admin-php/alias/delete/{{$alias}}" name="frm_alias_delete">
  @else
    <form method="post" action="/alias/delete/{{$alias}}" name="frm_alias_delete">
  @endif
    @csrf
    <button type="submit" class="btn btn-primary btn-lg btn-block">Confirm delete</button>
  </form>
@endsection
