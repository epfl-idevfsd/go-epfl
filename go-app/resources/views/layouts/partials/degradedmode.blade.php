<div class="callout callout-warning">
  <h4>Degraded mode</h4>
  Sorry, {{ config('app.name') }} is currently in read only due to maintenance reasons. Link redirects still work, but URL shortening and editing are disabled.
</div>