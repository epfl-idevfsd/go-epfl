@unless (config('app.env') == 'production')
  @env(['qual', 'qualite', 'staging', 'preprod'])
    <div class="corner-ribbon bottom-right sticky red shadow">{{ strtoupper(config('app.env')) }}</div>
  @else
    <div class="corner-ribbon bottom-right sticky green shadow">{{ strtoupper(config('app.env')) }}</div>
  @endenv
@endunless

