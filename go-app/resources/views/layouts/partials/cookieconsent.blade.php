    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script>
      window.addEventListener("load", function(){
      window.cookieconsent.initialise({
        "palette": {
          "popup": {
            "background": "#000"
          },
          "button": {
            "background": "#f1d600"
          }
        },
        "content": {
          "message": "By continuing your browsing on this site, you agree to the use of cookies to improve your user experience and to make statistics of visits. ",
          "dismiss": "OK!",
          "link": "Read the legal notice",
          "href": "https://www.epfl.ch/about/overview/overview/regulations-and-guidelines/"
        }
      })});
    </script>