<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title>{{ $feed->title }}</title>
  <link href="{{ $feed->link }}"/>
  <updated>{{ $feed->updated }}</updated>
  <author>
    <name type="html">{{ $feed->author->name }}</name>
    <email>{{ $feed->author->email }}</email>
    <uri>{{ $feed->author->uri }}</uri>
  </author>
  <id>{{ $feed->id }}</id>

  @foreach ($feed->entries as $entry)
    @include('layouts.feedEntry')
  @endforeach
</feed>
