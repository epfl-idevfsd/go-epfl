<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ config('app.name') }}</title>
    <meta name="title" content="Go EPFL">
    <meta name="description" content="EPFL's URL shortener."/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="author" href="humans.txt" />

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!-- Open Graph / Facebook -->
    <meta name="og:type" content="website">
    <meta name="og:url" content="https://go.epfl.ch/">
    <meta name="og:title" content="Go EPFL">
    <meta name="og:description" content="EPFL's URL shortener.">
    <meta name="og:image" content="{{ url('/img/screenshots/screely-full-logged-transparent.png') }}">

    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:url" content="https://go.epfl.ch/">
    <meta name="twitter:site" content="@goepflbot">
    <meta name="twitter:creator" content="@ponsfrilus">
    <meta name="twitter:title" content="Go EPFL">
    <meta name="twitter:description" content="EPFL's URL shortener.">
    <meta name="twitter:image" content="{{ url('/img/screenshots/screely-full-logged-transparent.png') }}">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="alternate" type="application/atom+xml" title="{{ config('app.name') }}'s syndication feed" href="/feed">

    @include('layouts.partials.cookieconsent')

</head>

<body data-spy="scroll" data-target="#intermediate-nav" data-offset="0">

@include('layouts.header')
@yield('body')
@include('layouts.footer')
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@yield('javascript')
</body>

</html>
