    <!-- Authentication Links -->
    @guest
      <li id="menu-item" class="nav-item{{ (isset($page) && $page == 'admin') ? ' current-menu-item current-page-item' : '' }}" >
         <a class="nav-link nav-item" href="{{ route('login') }}">{{ __('Login') }}</a>
      </li>
    @else
      @if(Auth::user()->is_admin)
        <li id="menu-item" class="nav-item{{ (isset($page) && $page == 'admin') ? ' current-menu-item current-page-item' : '' }}" >
          <a class="nav-link nav-item" href="{{ route('admin') }}">{{ __('Admin dashboard') }}</a>
        </li>
      @endif
       <li id="menu-item" class="nav-item{{ (isset($page) && $page == 'profile') ? ' current-menu-item current-page-item' : '' }}" >
         <a class="nav-link nav-item" href="{{ route('profile') }}">{{ Auth::user()->username }} ⌅</a>
      </li>
       <li id="menu-item" class="nav-item{{ (isset($page) && $page == 'logout') ? ' current-menu-item current-page-item' : '' }}" >
         <a class="nav-link nav-item" href="/logout">{{ __('Logout') }} →</a>
      </li>
    @endguest
