<form action="/browse/s/" class="d-xl-none">
  <div class="input-group search-mobile" role="search">
    <div class="input-group-prepend">
      <span class="input-group-text">
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-search"></use></svg>
      </span>
    </div>
    <label for="search" class="sr-only">Search</label>
    <input type="text" class="form-control" name="q" placeholder="Search">
  </div>
</form>