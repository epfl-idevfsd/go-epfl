<nav class="nav-lang nav-lang-short ml-auto">
  <!-- Right Side Of Navbar -->
  <ul class="navbar-nav ml-auto">
    <!-- Authentication Links -->
    @guest
      <li class="nav-item">
        <span><a href="{{ route('login') }}">{{ __('Login') }}</a></span>
      </li>
    @else
      @if(Auth::user()->is_admin)
        <li class="nav-item">
          <span><a href="/admin/">{{ __('Admin') }}</a></span>
        </li>
      @endif
      <li class="nav-item">
        <span><a href="/profile">{{ Auth::user()->username }} ⌅</a></span>
      </li>
      <li class="nav-item">
        <span><a href="/logout">{{ __('Logout') }} →</a></span>
      </li>
    @endguest
  </ul>
  <ul>
    <li>
      <a href="#" aria-label="Français" alt="">FR</a>
    </li>
    <li>
      <span class="active" aria-label="English">EN</span>
    </li>
  </ul>
</nav>