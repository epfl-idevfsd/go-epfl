<div class="dropdown dropright search d-none d-xl-block">
  <a class="dropdown-toggle" href="#" data-toggle="dropdown">
    <svg class="icon" aria-hidden="true"><use xlink:href="#icon-search"></use></svg>
  </a>
  <form action="{{config('app.url')}}browse/s/" class="dropdown-menu border-0 p-0">
    <div class="search-form mt-1 input-group">
      <label for="search" class="sr-only">Search</label>
      <input type="text" class="form-control" name="q" placeholder="Search" >
      <button type="submit" class="d-none d-xl-block btn btn-primary input-group-append">Search</button>
    </div>
  </form>
</div>