<li id="menu-item-home" class="nav-item{{ (isset($page) && $page == 'home') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('home') }}">{{ __('Home') }}</a>
</li>

<li id="menu-item-browse" class="nav-item{{ (isset($page) && $page == 'browse') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('browse') }}">{{ __('Browse') }}</a>
</li>

<li id="menu-item-stats" class="nav-item{{ (isset($page) && $page == 'stats') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('stats') }}">{{ __('Stats') }}</a>
</li>

<li id="menu-item-faq" class="nav-item{{ (isset($page) && $page == 'FAQ') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('FAQ') }}">{{ __('FAQ') }}</a>
</li>

<li id="menu-item-about" class="nav-item{{ (isset($page) && $page == 'about') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('about') }}">{{ __('About') }}</a>
</li>

<li id="menu-item-contact" class="nav-item{{ (isset($page) && $page == 'contact') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('contact') }}">{{ __('Contact') }}</a>
</li>

<li id="menu-item-feed" class="nav-item{{ (isset($page) && $page == 'feed') ? ' current-menu-item current-page-item' : '' }}" >
  <a class="nav-link nav-item" href="{{ route('feed') }}"><img src="/svg/rss.svg" id="rss-icon" alt="RSS Feed"/></a>
</li>
