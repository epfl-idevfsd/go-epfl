  <entry>
    <title type="html">{{ config('app.url') }}{{ $entry->alias }} → {{{$entry->url->url}}}</title>
    <link href="{{ config('app.url') }}{{ $entry->alias }}"/>
    <id>tag:{{ config('app.url') }}{{ $entry->alias }}:@php echo sha1($entry->alias); @endphp</id>
    <updated>{{ $entry->updated_at }}</updated>
    <created>{{ $entry->created_at }}</created>
    <summary type="html">
      <![CDATA[
      The link <a href="{{ config('app.url') }}{{ $entry->alias }}">{{{$entry->url->url}}}</a> 
      has been shortened on {{ $entry->created_at }}.
      <br />
      Its alias is <b>{{ $entry->alias }}</b>.
      <br />
      It has been clicked {{ $entry->clicks_count }} times.
      <br />
      Please visit it here: <a href="{{ config('app.url') }}{{ $entry->alias }}">{{{$entry->url->url}}}</a>.
      <br /><hr />
      <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> — EPFL URL shortener (<a href="{{ config('app.url') }}contact">contact</a>)
      ]]>
    </summary>
  </entry>