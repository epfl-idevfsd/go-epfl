@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-8">
      <h2>@yield('title')</h2>
    </div>
    <div class="col-4">
      <h2>@yield('code')</h2>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-12">
      <p>
        @yield('message')
      </p>
      <p>
        The server returned the HTTP status code <b>@yield('code')</b> "@yield('title')".
        <br />
        You can have more information about this status code <a href="https://httpstatuses.com/@yield('code')" target="_blank">here</a>...
      </p>
      <p>
        If you think that should not have happened, please contact us <a href="/contact">here</a>. Otherwise, head back to the <a href="/">homepage</a>.
      </p>
    </div>
  </div>
@endsection