@extends('layouts.skeleton')
@section('body')
    <div class="main-container">
        @include('layouts.menu.mobile.menu')
        <div id="app-layout" class="container-full">
            <main class="py-4 pl-5 pr-5">
                @include('layouts.errors')
                @include('layouts.partials.announce')
                @yield('content')
            </main>
        </div>
    </div>
@endsection
