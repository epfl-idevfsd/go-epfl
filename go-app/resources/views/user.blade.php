@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">{{ $user->username }}'s profile</h3>

    <h5>Your aliases</h5>
    <!-- See https://epfl-si.github.io/elements/#/molecules/tables ! -->
    <div class="table-responsive-md">
        <table id="aliases" class=" NodataTable table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th>Link</th>
                    <th>Alias</th>
                    <th>Update</th>
                    <th>Date</th>
                    <th class="no-sort">Info</th>
                    <th class="no-sort">Edit</th>
                    <th class="no-sort">Delete</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <br />
    <h5>API Token</h5>
    <div class="card ">
        <div class="row p-4">
            @if($user->api_token)
                <div class="col-md-6">
                    <b>Token:</b> {{$user->api_token}}
                </div>
                <div class="col-md-2">
                    <a href="/token/delete" class="btn btn-danger">Delete</a>
                </div>
                <div class="col-md-2">
                    <a href="/token/generate" class="btn btn-danger">Regenerate</a>
                </div>
            @else
                <div class="col-md-4 offset-4">
                    <a href="/token/generate" class="btn btn-danger">Generate token</a>
                </div>
            @endif
        </div>
    </div>
    <h5>Preferences</h5>
    <i>This is a work in progress...</i>
    <br />
    <div class="row">
        <div class="col-md-6">
            <p>Hash generation length</p>
        </div>
        <div class="col-md-4">
            <p>(default 8)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Show alert on hash generation</p>
        </div>
        <div class="col-md-4">
            <p>(default on)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Default TTL</p>
        </div>
        <div class="col-md-4">
            <p>(default 6 month)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Alias hidden</p>
        </div>
        <div class="col-md-6">
            <p>(default off)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Result per page on browse</p>
        </div>
        <div class="col-md-6">
            <p>(default 10)</p>
        </div>
    </div>
@endsection
@section('javascript')
<script>
$(function(){
    let aliases =  {!! $aliases !!};
    let rootURL = '{!! url('/').'/'; !!}';
    let data = aliases.map(function(i) {
        return ['<a href="'+encodeURI(rootURL+i.alias)+'">'+i.truncated_url+'</a>',
                '<a href="'+encodeURI(rootURL+i.alias)+'">'+i.alias+'</a>',
                i.updated_at,
                i.created_at,
                '<a href="'+encodeURI(rootURL+'info/'+i.alias)+'">📈</a>',
                '<a href="'+encodeURI(rootURL+'edit/alias/'+i.alias)+'">edit</a>',
                '<a href="'+encodeURI(rootURL+'alias/confirm-delete/'+i.alias)+'">delete</a>']
    });
    console.log("Loading " + aliases.length + " rows...");

    let noSortColumns = [4, 5, 6]
    $('#aliases').dataTable({
        data: data,
        deferRender: true,
        columnDefs: [{
            // no sorting on info or report
            targets: noSortColumns,
            orderable: false
        }],
        // default sort on date
        order: [[ 3, "desc" ]]
    });
});
</script>
@endsection
