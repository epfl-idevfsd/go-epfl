@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">{{ config('app.name') }} - API</h3>

    <h4>Infos</h4>
    <p>
        <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> provides some <a href="">API</a> endpoints.
    </p>
    <br />
    <h4>Prerequisites</h4>
    <p>
        <ol>
            <li>Be a member of the EPFL community (e.g. have a <a href="https://gaspar.epfl.ch">Gaspar</a> login);</li>
            <li>Have an API token (you can generate one in your <a href="/profile">profile</a>).</li>
        </ol>
    </p>
    <br />
    <h4>Enpoints</h4>
    <br>
    <h5 name="aliases"># aliases</h5>
    <p>
        ➜ <a name="aliases"><b>aliases</b></a>: Get all aliases (paginated)<br/>
        <code>GET {{ config('app.url') }}api/v1/aliases</code><br />
        <a href="{{ config('app.url') }}api/v1/aliases">Try it out</a>
        <p>
            The URL accept some parameters, such as
            <ul>
                <li><code>sort=asc|desc</code> (default is <code>asc</code>)</li>
                <li><code>orderby=id|alias|url|clicks|created_at|updated_atc</code> (default is <code>id</code>)</li>
                <li><code>page=number</code> (default is <code>1</code>)</li>
                <li><code>per_page=number</code> (default is <code>10</code>)</li>
            </ul>
            Example: <code>GET {{ config('app.url') }}api/v1/aliases?orderby=created_at&sort=desc</code><br />
            <a href="{{ config('app.url') }}api/v1/aliases?orderby=created_at&sort=desc">Try it out</a>
        </p>
        <p>
            The results are paginated, please check the <code>links</code> and
            <code>meta</code> elements in the answer.
        </p>
    </p>
    <br>
    <h5 name="alias"># alias</h5>
    <p>

        ➜ <a name="alias/{alias}"><b>alias/{alias}</b></a>: Get a specific alias<br/>
        <code>GET {{ config('app.url') }}api/v1/alias/{alias}</code><br />
        <a href="{{ config('app.url') }}api/v1/alias/zombo">Try it out</a>
    </p>
    <p>
        ➜ <a name="alias"><b>alias</b></a>: Create an alias<br/>
        <code>POST {{ config('app.url') }}api/v1/alias</code><br/>
        Params:<ul>
                    <li><code>token: 0123456789abcdef</code></li>
                    <li><code>alias: zombo</code></li>
                    <li><code>url: https://zombo.com</code></li>
                </ul>
    </p>
    <br>
    <h5 name="clicks"># clicks</h5>
    <p>
        ➜ <a name="clicks/{alias}"><b>clicks/{alias}</b></a>: Get all clicks information on a alias (paginated)<br/>
        <code>GET {{ config('app.url') }}api/v1/clicks/{alias}</code><br />
        <a href="{{ config('app.url') }}api/v1/clicks/zombo">Try it out</a>
    </p>
    <p>
        ➜ <a name="clicks/csv/{alias}"><b>clicks/csv/{alias}</b></a>: Get all clicks information on a alias in a CSV file<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;<small>Note: there is a 2 hours cache on the query.</small><br />
        <code>GET {{ config('app.url') }}api/v1/clicks/csv/{alias}</code><br />
        <a href="{{ config('app.url') }}api/v1/clicks/csv/zombo">Try it out</a><br />
    </p>
    <br />
    <h4>CURL example</h4>
    <p>
    <code>curl -X POST \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;--header 'Content-Type: application/json' \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;--header 'Accept: application/json' \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;-d '{"token":"0123456789abcdef", "alias":"zombo", "url":"https://zombo.com"}' \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'{{ config('app.url') }}api/v1/alias'
    </code>
    </p>
@endsection
