@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load('visualization', '1.0', {'packages':['corechart', 'geochart']});
    </script>

    <h3 class="tlbx-variant-heading">Info</h3>
    <div class="row">
        <div class="col-md-8">
            <div id="total_hits">
                {{__('Hits count')}}: {{$alias->clicks_count}}
            </div>
            <div id="sorry-nostats">
                <b>{{__('Sorry but you have no hits yet.')}}</b>
            </div>
        </div>
        <div class="col-md-4" id="qrcode-container">
            <h3>QR Code</h3>
            {!! $qrcode !!}
            <br><br>
            <a href="/qr/{{$alias->alias}}">
                <button class="btn btn-primary"><i class="fa fa-download"></i>Customize QR code</button>
            </a>
        </div>
    </div>
@endsection
