@extends('layouts.app')

@section('content')
  <h2 class="tlbx-variant-heading">Design</h2>
  <h3 class="tlbx-variant-heading">About</h3>
  <p>
    At the beginning of 2021, two Interactive Media Designer (IMD) apprentices, 
    Tania Di Paola and Titouan Veuillet, were commissioned to propose a logo for
    the GoEPFL application. This page gather the results of their work &mdash;
    A huge thank you to them!
    <blockquote>
      "Le logo Go EPFL représente deux chaînons alliés, faisant référence à un 
      lien. L'icône du chaînon est très souvent utilisée pour représenter le 
      lien. C'est pour cela que nous avons décidé de le créer ainsi. On peut 
      distinguer un G à l'intersection des deux chaînons. Rappelant le G de Go 
      et le tout est encerclé d'un rond noir, rappelant la forme d'un o de GO."
    </blockquote>
  </p>
  <br />
  <hr />
  <h3 class="tlbx-variant-heading">SVG</h3>
  <div class="container">
    <div class="row">
      <div class="col-md-4 p-2 text-center " style="background: #fff;">
        <a href="/logo/GoEPFL_black.svg" target="_blank" title="GoEPFL_black.svg" alt="GoEPFL_black.svg">
          <img src="/logo/GoEPFL_black.svg" width="300px" alt="" / >
        </a>
      </div>
      <div class="col-md-4 p-2 text-center " style="background: #000;">
        <a href="/logo/GoEPFL_white.svg" target="_blank" title="GoEPFL_white.svg" alt="GoEPFL_white.svg">
          <img src="/logo/GoEPFL_white.svg" width="300px" alt="" / >
        </a>
      </div>
      <div class="col-md-4 p-2 text-center " style="background: #fff;">
        <a href="/logo/GoEPFL_red.svg" target="_blank" title="GoEPFL_red.svg" alt="GoEPFL_red.svg">
          <img src="/logo/GoEPFL_red.svg" width="300px" alt="" / >
        </a>
      </div>
    </div>
  </div>

  <h3 class="tlbx-variant-heading">PNG</h3>
  <div class="container">
    <div class="row">
      <div class="col-md-4 p-2 text-center " style="background: #fff;">
        <a href="/logo/GoEPFL_black.png" target="_blank" title="GoEPFL_black.png" alt="GoEPFL_black.png">
          <img src="/logo/GoEPFL_black.png" width="300px" alt="" / >
        </a>
      </div>
      <div class="col-md-4 p-2 text-center " style="background: #000;">
        <a href="/logo/GoEPFL_white.png" target="_blank" title="GoEPFL_white.png" alt="GoEPFL_white.png">
          <img src="/logo/GoEPFL_white.png" width="300px" alt="" / >
        </a>
      </div>
      <div class="col-md-4 p-2 text-center " style="background: #fff;">
        <a href="/logo/GoEPFL_red.png" target="_blank" title="GoEPFL_red.png" alt="GoEPFL_red.png">
          <img src="/logo/GoEPFL_red.png" width="300px" alt="" / >
        </a>
      </div>
    </div>
  </div>

  <h3 class="tlbx-variant-heading">JPG</h3>
  <h4 class="tlbx-variant-heading">Small</h4>
  <div class="container">
    <div class="row">
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_small_black_white.jpg" target="_blank" title="GoEPFL_small_black_white.jpg" alt="GoEPFL_small_black_white.jpg">
          <img src="/logo/GoEPFL_small_black_white.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_small_white_black.jpg" target="_blank" title="GoEPFL_small_white_black.jpg" alt="GoEPFL_small_white_black.jpg">
          <img src="/logo/GoEPFL_small_white_black.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_small_red_white.jpg" target="_blank" title="GoEPFL_small_red_white.jpg" alt="GoEPFL_small_red_white.jpg">
          <img src="/logo/GoEPFL_small_red_white.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_small_red_black.jpg" target="_blank" title="GoEPFL_small_red_black.jpg" alt="GoEPFL_small_red_black.jpg">
          <img src="/logo/GoEPFL_small_red_black.jpg" width="250px" alt="" / >
        </a>
      </div>
    </div>
  </div>

  <h4 class="tlbx-variant-heading">Medium</h4>
  <div class="container">
    <div class="row">
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_medium_black_white.jpg" target="_blank" title="GoEPFL_medium_black_white.jpg" alt="GoEPFL_medium_black_white.jpg">
          <img src="/logo/GoEPFL_medium_black_white.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_medium_white_black.jpg" target="_blank" title="GoEPFL_medium_white_black.jpg" alt="GoEPFL_medium_white_black.jpg">
          <img src="/logo/GoEPFL_medium_white_black.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_medium_red_white.jpg" target="_blank" title="GoEPFL_medium_red_white.jpg" alt="GoEPFL_medium_red_white.jpg">
          <img src="/logo/GoEPFL_medium_red_white.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_medium_red_black.jpg" target="_blank" title="GoEPFL_medium_red_black.jpg" alt="GoEPFL_medium_red_black.jpg">
          <img src="/logo/GoEPFL_medium_red_black.jpg" width="250px" alt="" / >
        </a>
      </div>
    </div>
  </div>

  <h4 class="tlbx-variant-heading">Large</h4>
  <div class="container">
    <div class="row">
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_large_black_white.jpg" target="_blank" title="GoEPFL_large_black_white.jpg" alt="GoEPFL_large_black_white.jpg">
          <img src="/logo/GoEPFL_large_black_white.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_large_white_black.jpg" target="_blank" title="GoEPFL_large_white_black.jpg" alt="GoEPFL_large_white_black.jpg">
          <img src="/logo/GoEPFL_large_white_black.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_large_red_white.jpg" target="_blank" title="GoEPFL_large_red_white.jpg" alt="GoEPFL_large_red_white.jpg">
          <img src="/logo/GoEPFL_large_red_white.jpg" width="250px" alt="" / >
        </a>
      </div>
      <div class="col-md-3 text-center ">
        <a href="/logo/GoEPFL_large_red_black.jpg" target="_blank" title="GoEPFL_large_red_black.jpg" alt="GoEPFL_large_red_black.jpg">
          <img src="/logo/GoEPFL_large_red_black.jpg" width="250px" alt="" / >
        </a>
      </div>
    </div>
  </div>


@endsection
