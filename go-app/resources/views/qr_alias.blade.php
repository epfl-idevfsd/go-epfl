@extends('layouts.app')

@section('content')
    <h1>{{$alias->alias}}</h1>

    {!! $qrcode !!}
    <br><br>
    <a href="/qr/{{$alias->alias}}.png?color={{$color}}&backgroundcolor={{$backgroundcolor}}&size={{$size}}&style={{$style}}&margin={{$margin}}" target="_blank">
        <button class="btn btn-primary"><i class="fa fa-download"></i>View image only</button>
    </a>

    <br><br>

    <form id="frm_qr_customizer" name="frm_qr_customizer" method="get" action="/qr/{{$alias->alias}}">
        <h4>Color</h4>
        <select class="form-control colors" name="color" id="color-input">
            <option {{ "black" === strtolower($color) ? "selected" : "" }} value="black">Black</option>
            <option {{ "red" === strtolower($color) ? "selected" : "" }} value="red">Red</option>
            <option {{ "green" === strtolower($color) ? "selected" : "" }} value="green">Green</option>
            <option {{ "blue" === strtolower($color) ? "selected" : "" }} value="blue">Blue</option>
        </select>
        <h4>Background color</h4>
        <select class="form-control background-colors" name="backgroundcolor" id="backgroundcolor-input">
            <option {{ "white" === strtolower($backgroundcolor) ? "selected" : "" }} value="white">White</option>
            <option {{ "black" === strtolower($backgroundcolor) ? "selected" : "" }} value="black">Black</option>
            <option {{ "red" === strtolower($backgroundcolor) ? "selected" : "" }} value="red">Red</option>
            <option {{ "green" === strtolower($backgroundcolor) ? "selected" : "" }} value="green">Green</option>
            <option {{ "blue" === strtolower($backgroundcolor) ? "selected" : "" }} value="blue">Blue</option>
        </select>
        <h4>Style</h4>
        <select class="form-control styles" name="style" id="style-input">
            <option {{ "square" === $style ? "selected" : "" }} value="square">Square</option>
            <option {{ "round" === $style ? "selected" : "" }} value="round">Round</option>
        </select>
        <h4>Size</h4>
        <output>{{ 200 === $size ? "200" : $size }}</output>
        <input type="range" value='{{ 200 === $size ? "200" : $size }}' class="custom-range" name="size" min="100" max="1000" step="100" id="size-range" oninput="this.previousElementSibling.value = this.value">
        <h4>Margin</h4>
        <output>{{ 1 === $margin ? "1" : $margin }}</output>
        <input type="range" value='{{ 1 === $margin ? "1" : $margin }}' class="custom-range" name="margin" min="1" max="5" step="1" id="margin-range" oninput="this.previousElementSibling.value = this.value">
        <br>
        <input type="submit" id="loadURL" class="btn btn-primary" value="Submit" />
    </form>
@endsection