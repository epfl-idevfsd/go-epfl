<?php

use App\Models\FlagType;
use App\Models\Flag;
use Illuminate\Http\Request;
use App\Models\BlacklistItem;
use Yajra\Datatables\Datatables;
use App\Http\Resources\FlagResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\AliasResource;
use App\Http\Resources\BlacklistItemResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->group(function () {
    Route::prefix('v1')->group(function () {
        // https://scqq.blogspot.com/2018/10/laravel-57-tutorial-datatables-api-jquery.html
        Route::get('datatable', function () {
            if (Cache::has('aliases')) {
                $aliases = Cache::get('aliases');
            } else {
                Cache::put('aliases', \App\Models\Alias::public()->withCount('clicks'), 30); //30 minutes
                $aliases = Cache::get('aliases');
            }
            // return Datatables::of(\App\Models\Alias::public()->withCount('clicks'))->make(true);
            return Datatables::of($aliases)->make(true);
        });
        Route::get('datatableView', function () {
            return Datatables::of(DB::table('v_aliases')->get())->make(true);
        });
        Route::get('datatableView2', function () {
            if (Cache::has('aliasesV')) {
                $aliasesV = Cache::get('aliasesV');
            } else {
                Cache::put('aliasesV', DB::table('v_aliases')->get(), 30); //30 minutes
                $aliasesV = Cache::get('aliasesV');
            }
            return Datatables::of($aliasesV)->make(true);
        });

        Route::get('aliases', 'AliasController@getAliases');
        Route::get('alias/{alias}', 'AliasController@index');
        Route::post('alias', 'AliasController@create');

        // Admin / backoffice
        Route::group([
          'middleware' => ['admin']
          ], function () {
              Route::get('blacklist-items', 'AdminController@getKeywords');
              Route::post('blacklist-items', 'AdminController@appendToBlacklist');
              Route::post('blacklist-items/{id}', 'AdminController@deleteFromBlacklist');

              Route::get('admins', 'AdminController@getAdministrators');
              Route::post('admin', 'AdminController@addAdministrator');
              Route::post('admin/remove', 'AdminController@removeAdministrator');
              Route::post('admin/run-reliability-test', 'AdminController@runReliabilityTest');

              Route::get('users', 'UserController@getUsers');
              Route::post('user/{id}/generate-token', 'UserController@generateToken');
              Route::post('user/{id}/delete-token', 'UserController@deleteToken');

              Route::get('alias/flags/reliability', 'FlagController@getFlags');
              Route::get('alias/flags/report', 'AliasController@getAliasesWithFlags');
          });

        Route::get('users/current', function (Request $request) {
            return $request->user();
        });

        // CLICKS
        Route::get('/clicks', function () {
            return redirect('/api/v1');
        });
        Route::get('clicks/{alias}', 'LogController@index');
        Route::get('clicks/csv/{alias}', 'LogController@csv');
    });
});
