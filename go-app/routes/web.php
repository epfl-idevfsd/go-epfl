<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Head Up: phoinfo will expose sensitive data!
// Route::get('/phpinfo', function () {
//     phpinfo();
// });

Route::get('/', function () {
    return view('home', ['page' => 'home']);
})->name('home');

Route::get('/react', function () {
  return view('home-react');
});

Route::get('/try', function () {
    dd(app()->routes->getRoutes());
});

// Confirm pages
Route::post('/aliasconfirm', 'AliasController@create')->name('aliasconfirm');
Route::post('/emailconfirm', 'PageController@contactSend')->name('emailconfirm');
Route::view('/reportconfirm', 'reportconfirm')->name('reportconfirm');

// Main pages
Route::get('/about', 'PageController@about')->name('about');
Route::get('/stats', 'StatsController@index')->name('stats');
Route::get('/faq', 'PageController@FAQ')->name('FAQ');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/profile', 'UserController@details')->name('profile');
Route::get('/me', 'UserController@me')->name('me');
Route::get('/moi', 'UserController@me')->name('moi');
Route::get('/reveal', 'PageController@revealInfo')->name('revealInfo');
Route::get('/extension', 'PageController@extension')->name('Web Extension');
Route::get('/design', 'PageController@design')->name('Logos');

// Browse
Route::get('/browse', 'BrowseController@index_datatable')->name('browse');
Route::get('/browsedt', 'BrowseController@index_datatable_cache')->name('browsedt');
Route::get('/browsedtnocache', 'BrowseController@index_datatable')->name('browsenocache');
Route::get('/browsedtajax', 'BrowseController@index_datatable_ajax')->name('browsedtajax');
Route::get('/browse/s/', 'BrowseController@search')->name('search');

// API
Route::get('/api', 'PageController@api')->name('api');
Route::get('/api/v1', 'PageController@api')->name('api');

// Specific pages
Route::get('/info/{alias}', 'AliasDetails@info')->name('info');
Route::get('/qr', 'QRController@qr')->name('qr');
Route::get('/qr/{alias}.{format}', 'QRController@qr_image')->name('qr_image');
Route::get('/qr/{alias}', 'QRController@qr_alias')->name('qr_alias');
Route::get('/edit/alias/{alias}', 'AliasController@edit')->name('edit/alias');
Route::post('/update/alias', 'AliasController@update')->name('update/alias');
Route::post('/update/owner', 'AliasController@updateOwner')->name('update/owner');
Route::get('/alias/confirm-delete/{alias}', 'AliasController@confirmDelete')->name('confirm-delete/alias');
Route::post('/alias/delete/{alias}', 'AliasController@delete')->name('delete/alias');
Route::get('/reveal/{alias}', function (App\Models\Alias $alias){
    return view('reveal', [
        'alias' => $alias->alias,
        'url'   => $alias->url->url
    ]);
});
Route::get('/report/{alias}', 'FlagsController@report')->middleware('auth');
Route::get('/token/generate', 'UserController@generateToken');
Route::get('/token/delete', 'UserController@deleteToken');

// Syndication Feed
Route::get('/feed', 'FeedController@index')->name('feed');
Route::permanentRedirect('/rss', '/feed');
Route::permanentRedirect('/atom', '/feed');

// Admin / backoffice
Route::group([
    'prefix' => 'admin-php',
    'middleware' => ['admin']
], function () {
    Route::get('/', 'AdminController@index')->name('admin');

    Route::get('aliases', 'AdminController@aliases')->name('admin-aliases');
    Route::get('alias/{alias}', 'AdminController@editAlias')->name('admin-edit-alias');
    Route::get('/alias/confirm-delete/{alias}', 'AdminController@confirmDelete')->name('alias/confirm-delete');
    Route::post('/alias/delete/{alias}', 'AdminController@deleteAlias')->name('delete/alias');
    Route::post('update/owner', 'AdminController@updateOwner')->name('admin-update-owner');
    Route::post('delete/owner', 'AdminController@deleteOwner')->name('admin-delete-owner');
    Route::post('update/{alias}', 'AdminController@updateAlias')->name('admin-update-alias');

    Route::get('flags', 'AdminController@flags')->name('admin-flags');
    Route::get('people', 'AdminController@people')->name('admin-people');

    Route::get('blacklist', 'AdminController@blacklist')->name('admin-blacklist');
    Route::post('blacklist/append', 'AdminController@appendToBlacklist')->name('admin-blacklist-append');
    Route::delete('blacklist/delete/{keyword}', 'AdminController@deleteFromBlacklist')->name('admin-blacklist-delete');

    Route::get('advanced', 'AdminController@advanced')->name('admin-advanced');
    Route::post('add/administrator', 'AdminController@addAdministrator')->name('admin-add-administrator');
    Route::get('jobs/reliability_test', 'AdminController@runReliabilityTest')->name('admin-reliability-test');
});

// React-Admin / backoffice
Route::group([
  'prefix' => 'admin',
  'middleware' => ['admin']
], function () {
  Route::get('/{path1?}/{path2?}', function () {
    return view('home-react');
  });
});

/*
 * ServiceNow magic redirect for INC, CHG, CIM, CTASK, OUT, KB, RITM, IDEA, PRB and SVC
 *
 * Manual Testing:
 *  - https://go-dev.epfl.ch/INC0441402
 *  - https://go-dev.epfl.ch/CHG0040706
 *  - https://go-dev.epfl.ch/CIM0001553
 *  - https://go-dev.epfl.ch/CTASK0019229
 *  - https://go-dev.epfl.ch/OUT0006519
 *  - https://go-dev.epfl.ch/KB0015277
 *  - https://go-dev.epfl.ch/bKB0015277
 *  - https://go-dev.epfl.ch/RITM0246550
 *  - https://go-dev.epfl.ch/IDEA0001152
 *  - https://go-dev.epfl.ch/PRB0040262
 *  - https://go-dev.epfl.ch/SVC0176
 *  - https://go-dev.epfl.ch/SCR0974766
 *  - https://go-dev.epfl.ch/RSK0000074 (https://support.epfl.ch/nav_to.do?uri=x_epfl2_audit_risk.do?sysparm_query=number=RSK0000074)
 *  - https://go-dev.epfl.ch/AUD0000001 (https://support.epfl.ch/nav_to.do?uri=x_epfl2_audit_report.do?sysparm_query=number=AUD0000001)
 *  - https://go-dev.epfl.ch/PRJ0044890 (https://support.epfl.ch/nav_to.do?uri=/pm_project.do?sysparm_query=number=PRJ0044890)
 *
 * ServiceNow doc:
 *   - https://docs.servicenow.com/bundle/paris-platform-user-interface/page/use/navigation/task/navigate-using-url.html
 *   - https://docs.servicenow.com/bundle/paris-platform-user-interface/page/use/navigation/reference/r_NavigatingByURLExamples.html
 */
$servicenow_url = 'https://support.epfl.ch'; // https://epfl.service-now.com
// magic-redirect for INCident.
Route::get(
    '/INC{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/incident.do?sysparm_query=number=INC' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for CHanGe.
Route::get(
    '/CHG{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/change_request.do?sysparm_query=number=CHG' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for CIM (Continuous IMprovement).
Route::get(
    '/CIM{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/sn_cim_register.do?sysparm_query=number=CIM' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for Change TASK.
Route::get(
    '/CTASK{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/change_task.do?sysparm_query=number=CTASK' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for TASK.
Route::get(
    '/TASK{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/sc_task.do?sysparm_query=number=TASK' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for OUTage.
Route::get(
    '/OUT{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/cmdb_ci_outage.do?sysparm_query=u_number=OUT' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for KnowledgeBase.
Route::get(
    '/KB{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/epfl?id=epfl_kb_article_view&sysparm_article=KB' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for KnowledgeBase (Backoffice), e.g. https://support.epfl.ch/nav_to.do?uri=%2Fkb_view.do%3Fsysparm_article%3DKB0012435
// https://support.epfl.ch/nav_to.do?uri=/kb_view.do&sysparm_article=KB0012435
// It appears that the help desk want to use short links inside
// Service Now itself and it somehow can't manage the link that add
// the left menu — backing off to http://support.epfl.ch/kb_view.do?sysparm_article=KB0012435 while I don't like it at all.
Route::get(
    '/bKB{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/kb_view.do?sysparm_article=KB' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for RITM.
Route::get(
    '/RITM{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/sc_req_item.do?sysparm_query=number=RITM' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for IDEA.
Route::get(
    '/IDEA{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/idea.do?sysparm_query=number=IDEA' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for PRoBlem.
Route::get(
    '/PRB{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/problem.do?sysparm_query=number=PRB' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for PRoJect.
Route::get(
    '/PRJ{id}',
    function ( $id ) use ( $servicenow_url ) {
        https://support.epfl.ch/pm_project.do?sys_id=4f8164809764b1102f9976971153afbf
        return redirect( $servicenow_url . '/nav_to.do?uri=/pm_project.do?sysparm_query=number=PRJ' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for SerViCes.
Route::get(
    '/SVC{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/cmdb_ci_service.do?sysparm_query=u_number=SVC' . $id );
    }
)->where( 'id', '[0-9]{4}' );
// magic-redirect for SCR (Service Centraux Request).
Route::get(
    '/SCR{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=/incident.do?sysparm_query=number=SCR' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for RiSKs.
Route::get(
    '/RSK{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=x_epfl2_audit_risk.do?sysparm_query=number=RSK' . $id );
    }
)->where( 'id', '[0-9]{7}' );
// magic-redirect for AUDits.
Route::get(
    '/AUD{id}',
    function ( $id ) use ( $servicenow_url ) {
        return redirect( $servicenow_url . '/nav_to.do?uri=x_epfl2_audit_report.do?sysparm_query=number=AUD' . $id );
    }
)->where( 'id', '[0-9]{7}' );

// Moodle Redirect (see INC0531022)
// Liste des mots réservés : AR, BIO, BIOENG, CH, ChE, CIVIL, COM, CS, DH, EE, ENG, ENV, FIN, HUM, MATH, ME, MGT, MICRO, MSE, NX, PENS, PHYS, PREPA, QUANT, UNIL
// [2 à 6 lettres (mots réservés)] [1 tiret -][un nombre de 100 à 900][Facultatif : un underscore, 1 à 2 lettre minuscules]
// => '^(AR|BIO|BIOENG|CH|ChE|CIVIL|COM|CS|DH|EE|ENG|ENV|FIN|HUM|MATH|ME|MGT|MICRO|MSE|NX|PENS|PHYS|PREPA|QUANT|UNIL)-(10[0-9]|1[1-9][0-9]|[2-8][0-9]{2}|900)_?[a-z]?[a-z]?'
//
// Addition for INC0549889: special case might include a capital letter or a
// number in parentheses, e.g. MGT-690(A) or BIO-670(1).
// => '^(AR|BIO|BIOENG|CH|ChE|CIVIL|COM|CS|DH|EE|ENG|ENV|FIN|HUM|MATH|ME|MGT|MICRO|MSE|NX|PENS|PHYS|PREPA|QUANT|UNIL)-(10[0-9]|1[1-9][0-9]|[2-8][0-9]{2}|900)((_[a-z]{2})|(\([A-Z0-9]\)))?'
// Modification for INC0549889: no parenthesis
// => '^(AR|BIO|BIOENG|CH|ChE|CIVIL|COM|CS|DH|EE|ENG|ENV|FIN|HUM|MATH|ME|MGT|MICRO|MSE|NX|PENS|PHYS|PREPA|QUANT|UNIL)-(10[0-9]|1[1-9][0-9]|[2-8][0-9]{2}|900)((_[\w\d]{1,2}))?$'
//
// Test case :
// =============== OK ==============
// MGT-690_a
// BIO-670_2
// MGT-690_A
// ChE-222_ab
// ChE-222_AB
// ChE-222_Ab
// ChE-222_aB
// ChE-222_a2
// ChE-222_2a
// AR-100
// ChE-222
// COM-111
// AR-100
// AR-100_as
//
// ============ NOT OK ==============
// MGT-690(A)
// BIO-670(1)
// AR-999_as
// ChE-222_11
// XXX-333_aa
// CH_444_aa
// COM-111-
// ChE-222_A2a
// ChE-222_2Aa
// AR-999_as
// XXX-333_aa
// CH_444_aa
// MGT-690(A)
//
// Test it here: https://regex101.com/r/7mFinL/1
//
$moodle_url = 'https://moodle.epfl.ch/course/';
Route::get(
    '/{moodle_course_id}',
    function ( $moodle_course_id ) use ( $moodle_url ) {
        return redirect( $moodle_url . $moodle_course_id );
    }
)->where( 'moodle_course_id', '^(AR|BIO|BIOENG|CH|ChE|CIVIL|COM|CS|DH|EE|ENG|ENV|FIN|HUM|MATH|ME|MGT|MICRO|MSE|NX|PENS|PHYS|PREPA|QUANT|UNIL)-(10[0-9]|1[1-9][0-9]|[2-8][0-9]{2}|900)((_[\w\d]{1,2}))?$' );

// magic-redirect for Polylex (https://polylex-admin.epfl.ch/lex/1.0.1)
Route::get(
    '/LEX{ver}',
    function ( $ver ) {
        return redirect( 'https://polylex-admin.epfl.ch/lex/' . $ver );
    }
)->where( 'ver', '[0-9]{2}\.[0-9]{2}\.[0-9]{2}' );

// magic-redirect for WordPress-Next Jira project (https://erpdev.atlassian.net/jira/software/projects/WPN/boards/121)
Route::get(
    '/WPN-{num}',
    function ( $num ) {
        // return redirect( 'https://erpdev.atlassian.net/jira/software/projects/WPN/boards/121?selectedIssue=WPN-' . $num );
        return redirect( 'https://erpdev.atlassian.net/browse/WPN-' . $num );
    }
)->where( 'num', '[0-9]{1,3}' );

// Login routes
Route::get('/login', 'Auth\LoginController@redirectToProvider')->name('login');
Route::get('/login/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('/logout', 'Auth\LoginController@logout');

// Dynamic routes KEEP AT THE END
Route::get('/{alias}', 'RedirectController');
