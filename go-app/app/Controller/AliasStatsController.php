<?php

// @todo: Move this controller in the controllers folder

namespace App\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;

class AliasStatsController
{
    static $view_name = "v_aliases";

    /**
     * Refresh the realized view.
     */
    static function refresh()
    {
        DB::statement(
            sprintf("REFRESH MATERIALIZED VIEW CONCURRENTLY %s",
                    static::$view_name));
    }

    static function register_artisan_command($name)
    {
        \Artisan::command($name, function() {
            AliasStatsController::refresh();
        })->describe(static::description());
    }

    static function description()
    {
        return sprintf("Refresh the %s materialized view",
                       static::$view_name);
    }

    static function schedule(Schedule $schedule)
    {
        return $schedule->call(function() {
            AliasStatsController::refresh();
        })->description(static::description());
    }
}
