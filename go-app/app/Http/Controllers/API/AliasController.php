<?php

namespace App\Http\Controllers\API;

use App\Models\Alias;
use App\Models\Url;
use App\Models\User;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\AliasResource;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\AliasWithFlagsResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;


use App\Mail\NewAlias;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Telegram;

class AliasController extends Controller
{
    public function index(Request $request, Alias $alias)
    {
        // in case the alias is hidden, do not display the logs data
        // @TODO: if someone provides a valid token and is the owner of
        //        an hidden alias, then he should be able to access them.
        if ($alias->hidden && (!$request->user() || ($request->user() && !$request->user()->is_admin))) {
            return response()
                ->json([
                    'status' => 'hidden',
                    'errors' => 'Unavailable (alias\'s data hidden).'
                ], Response::HTTP_UNPROCESSABLE_ENTITY)
                ->header('Access-Control-Allow-Origin', '*');
        }
        //return new AliasResource(DB::table('aliases')->where('alias_id', $alias->id)->paginate(15));
        //return new AliasResource($alias->withCount('clicks'));
        return new AliasResource(Alias
            ::where(['alias' => $alias->alias])
            ->withCount('clicks')
            ->first());
    }

    public function getAliases(Request $request)
    {
        // https://stackoverflow.com/questions/57345241/order-direction-must-be-asc-or-desc-laravel-vue
        // https://stackoverflow.com/a/31137592/960623
        // https://laravel.com/docs/8.x/pagination#appending-query-string-values
        $per_page = intval(\Request::get('per_page')) ?: 100;
        if ($per_page > 100) {
            $per_page = 100;
        }

        $sort = strtolower(\Request::get('sort')) == 'desc' ? 'desc' : 'asc';
        $orderby = strtolower(\Request::get('orderby')) ?: 'id';
        $allowedOrderBy = ['id', 'alias', 'url', 'clicks', 'created_at', 'updated_at'];
        if (!in_array($orderby, $allowedOrderBy)) {
            $orderby = 'id';
        }

        // allows the ?owner=nicolas.borboen@epfl.ch in query string, if admin
        $owner = $request->get('owner');
        if ($owner && $request->user() && $request->user()->is_admin) {
            if ($owner === 'empty') {
                $aliases = \App\Models\Alias::whereDoesntHave('owners')
                            ->withCount('clicks')
                            ->orderBy($orderby, $sort)
                            ->paginate($per_page);
            } else {
                $aliases = \App\Models\Alias::whereHas('owners', function (Builder $query) use ($owner) {
                    // Query the pivot table
                    $query->where('email', $owner);
                })->withCount('clicks')
                  ->orderBy($orderby, $sort)
                  ->paginate($per_page);
            }
            $aliases->appends(['owner' => $owner]);

        // admin, but without the owner's lookup
        } elseif ($request->user() && $request->user()->is_admin) {
            $aliases = \App\Models\Alias::withCount('clicks')
                            ->orderBy($orderby, $sort)
                            ->paginate($per_page);
        }
        // NOT admin, only «public» (aka no-hidden) aliases
        else {
            //$aliases = \App\Models\Alias::public()->withCount('clicks')->paginate($per_page);
            $aliases = \App\Models\Alias::public()
                            ->withCount('clicks')
                            ->orderBy($orderby, $sort)
                            ->paginate($per_page);
        }
        $aliases->appends(['per_page' => $per_page])
                ->appends(['sort' => $sort])
                ->appends(['orderby' => $orderby]);
        return AliasResource::collection($aliases);
    }

    public function create(Request $request)
    {
        if ($request->user() && $request->user()->is_admin) {
            $user = ApiHelper::checkBearerToken($request);
        } else {
            $user = ApiHelper::checkUserToken($request);
        }
        $url = $request->input('url');
        $aliasInput = $request->input('alias');
        $hidden = $request->has('hidden');
        $user_id = $user->id;

        $url = Url::firstOrCreate(['url' => $url]);
        $alias = new Alias();
        $alias->alias = $aliasInput;
        $alias->url()->associate($url);
        $alias->hidden = $hidden;

        $validator = $alias->validate();
        if ($validator->fails()) {
            return response()
                ->json([
                    'status' => 'invalid',
                    'errors' => $validator->errors()
                ], Response::HTTP_UNPROCESSABLE_ENTITY)
                ->header('Access-Control-Allow-Origin', '*');
        }

        $alias->save();
        $alias->owners()->attach($user_id);

        //Log::warning(var_export($alias, true));
        //Log::warning(var_export($user, true));
        Mail::to(config('mail.admins'))
            ->send(new NewAlias($alias, $user, $fromAPI=true));
        $tg = new Telegram;
        $tg->aliasCreated($alias, $user, $fromAPI=true);

        return \response()
            ->json([
                    'status' => 'created',
                    'url'    => config('app.url').$alias->alias
                ], Response::HTTP_CREATED)
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function getAliasesWithFlags(Request $request)
    {
        $per_page = intval(\Request::get('per_page')) ?: 100;
        if ($per_page > 100) {
            $per_page = 100;
        }
        $alias = Alias::withCount('flags')->whereHas('flags', function (Builder $query) {
            $query->where('flag_type', 'report');
        })->paginate($per_page);
        
        $alias->appends(['per_page' => $per_page]);
        return AliasWithFlagsResource::collection($alias);
    }
}
