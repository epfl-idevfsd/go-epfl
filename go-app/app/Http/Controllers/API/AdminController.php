<?php

namespace App\Http\Controllers\API;

use App\Models\Url;
use App\Models\User;
use App\Models\Alias;
use Illuminate\Http\Request;
use App\Models\BlacklistItem;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Helpers\ApiHelper;
use App\Http\Resources\BlacklistItemResource;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdminController extends Controller
{
    public function getKeywords(Request $request)
    {
        ApiHelper::checkBearerToken($request);
        return BlacklistItemResource::collection(\App\Models\BlacklistItem::orderBy('keyword')->paginate());
    }

    public function getAdministrators(Request $request)
    {
        ApiHelper::checkBearerToken($request);
        return UserResource::collection(\App\Models\User::admin()->orderBy('email')->paginate());
    }

    public function deleteFromBlacklist($id, Request $request)
    {
        ApiHelper::checkBearerToken($request);

        $keyword = BlacklistItem::findOrFail($id);
        $success_message = "Keyword \"$keyword->keyword\" successfully deleted from blacklist";
        $keyword->delete();

        return \response()
        ->json([
            'status' => 'deleted',
            'keyword'    => $keyword->keyword
        ], Response::HTTP_OK);
    }

    public function appendToBlacklist(Request $request)
    {
        ApiHelper::checkBearerToken($request);

        try {
            $validated = $request->validate([
              'keyword' => 'required|unique:blacklist_items|max:255'
          ]);
            $status = 'created';
            $word = new BlacklistItem();
            $word->keyword = $validated['keyword'];
            $word->save();
            $message = $word;
            return \response()
          ->json([
              'status' => $status,
              'keyword' => $message
          ], Response::HTTP_CREATED);
        } catch (ValidationException $exception) {
            $status = 'keyword-already-exist';
            $message = $exception->errors();
            return \response()
          ->json([
              'status' => $status,
              'keyword' => $message
          ], Response::HTTP_CREATED);
        }
    }

    public function removeAdministrator(Request $request)
    {
        ApiHelper::checkBearerToken($request);

        $admin_email = $request->input('email');
        $admin       = User::where('email', $admin_email)->first();
        if (empty($admin)) {
            return redirect()->back()->withErrors(
                "There is no user with the email address \"$admin_email\""
            );
        }
        $admin->is_admin = false;
        $admin->save();

        return \response()
          ->json([
              'status' => 'deleted',
              'email'    => $admin->email
          ], Response::HTTP_CREATED);
    }

    public function addAdministrator(Request $request)
    {
        ApiHelper::checkBearerToken($request);

        $new_admin_email = $request->input('email');
        $new_admin = User::where('email', $new_admin_email)->first();

        // @TODO: Improve HTTP status RESPONSE
        // https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists
        if (empty($new_admin)) {
            $status = 'user-does-not-exist';
            $message = "There is no user with the email address " . $new_admin_email;
        } elseif ($new_admin->is_admin) {
            $status = 'user-is-already-admin';
            $message = "User is already admin";
        } else {
            $status = 'created';
            $message = $new_admin;
            $new_admin->is_admin = true;
            $new_admin->save();
        }
        return \response()
            ->json([
                'status' => $status,
                'admin' => $message,
            ], Response::HTTP_CREATED);
    }

    public function runReliabilityTest(Request $request)
    {
        ApiHelper::checkBearerToken($request);

        dispatch(function () {
            $reliability_test = new \App\Console\ReliabilityTest;
            $reliability_test();
        })->onQueue('reliability_test');

        return \response()
          ->json([
              'status' => 'Reliability test started',
          ], Response::HTTP_CREATED);
    }
}
