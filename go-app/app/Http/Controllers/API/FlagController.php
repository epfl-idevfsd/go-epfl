<?php

namespace App\Http\Controllers\API;

use App\Models\Url;
use App\Models\Flag;
use App\Models\User;
use App\Models\Alias;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\FlagResource;
use App\Http\Resources\AliasResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FlagController extends Controller
{
    public function getFlags(Request $request)
    {
        // you can define 'per_page' value ~/api/v1/flags?per_page=50. By default 100 flags
        $per_page = intval(\Request::get('per_page')) ?: 100;
        if ($per_page > 100) {
            $per_page = 100;
        }
        $status_code = $request->get('status');
        if ($status_code) {
            $flags = Flag::type('reliability')->where('details->status_code', $status_code)->paginate($per_page);
            $flags->appends(['per_page' => $per_page, 'status' => $status_code]);
        } else {
            $flags = Flag::type('reliability')->paginate($per_page);
            $flags->appends(['per_page' => $per_page]);
        }
        return FlagResource::collection($flags);
    }
}
