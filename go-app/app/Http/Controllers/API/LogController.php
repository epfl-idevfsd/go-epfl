<?php

namespace App\Http\Controllers\API;

use App\Models\Alias;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\LogResource;
use App\Http\Controllers\Controller;

// use Symfony\Component\HttpFoundation\Response;

class LogController extends Controller
{
    /**
     * By default, gives a paginated JSON output of alias's logs
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Alias $alias)
    {
        // in case the alias is hidden, do not display the logs data
        // @TODO: if someone provides a valid token and is the owner of
        //        an hidden alias, then he should be able to access them.
        if ($alias->hidden) {
            return response()
                ->json([
                    'status' => 'hidden',
                    'errors' => 'Unavailable (alias\'s data hidden).'
                ], Response::HTTP_UNPROCESSABLE_ENTITY)
                ->header('Access-Control-Allow-Origin', '*');
        }
        return LogResource::collection(DB::table('logs')->where('alias_id', $alias->id)->paginate(15));
    }
    
    /**
     * Export logs to CSV
     * See https://www.laravelcode.com/post/how-to-export-csv-file-in-laravel-example
     *
     * @return \Illuminate\Http\Response
     */
    public function csv(Alias $alias)
    {
        // in case the alias is hidden, do not display the logs data
        // @TODO: if someone provides a valid token and is the owner of
        //        an hidden alias, then he should be able to access them.
        if ($alias->hidden) {
            return response()
                ->json([
                    'status' => 'hidden',
                    'errors' => 'Unavailable (alias\'s data hidden).'
                ], Response::HTTP_UNPROCESSABLE_ENTITY)
                ->header('Access-Control-Allow-Origin', '*');
        }

        // Set two hours cache on logs request
        $logs_cache = 'go_cache_' . $alias->id;
        if (\Cache::has($logs_cache)) {
            $logs = \Cache::get($logs_cache);
        } else {
            \Cache::put($logs_cache, DB::table('logs')->where('alias_id', $alias->id)->get(), 120); // 120 minutes
            $logs = \Cache::get($logs_cache);
        }
        $fileName = date("Y-m-d_H-i-s") . '_go_' . $alias->alias . '_logs.csv';

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array(
            'id',
            'alias_id',
            'alias',
            'go_url',
            'url',
            'click_time',
            'referrer',
            'iso_code',
            'user_agent',
        );

        $callback = function() use($logs, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

         foreach ($logs as $log) {
             $row['id'] = $log->id;
             $row['alias_id'] = $log->alias_id;
             $row['alias'] = $log->alias;
             $row['go_url'] = config('app.url') . $log->alias;
             $row['url'] = $log->url;
             $row['click_time'] = $log->click_time;
             $row['referrer'] = $log->referrer;
             $row['iso_code'] = $log->iso_code;
             $row['user_agent'] = $log->user_agent;

             fputcsv($file, array(
                 $row['id'],
                 $row['alias_id'],
                 $row['alias'],
                 $row['go_url'],
                 $row['url'],
                 $row['click_time'],
                 $row['referrer'],
                 $row['iso_code'],
                 $row['user_agent'],
             ));
         }

         fclose($file);
     };

     return response()->stream($callback, 200, $headers);
    }

}
