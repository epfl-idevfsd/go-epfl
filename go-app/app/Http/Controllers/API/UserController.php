<?php

namespace App\Http\Controllers\API;

use App\Models\Url;
use App\Models\User;
use App\Models\Alias;
use Illuminate\Http\Request;
use App\Models\BlacklistItem;
use Illuminate\Support\Facades\Log;
use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    public function getUsers(Request $request)
    {
        ApiHelper::checkBearerToken($request);

        // you can define 'per_page' value ~/api/v1/users?per_page=50. By default 100 users
        $per_page = intval(\Request::get('per_page')) ?: 100;
        $users = User::withCount(['aliases'])->orderBy('email')->paginate($per_page);
        $users->appends(['per_page' => $per_page]);
        return UserResource::collection($users);
    }

    public function generateToken($user_id, Request $request)
    {
        ApiHelper::checkBearerToken($request);
       
        $user = User::findOrFail($user_id);
        $new_token = $this->getToken(16);
        $user->api_token = $new_token;
        $user->save();

        return \response()
          ->json([
              'status' => 'created',
              'token'    => $new_token,
          ], Response::HTTP_OK);
    }

    public function deleteToken($user_id, Request $request)
    {
        ApiHelper::checkBearerToken($request);

        $user = User::findOrFail($user_id);
        $user->api_token = null;
        $user->save();

        return \response()
            ->json([
                    'status' => 'token revoked',
                ], Response::HTTP_OK);
    }

    private function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[rand(0, $max-1)];
        }

        return $token;
    }
}
