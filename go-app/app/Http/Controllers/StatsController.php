<?php

namespace App\Http\Controllers;

use App\Models\Alias;
use App\Models\Log;
use Illuminate\Http\Request;

function sql($q) {

    $response = get_object_vars(\DB::select($q)[0]);
    return array_values($response)[0];
}

class StatsController extends Controller
{
    public function index()
    {
        $nb_urls = sql("select COUNT(distinct(url)) from v_aliases");
        $nb_epfl = sql("select count(distinct(url)) from
                        (select url from v_aliases where url like '%epfl.ch%') as epflURLs ");
        $percentage = $nb_urls ? round($nb_epfl / $nb_urls * 100) : 0;
        $nb_aliases = sql("select count(*) from v_aliases");
        $aliases_per_url = $nb_urls ? round($nb_aliases/$nb_urls, 3) : 0;

        $top_alias = \DB::select("select url, COUNT(distinct(alias)) from v_aliases
                         group by url order by count desc LIMIT 1")[0];
        $most_aliases = $top_alias->count;
        $most_aliased_url = $top_alias->url;

        $nb_redirects = sql("select SUM(clicks) from v_aliases");

        $myview = view('stats', [
            'page'              => 'stats',
            'nb_urls'           => $nb_urls,
            'nb_epfl'           => $nb_epfl,
            'percentage'        => $percentage,
            'nb_aliases'        => $nb_aliases,
            'aliases_per_url'   => $aliases_per_url,
            'most_aliases'      => $most_aliases,
            'most_aliased_url'  => $most_aliased_url,
            'nb_redirects'      => $nb_redirects
        ]);

        return $myview;
    }
}
