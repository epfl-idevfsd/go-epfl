<?php

namespace App\Http\Controllers;

use App\Models\Alias;
use App\Models\Flag;
use App\Models\FlagType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Mail\NewReport;
use Illuminate\Support\Facades\Mail;

use App\Helpers\Telegram;

class FlagsController extends Controller
{
    public function report(Alias $alias)
    {
        $flag = new Flag;
        $flag->flag_type = FlagType::REPORT;
        $flag->details = array(
            'sciper'  => Auth::user()->sciper,
            'email'   => Auth::user()->email,
            'message' => 'Inappropriate'
        );

        $alias->flags()->save($flag);
        $this->mailReport($alias);

        $tg = new Telegram;
        $tg->aliasReported($alias, Auth::user());

        return redirect('reportconfirm')->with('alias', $alias);
    }

    private function mailReport(Alias $alias)
    {
        Mail::to(config('mail.admins'))
            ->send(new NewReport($alias, Auth::user()));
    }
}
