<?php namespace App\Http\Controllers;

use App\Models\Alias;
use App\Traits\Pagination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Pagination\LengthAwarePaginator;

class BrowseController extends Controller
{
    use Pagination;

    // This is the default Laravel implementation with pagination.
    public function index()
    {
        $aliases = Alias::public()->withCount('clicks')->paginate(15);
        return $this->displayTable($aliases);
    }

    // This implementation uses datatable and the materialized view without cache.
    // THIS IS THE NEW DEFAULT!
    public function index_datatable()
    {
        // let admins see the hidden URLs
        if (Auth::check() && Auth::user()->is_admin) {
            $aliases = DB::table('v_aliases')->get()->toJson();
        } else {
            $aliases = DB::table('v_aliases')->where('hidden', false)->get()->toJson();
        }

        return view('browse-datatable', [
            'aliases' => $aliases,
            'displayed_pages' => 199,
            'page' => 'browse'
        ]);
    }

    // This implementation uses datatable and the materialized view with cache.
    public function index_datatable_cache()
    {
        $aliases = Cache::remember('aliases', now()->addSeconds(600), function () {
            return DB::table('v_aliases')->where('hidden', false)->get()->toJson();
        });
        return view('browse-datatable', [
            'aliases' => $aliases,
            'displayed_pages' => 0,
            'page' => 'browse'
        ]);
    }

    // This implementation uses datatable and the server side processing
    public function index_datatable_ajax()
    {
        return view('browse-datatable-ajax');
    }

    public function search(Request $request)
    {
        $aliases = Alias::public()->withCount('clicks')->leftJoin('urls', function($join){
            $join->on('aliases.url_id', '=', 'urls.id');
        })->search($request->input('q'))->paginate(20)->withQueryString();
        return $this->displayTable($aliases);
    }

    public function displayTable($aliases)
    {
        $displayed_pages = $this->pagination($aliases);

        return view('browse', [
            'aliases' => $aliases,
            'displayed_pages' => $displayed_pages,
            'page' => 'browse'
        ]);
    }
}
