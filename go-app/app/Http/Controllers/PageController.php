<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function FAQ()
    {
        return view('faq', ['page' => __FUNCTION__]);
    }

    public function about()
    {
        return view('about', ['page' => __FUNCTION__]);
    }

    public function contact()
    {
        return view('contact', ['page' => __FUNCTION__]);
    }

    public function api()
    {
        return view('api', ['page' => __FUNCTION__]);
    }

    public function revealInfo()
    {
        return view('reveal-info', ['page' => __FUNCTION__]);
    }

    public function extension()
    {
        return view('extension', ['page' => __FUNCTION__]);
    }

    public function design()
    {
        return view('design', ['page' => __FUNCTION__]);
    }

    public function contactSend(Request $request)
    {
        $sender    = Auth::user() ?? 'guest';
        $subject   = $request->input('subject');
        $message   = $request->input('message');
        $recipient = $request->input('recipient');
        $question  = $request->input('question');
        $to = '';

        // Ensure the dropdown is selected with admin or bug
        if ($recipient === 'bug') {
            $to = config('mail.gitlab_issues');
        } elseif ($recipient === 'admin') {
            $to = config('mail.admins');
        } else {
            return redirect('/contact')->withErrors('Please select the recipient.');
        }

        // Poor's man capcha
        // if (!in_array(mb_strtolower($question), ['martin vetterli', 'vetterli', 'veterli', 'vetterlli', 'martin'])) {
        if (!in_array(mb_strtolower($question), ['anna fontcuberta i morral', 'anna fontcuberta', 'fontcuberta', 'i morral', 'morral', 'anna'])) {
            return redirect('/contact')->withErrors('Wrong answer. Please search for it.');
        }

        Mail::to($to)
            ->send(new ContactUs($sender, $subject, $message));

        return view('mailconfirm');
    }
}
