<?php

namespace App\Http\Controllers;

use App\Models\Alias;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Helpers\QRHelper;
use Illuminate\Http\Request;

class QRController extends Controller
{
    use QRHelper;

    private $alias;

    private function checkAlias($alias)
    {
        $this->alias = Alias::where(['alias' => $alias])->first();
    }

    public function qr(Request $request)
    {
        // https://laravel.com/docs/8.x/requests
        $aliasInput = $request->input('aliasInput');

        $this->checkAlias($aliasInput);
        if (!$this->alias) {
            $aliasView = view('qr', [
                'alias' => $aliasInput,
            ]);
            if (!empty($aliasInput)) {
                return $aliasView->withErrors("The alias \"$aliasInput\" does not exist");
            } else {
                return $aliasView;
            }
        } else {
            return redirect('/qr/' . $this->alias->alias);
        }

    }

    public function qr_alias(Request $request, $alias)
    {
        $this->checkAlias($alias);

        $color = strtolower($request->query('color')) ?? "black";
        $backgroundcolor = strtolower($request->query('backgroundcolor')) ?? "white";
        $size = $request->query('size') ?? 300;
        $style = $request->query('style') ? strtolower($request->query('style')) : "square";
        $margin = $request->query('margin') ?? 1;
        
        $qrcode = $this->QRcode($color, $backgroundcolor, "svg", $size, $style, $margin);
    
        return view('qr_alias', [
            'alias'             => $this->alias,
            'qrcode'            => $qrcode,
            'color'             => $color,
            'backgroundcolor'   => $backgroundcolor,
            'size'              => $size,
            'style'             => $style,
            'margin'            => $margin,
        ]);

    }

    public function qr_image(Request $request, $alias, $format)
    {
        $this->checkAlias($alias);

        if ($format == "png" || $format == "svg") {
            $color = strtolower($request->query('color')) ?? "black";
            $backgroundcolor = strtolower($request->query('backgroundcolor')) ?? "white";
            $size = $request->query('size') ?? 300;
            $style = $request->query('style') ? strtolower($request->query('style')) : "square";
            $margin = $request->query('margin') ?? 1;
    
            $qrcode = $this->QRcode($color, $backgroundcolor, $format, $size, $style, $margin);
            return response($qrcode)->header('Content-type',`image/$format`);
        } else {
            return abort(404);
        }

    }
}