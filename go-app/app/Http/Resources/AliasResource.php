<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AliasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->user() && $request->user()->is_admin) {
            return [
                'id' => $this->id,
                'alias' => $this->alias,
                'url' => $this->url->url,
                'hidden' => $this->hidden,
                'clicks' => $this->clicks_count,
                'clicks_details' => url("/api/v1/clicks/{$this->alias}"),
                'owners' => $this->owners(),
                'flags' => $this->flags(),
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ];
        } else {
            return [
                'id' => $this->id,
                'alias' => $this->alias,
                'url' => $this->url->url,
                'clicks' => $this->clicks_count,
                'clicks_details' => url("/api/v1/clicks/{$this->alias}"),
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ];
        }
    }

    private function owners()
    {
        $owners = array();
        foreach ($this->owners as $owner)
        {
            $owners []= $owner->email;
        }
        return $owners;
    }

    private function flags()
    {
        $flags = array();
        foreach ($this->flags as $flag) {
            $flags []= $flag->flag_type;
        }
        return $flags;
    }
}
