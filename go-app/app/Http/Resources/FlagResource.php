<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FlagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'type' => $this->flag_type,
          'url' => $this->object(),
          'created_at' => $this->created_at,
          'updated_at' => $this->updated_at,
        ];
    }

    private function object()
    {
        $url = \App\Models\Url::findOrFail($this->object->id);
        $result['id'] = $url->id;
        $result['status_code'] = $url->status_code;
        $result['aliases'] = [];
        foreach ($url->aliases as $key => $alias) {
            $r['id'] = $alias->id;
            $r['alias'] = $alias->alias;
            $result['aliases'][] = $r;
        }
        $result['url'] =  $url->url;
        return $result;
    }
}
