<?php

namespace App\Http\Resources;

use App\Models\Alias;
use Illuminate\Http\Resources\Json\JsonResource;

class LogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $alias = Alias::where("alias", $this->alias)->first();

        return [
            'click_time' => $this->click_time,
            'referrer' => $this->referrer,
            'iso_code' => $this->iso_code,
            'alias' => $this->alias,
            'alias_id' => $this->id,
            'go_url' => config('app.url') . $this->alias,
            'url' => [
                'id' => $alias->url->id,
                'url' => $alias->url->url, 
            ],
        ];
    }
}
