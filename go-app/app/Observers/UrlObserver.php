<?php

namespace App\Observers;

use App\Models\Flag;
use App\Models\FlagType;
use App\Models\Url;
use Illuminate\Support\Facades\Artisan;

class UrlObserver
{
    /**
     * Handle the url "created" event.
     *
     * @param  \App\Models\Url  $url
     * @return void
     */
    public function created(Url $url)
    {
        Artisan::call('test:reliability', ['url' => $url->url]);
    }

    /**
     * Handle the url "updated" event.
     *
     * @param  \App\Models\Url  $url
     * @return void
     */
    public function updated(Url $url)
    {
        if ($url->status_code === 0 || $url->status_code > 399)
        {
            $flag = new Flag;
            $flag->flag_type = FlagType::RELIABILITY;
            $flag->details = array(
                'status_code'  => $url->status_code,
                'tested_at'   => $url->tested_at,
                'message' => 'Inaccessible'
            );

            $url->flags()->save($flag);
        }
    }

    /**
     * Handle the url "deleted" event.
     *
     * @param  \App\Models\Url  $url
     * @return void
     */
    public function deleted(Url $url)
    {
        //
    }

    /**
     * Handle the url "restored" event.
     *
     * @param  \App\Models\Url  $url
     * @return void
     */
    public function restored(Url $url)
    {
        //
    }

    /**
     * Handle the url "force deleted" event.
     *
     * @param  \App\Models\Url  $url
     * @return void
     */
    public function forceDeleted(Url $url)
    {
        //
    }
}
