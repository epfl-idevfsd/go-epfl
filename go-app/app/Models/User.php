<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'sciper', 'username', 'token'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function aliases()
    {
        return $this->belongsToMany('App\Models\Alias', 'permissions')->leftJoin('urls', 'aliases.url_id', '=', 'urls.id')->select(['urls.*' , 'aliases.*']) ;
    }

    public function scopeAdmin($query)
    {
        return $query->where('is_admin', true);
    }
}
