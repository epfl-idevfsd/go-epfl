<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Url extends Model
{
    protected $fillable = ['url'];

    /**
     * Get all of the url's flags.
     */
    public function flags()
    {
        return $this->morphMany('App\Models\Flag', 'object');
    }

    public function aliases()
    {
        return $this->hasMany('App\Models\Alias');
    }

    public function validate()
    {
        return Validator::make(
            array(
                'url'   => $this->url
            ),
            array(
                'url'   => ['required', new \App\Rules\URL(), new \App\Rules\Blacklist()]
            )
        );
    }
}
