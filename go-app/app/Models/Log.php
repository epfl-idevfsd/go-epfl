<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function alias()
    {
        return $this->belongsTo('App\Models\Alias');
    }
}
