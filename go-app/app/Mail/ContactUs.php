<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public $sender;
    public $subject;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $subject, $message)
    {
        $this->sender  = $sender;
        $this->subject_no_prefix = $subject;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->sender !== 'guest') {
            // in case the sender is known, add the replyto
            $this->replyTo($this->sender->email, $this->sender->firstname . ' ' . $this->sender->lastname);
        }
        return $this->view('emails.contact-us')
                    ->text('emails.contact-us-txt')
                    ->subject(config('mail.prefix') . ' ' . $this->subject)
                    ->with([
                        'sender'            => $this->sender,
                        'subject_no_prefix' => $this->subject_no_prefix,
                        'sender_message'    => $this->message
                    ]);
    }
}
