<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteAlias extends Mailable
{
    use Queueable, SerializesModels;

    public $alias;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($alias, $user, $receivers)
    {
        $this->alias = $alias;
        $this->user = $user;
        $this->receivers = $receivers;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.' . ($this->receivers == "owners" ? "owners." : "") . 'delete-alias')
                    ->text('emails.' . ($this->receivers == "owners" ? "owners." : "") . 'delete-alias-txt')
                    ->subject(config('mail.prefix') . ' Alias deleted (' . $this->alias->alias . ')')
                    ->with([
                        'user'  => $this->user,
                        'alias' => $this->alias,
                        'receivers' => $this->receivers
                    ]);
    }
}
