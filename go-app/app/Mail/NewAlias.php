<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAlias extends Mailable
{
    use Queueable, SerializesModels;

    public $alias;
    public $user;
    public $fromAPI;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($alias, $user, $fromAPI=false)
    {
        $this->alias = $alias;
        $this->user = $user;
        $this->fromAPI = $fromAPI;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = config('mail.prefix') . ' New alias created';
        if ($this->fromAPI) {
            $subject .= ' from the API';
        }
        $subject .= ' — ' . $this->alias->alias;
        return $this->view('emails.new-alias')
                    ->text('emails.new-alias-txt')
                    ->subject($subject)
                    ->with([
                        'user'  => $this->user,
                        'alias' => $this->alias,
                        'fromAPI' => $this->fromAPI
                    ]);
    }
}
