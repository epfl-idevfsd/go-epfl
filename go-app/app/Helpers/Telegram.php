<?php

namespace App\Helpers;

class Telegram
{

    private $bot_token;
    private $tg_url;
    private $tg_bot_api = 'https://api.telegram.org/bot';
    private $public_recipients;
    private $admin_recipients;

    public function __construct() {
        $this->bot_token = config('telegram.bot_token');
        $this->tg_url = $this->tg_bot_api . $this->bot_token . '/sendMessage';

        $this->admin_recipients = explode(',', config('telegram.admin_recipients'));
        if (config('app.env') === 'production') {
            $this->public_recipients = explode(',', config('telegram.public_recipients'));
        } else {
            $this->public_recipients = null;
        }
    }

    public function aliasCreated($alias, $user, $fromAPI=false) {
        $msg_title = '⚡️ new alias created on ' . config('app.name') . ":\n";
        $public_msg = $msg_title;

        $public_msg .= "\t ‣ alias: " . config('app.url') . $alias->alias . "\n";
        $public_msg .= "\t ‣ url: <a href=\"" . config('app.url') . $alias->alias ."\">". $alias->url->url ."</a>\n";
        $public_msg .= "\t ‣ info: " . config('app.url') . "info/" . $alias->alias . "\n";

        $private_msg = $public_msg;
        $private_msg .= "\t ‣ hidden: " . ($alias->hidden ? '*yes*' : 'no') . "\n";
        $private_msg .= "\t ‣ obsolescence: " . ($alias->obsolescence_date ?: '∞') . "\n";
        $private_msg .= "\t ‣ user: $user->firstname $user->lastname (<a href=\"https://people.epfl.ch/" . $user->sciper . "\">" . $user->sciper . "</a>)\n";
        $private_msg .= "\t ‣ report: " . config('app.url') . "report/" . $alias->alias . "\n";
        $private_msg .= "\t ‣ edit: " . config('app.url') . "admin/alias/" . $alias->alias . "\n";

        if ($fromAPI) {
            $private_msg .= "\t ‣ from API: yes\n";
        }

        if (!$alias->hidden && !empty($this->public_recipients)) {
            $this->sendMessage($this->public_recipients, $public_msg);
        }
        $this->sendMessage($this->admin_recipients, $private_msg);
    }

    public function aliasUpdated($alias, $old_alias, $user) {
        $private_msg = '🔥 alias updated on ' . config('app.name') . ":\n";
        $private_msg .= "\t ‣ alias: " . config('app.url') . $alias->alias . "\n";
        $private_msg .= "\t\t\t (✝ it was alias: " . $old_alias . ")\n";
        $private_msg .= "\t ‣ url: <a href=\"" . config('app.url') . $alias->alias ."\">" . $alias->url->url ."</a>\n";
        $private_msg .= "\t ‣ info: " . config('app.url') . "info/" . $alias->alias . "\n";
        $private_msg .= "\t ‣ hidden: " . ($alias->hidden ? '*yes*' : 'no') . "\n";
        $private_msg .= "\t ‣ obsolescence: " . ($alias->obsolescence_date ?: '∞') . "\n";
        $private_msg .= "\t ‣ user: $user->firstname $user->lastname (<a href=\"https://people.epfl.ch/" . $user->sciper . "\">" . $user->sciper . "</a>)\n";
        $private_msg .= "\t ‣ report: " . config('app.url') . "report/" . $alias->alias . "\n";
        $private_msg .= "\t ‣ edit: " . config('app.url') . "admin/alias/" . $alias->alias . "\n";

        $this->sendMessage($this->admin_recipients, $private_msg);
    }

    public function aliasDeleted($alias, $user) {
        $private_msg = '🗑 alias deleted on ' . config('app.name') . ":\n";
        $private_msg .= "\t ‣ alias: " . $alias->alias . "\n";
        $private_msg .= "\t ‣ url: <a href=\"" . $alias->url->url ."\">" . $alias->url->url ."</a>\n";
        $private_msg .= "\t ‣ clicks: " . $alias->clicks_count . "\n";
        $private_msg .= "\t ‣ user: $user->firstname $user->lastname (<a href=\"https://people.epfl.ch/" . $user->sciper . "\">" . $user->sciper . "</a>)\n";

        $this->sendMessage($this->admin_recipients, $private_msg);
    }

    public function aliasReported($alias, $user) {
        $private_msg = '💥 ' . $alias->alias . ' have been reported on ' . config('app.name') . ":\n";
        $private_msg .= "\t ‣ user: $user->firstname $user->lastname ([$user->sciper](https://people.epfl.ch/$user->sciper))\n";
        $private_msg .= "\t ‣ alias: " . config('app.url') . $alias->alias . "\n";
        $private_msg .= "\t ‣ url: <a href=\"" . config('app.url') . $alias->alias . "\">" . $alias->url->url . "</a>\n";
        $private_msg .= "\t ‣ info: " . config('app.url') . "info/" . $alias->alias . "\n";
        $private_msg .= "\t ‣ hidden: " . ($alias->hidden ? '*yes*' : 'no') . "\n";
        $private_msg .= "\t ‣ obsolescence: " . ($alias->obsolescence_date ?: '∞') . "\n";
        $private_msg .= "\t ‣ report: " . config('app.url') . "report/" . $alias->alias . "\n";
        $private_msg .= "\t ‣ edit: " . config('app.url') . "admin/alias/" . $alias->alias . "\n";

        $this->sendMessage($this->admin_recipients, $private_msg);
    }


    private function sendMessage($chat_ids, $message) {
        foreach ($chat_ids as $key => $chat_id) {
            $params = [
                'chat_id' => $chat_id,
                'text' => $message,
                // 'parse_mode' => 'Markdown'
                'parse_mode' => 'HTML'
            ];
            $ch = curl_init($this->tg_url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $resultJSON = json_decode($result);
            if (!$resultJSON->ok) {
                error_log("Telegram sent error !");
                error_log(var_export($resultJSON, true));
            }
            curl_close($ch);
        }
    }
}
