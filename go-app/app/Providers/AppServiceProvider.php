<?php

namespace App\Providers;

use App\Observers\AliasObserver;
use App\Observers\UrlObserver;
use App\Models\Url;
use App\Models\Alias;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Url::observe(UrlObserver::class);
        Alias::observe(AliasObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
