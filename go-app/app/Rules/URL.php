<?php

namespace App\Rules;

use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Url as ContraintsUrl;

class URL implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Avoid go.epfl.ch/ping redirecting on go.epfl.ch/pong
     * Return false is the hostname is in (go.epfl.ch, short.epfl.ch, cut.epfl.ch)
     * @param  string  $url
     * @return bool
     */
    public function noLoopUrl($url) {
        // parse_url need a minimal scheme to functionate
        // Note: this will works with ftp:// but will fails with spotify:...
        if (strpos($url, '://') === false) {
            $url = '//' . $url;
        }
        $host = parse_url($url, PHP_URL_HOST);
        $ourh = parse_url(config('app.url'), PHP_URL_HOST);

        return !in_array($host, [$ourh, 'short.epfl.ch', 'cut.epfl.ch'], true);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $url)
    {
        if (!$this->noLoopUrl($url)) {
            return False;
        }
        $validator = Validation::createValidator();
        $url_contraints = new ContraintsUrl(
            ['protocols' => ['http', 'https', 'ftp'],]
        );
        $violations = $validator->validate($url, $url_contraints);
        if (0 !== count($violations)) {
            $url = rawurlencode($url);
            $violations = $validator->validate($url, $url_contraints);
        } 
        return 0 === count($violations);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The url :input is not a valid URL';
    }
}
