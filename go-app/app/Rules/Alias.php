<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class Alias implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //$pattern = "/^[a-zA-Z0-9_\-:%\.\?\=\#$\*\+!'(),&;]*$/";
        // Note: to avoid XSS, char like
        //       %, (), =, ?, +, #, $, *, !, ","
        //       are now allowed anymore.
        $pattern = "/^[a-zA-Z0-9_\-\.:&]*$/";

        $curl_url = rawurlencode($value);
        $curl_safe = (bool) preg_match($pattern, $curl_url);

        $html_url = htmlentities($value, ENT_QUOTES, 'UTF-8', false);
        $html_safe = (bool) preg_match($pattern, $html_url);

        // ServiceNow's magic redirects: INC, CHG, CTASK, OUT, KB, RITM, IDEA and PRB
        $sn_inc_pattern = "/^(INC|CHG|CTASK|OUT|KB|RITM|IDEA|PRB)[0-9]{7}$/";
        $sn_check = ! (bool) preg_match($sn_inc_pattern, $html_url);

        return $curl_safe && $html_safe && $sn_check;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The alias :input is not a valid alias';
    }
}
