<?php
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 13.03.19
 * Time: 15:04
 */

namespace App\YOURLS;

class Interval
{
    const Clicks24h = 24;
    const Clicks7d  = 7;
    const Clicks30d = 30;
    const ClicksAll = 0;
}
