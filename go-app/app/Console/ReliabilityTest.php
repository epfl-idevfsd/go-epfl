<?php
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 18.04.19
 * Time: 09:24
 */

namespace App\Console;


use App\Models\Url;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class ReliabilityTest
{
    public function __invoke()
    {
        // let's start with the most recent URLs...
        $urls = Url::orderBy('updated_at')->get();
        foreach ($urls as $url)
        {
            Artisan::call('test:reliability '. $url->url . ' --persist');
            Log::info("Reliability test:", [$url->url, trim(Artisan::output()), \App\Models\HTTPStatuses::getStatusDescription($url->status_code)]);
        }
    }
}
