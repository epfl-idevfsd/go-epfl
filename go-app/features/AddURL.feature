Feature:
    A logged in user should be able to register an URL
    with an alias on the homepage. After he should be
    redirected to a confirmation page that displays the
    new GO URL.

    Scenario: Not logged user
        When I am on the homepage
        Then I should not see an "urlInput" field
        And  I should not see an "aliasInput" field

    @database
    Scenario: Logged user
        Given I am a sample user
        When I am on the homepage
        Then I should see an "urlInput" field
        And  I should see an "aliasInput" field

    @database
    Scenario: Duplicated alias
        Given "double" alias exists for "http://double.test/"
        And I am a sample user
        And I am on the homepage
        When I register "double" alias for "http://another.url/"
        Then I should be on homepage
        And I should see an "div #errors" element
        And I should see "The alias has already been taken."

    @database
    Scenario Outline: Register valid URL
        Given I am a sample user
        And I am on the homepage
        When I fill in "urlInput" with "<valid_url>"
        And I fill in "aliasInput" with "ggl"
        And I press "Shorten URL"
        Then the response status code should be 200
        And I should be on "aliasconfirm"
        And I should see "go.epfl.ch/ggl"

        Examples:
            |          valid_url           |
            | https://google.com           |
            | https://google.com:443       |
            | http://google.com            |
            | http://google.com/           |
            | http://google.com/./         |
            | http://google.com/../        |
            | http://google.com/?test=test |
            | http://google.com/?test      |
            | http://google.com/#test      |
            | http://google.com/$          |
            | http://go-gle.com/$          |
            | http://go_gle.com/           |
            | http://go*gle.com/           |
            | http://go+gle.com/           |
            | http://go!gle.com/           |
            | http://go'gle.com/           |
            | http://go(gle.com/           |
            | http://go)gle.com/           |
            | http://go,gle.com/           |
            | http://go.g.le.com/          |
            | http://google.com/?t=t&u     |

    @database
    Scenario Outline: Invalid URLs produce errors
        Given I am a sample user
        And I am on the homepage
        When I fill in "urlInput" with "<wrong_url>"
        And I fill in "aliasInput" with "test"
        And I press "Shorten URL"
        Then I should be on homepage
        And I should see an "div #errors" element
        And I should see "The url <wrong_url> is not a valid URL"

        Examples:
            |           wrong_url             |
            | http://[t.es.t]/                |
            | http://t.e{s.t/                 |
            | http://t.e}s.t/                 |
            | http://t.e\|s.t/                |
            | http://t.e\s.t/                 |
            | http://t.e^s.t/                 |
            | http://t.e~s.t/                 |
            | http://t.e[s.t/                 |
            | http://t.e]s.t/                 |
            | http://t.e`s.t/                 |
            | http://t. es.t                  |
            | http://t.e\s.t                  |
            | http://t.e\s.t                  |

    @database
    Scenario Outline: Invalid Aliases produce errors
        Given I am a sample user
        And I am on the homepage
        When I fill in "urlInput" with "idontcare"
        And I fill in "aliasInput" with "<wrong_alias>"
        And I press "Shorten URL"
        Then I should be on homepage
        And I should see an "div #errors" element
        And I should see "The alias <wrong_alias> is not a valid alias"

        Examples:
            |           wrong_alias           |
            | [t.es.t]                        |
            | t.e{s.t                         |
            | t.e}s.t                         |
            | t.e\|s.t                        |
            | t.e\s.t                         |
            | t.e^s.t                         |
            | t.e~s.t                         |
            | t.e[s.t                         |
            | t.e]s.t                         |
            | t.e`s.t                         |
            | t. es.t                         |
            | t.e\s.t                         |
            | t.e\s.t                         |
            | t.e/s.t                         |
