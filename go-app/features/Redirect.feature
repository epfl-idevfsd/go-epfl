Feature:
    A redirect should be performed when an alias is requested by a GET "/alias". It has to be dynamic and should
    not override default routes.

    @database @mink:goutte
    Scenario: The redirect is working
        Given "ggl" alias exists for "https://google.com"
        When I am on alias "ggl" before redirection
        Then the response status code should be 302
        And I follow the redirection
        And I should be on "https://google.com"

    Scenario: If no alias is found 404
        When I am on "/doesnotexist"
        Then the response status code should be 404

    @database
    Scenario: Named routes are prioritised
        Given "browse" alias exists for "https://epfl.ch"
        When I am on "/browse"
        Then I should be on "/browse"
        And I should see "Browse links"
