Feature:
    The blacklist is a forbidden keywords list that should not appear in aliases or urls

    @database
    Scenario: The blacklist is applied to aliases via HTML form
        Given "isis" is in the blacklist
        And I am a sample user
        And I am on the homepage
        When I register "isis" alias for "https://epfl.ch"
        Then I should be on homepage
        And I should see "The word \"isis\" is forbidden by blacklist. In case this is a mistake, please use the contact form."

    @database
    Scenario: The blacklist is applied to urls via HTML form
        Given "isis" is in the blacklist
        And I am a sample user
        And I am on the homepage
        When I register "isis" alias for "https://epfl.ch"
        Then I should be on homepage
        And I should see "The word \"isis\" is forbidden by blacklist. In case this is a mistake, please use the contact form."
