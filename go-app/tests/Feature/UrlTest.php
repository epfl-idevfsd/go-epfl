<?php

namespace Tests\Feature;

use App\Models\Url;
use Tests\TestCase;
use App\Models\Alias;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UrlTest extends TestCase
{
    /**
     * Test right urls
     *
     * @return void
     */
    public function test_right_urls() {

        $right_urls = [
            'https://google.com',
            'https://google.com:443',
            'http://google.com',
            'http://google.com/',
            'http://google.com/./',
            'http://google.com/../',
            'http://google.com/?test=test',
            'http://google.com/?test',
            'http://google.com/#test',
            'http://google.com/$',
            'http://go-gle.com/$',
            'http://go_gle.com/',
            'http://go+gle.com/',
            'http://go.og.le.com/',
            'http://google.com/?t=t&t',
            'http://t.e^s.t/',
            'http://t.e~s.t/',
            'http://t.e`s.t/',
            'https://en.wikipedia.org/wiki/Möbius_strip',
            'https://zh.wikipedia.org/wiki/Wikipedia:关于中文维基百科/en',
            'http://www.w3.org/TR/html5/links.html#attr-hyperlink-href',
            'https://subsub.sub.domain.co.uk',
            'http://example.com/a&amp;b',
            'http://example.com/a&b',
            'https://www.dotabuff.com/search?q=PPD&utf8=%E2%9C%93',
            'http://www.ltg.ed.ac.uk/~richard/utf-8.cgi?input=2713&mode=hex',
            'https://ewa.epfl.ch/owa/#path=/calendar/view/Week',
            'https://plan.epfl.ch/?dim_floor=99&lang=en&dim_lang=en&tree_groups=centres_nevralgiques%2Cacces%2Cmobilite_reduite%2Censeignement%2Ccommerces_et_services%2Cvehicules%2Cinfrastructure_plan_grp&tree_group_layers_centres_nevralgiques=information_epfl%2Cguichet_etudiants&tree_group_layers_acces=metro&tree_group_layers_mobilite_reduite=&tree_group_layers_enseignement=&tree_group_layers_commerces_et_services=&tree_group_layers_vehicules=&tree_group_layers_infrastructure_plan_grp=batiments_query_plan&baselayer_ref=grp_backgrounds',
            'ftp://ftp.archive.ubuntu.com/',
        ];

        foreach ($right_urls as $key => $url) {
            $rightUrl = Url::firstOrCreate(['url' => $url]);
            $alias = new Alias;
            $alias->alias = 'unitestRightUrl' . $key;
            $alias->url()->associate($rightUrl);
            $validator = $alias->validate();
            $this->assertTrue($validator->errors()->isEmpty());
        }
    }

    /**
     * Test wrong urls
     *
     * @return void
     */
    public function test_wrong_urls() {
        $wrong_urls = [
            'http://[t.es.t]/',
            'http://t.e{s.t/',
            'http://t.e}s.t/',
            'http://t.e\|s.t/',
            'http://t.e\s.t/',
            'http://t.e[s.t/',
            'http://t.e]s.t/',
            'http://t. es.t',
            'http://t.e\s.t',
            'http://t.e\s.t',
            'http://go*gle.com/',
            'http://go!gle.com/',
            "http://go'gle.com/",
            'http://go(gle.com/',
            'http://go)gle.com/',
            'http://go,gle.com/',
            'https://chat.fosdem.org/#/room/#cloud:fosdem.org',
            'https://www.dotabuff.com/search?q=PPD&utf8=✓',
            'file:///home/greg/Downloads/Fiche',
            'https://www.example.com/file[/].html',
            'https://boobs.com',
            'https://porn.com',
        ];

        foreach ($wrong_urls as $key => $url) {
            $wrongUrl = Url::firstOrCreate(['url' => $url]);
            $alias = new Alias;
            $alias->alias = 'unitestWrongUrl' . $key;
            $alias->url()->associate($wrongUrl);
            $validator = $alias->validate();
            $this->assertTrue($validator->errors()->isNotEmpty());
        }
    }
}
