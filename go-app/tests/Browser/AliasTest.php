<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;


class AliasTest extends DuskTestCase
{

    private function loginTequila($browser) {

        $browser->visit('/')
            ->pause(1000)->clickLink('Login')
            ->pause(1000)
            ->type('username', env('DUSK_TEQUILA_USERNAME'))
            ->pause(1000)
            ->type('password', env('DUSK_TEQUILA_PASSWORD'))
            ->pause(1000)
            ->press('#loginbutton')
            ->pause(1000)
            ->click('input[value="Approve"]')
            ->pause(1000);
        return $browser;
    }
    
    /**
     * Create an alias
     *
     * @return void
     */
    public function testCreateAlias()
    {
        $this->browse(function (Browser $browser) {
          $browser = $this->loginTequila($browser);  
          $browser->type('url-new', 'https://laravel.com/docs/8.x/dusk')
            ->pause(1000)
            ->type('alias-new', 'laravel-8-dusk')
            ->pause(1000)
            ->press("Shorten URL")
            ->pause(40000);
        });
    }
}
