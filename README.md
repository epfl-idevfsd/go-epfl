# Go EPFL

URL shortener of the [EPFL] community, available at https://go.epfl.ch.

<!-- TOC titleSize:2 tabSpaces:2 depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 skip:1 title:1 charForUnorderedList:* -->
## Table of Contents
* [About](#about)
* [Is it any good?](#is-it-any-good)
* [Get started](#get-started)
  * [Requirements](#requirements)
  * [Deploy Go](#deploy-go)
* [Architecture](#architecture)
* [Changelog](#changelog)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
<!-- /TOC -->

## About

Since 2010, https://go.epfl.ch is the [EPFL]'s URL shortener. Go EPFL uses
the [Laravel] framework and a [PostgreSQL] database. Everything is ran in
[Docker] containers.

An [article](./docs/fi-5-2011.pdf) in "Flash Informatique" was published in 
2011, french readers might be interessed (page 11).

In 2019, Loïc Humbert has completely rewritten the application with Laravel 
during his end-of-apprenticeship work.

Since march 2020, https://go.epfl.ch is a official [EPFL] service.


## Is it any good?

[Yes](https://news.ycombinator.com/item?id=3067434).


## Get started

### Requirements

  * All scripts are made to work with any modern Linux system (systemd)
  * [docker](https://docs.docker.com/engine/install/ubuntu/) 
    & [docker-compose](https://docs.docker.com/compose/install/)
  * [just](https://github.com/casey/just) is a handy way to save and run
    project-specific commands


### Deploy Go

It should be straight-forward:

1. Clone the repo:  
   `git clone git@gitlab.com:epfl-isasfsd/go-epfl.git && cd go-epfl`
2. Create the `.env` files from `.env.example` and `go-app/.env.example`
3. Run `just install`
4. Go grab a cup of coffee... then go to https://go-dev.epfl.ch

More detailled instruction can be found in the [INSTALL.md](./INSTALL.md) file.


## Architecture

![go.epfl.ch's architecture diagram](./docs/architecture/goepfl.png)

More detailled explainations can be found in the [ARCHITECTURE.md](./ARCHITECTURE.md) file.


## Changelog

The change log is available in the [CHANGELOG.md](./CHANGELOG.md) file.


## Roadmap

Strickly speaking there is no roadmap, but the [projet's board](https://gitlab.com/epfl-isasfsd/go-epfl/-/boards) can replaces it.


## Contributing

♥ We love pull requests from everyone ! Please have a look to [CONTRIBUTING.md](CONTRIBUTING.md).



[##]: //
[EPFL]: https://www.epfl.ch/
[Laravel]: https://laravel.com
[PostgreSQL]: https://www.postgresql.org/
[Docker]: https://docker.com
