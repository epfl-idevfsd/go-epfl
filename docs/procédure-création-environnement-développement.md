# Procédure de création de l’environnement de développement

<!-- TOC titleSize:2 tabSpaces:3 depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 skip:1 title:0 -->

- [À propos](#-propos)
- [Système](#systme)
- [Pré-Requis](#pr-requis)
   - [Docker](#docker)
   - [Docker-compose](#docker-compose)
- [Emplacement des fichiers](#emplacement-des-fichiers)
- [Première mise en place](#premire-mise-en-place)
   - [Définition des variables propre au projet](#dfinition-des-variables-propre-au-projet)
   - [Construction des containers](#construction-des-containers)
   - [Base de donnée](#base-de-donne)
      - [Importation de données de test (seed)](#importation-de-donnes-de-test-seed)

<!-- /TOC -->


# À propos
Ce fichier liste les étapes nécessaires pour la mise en place du projet sur un
environement de développement, soit une machine virtuelle ou un ordinateur
personel, avec le système d'exploitation Ubuntu 18.04.2 LTS.

On considère que la machine est déjà pré-installée avec de manière minimale, et
que l'utilisateur a un accès ssh ainsi que les droits "root", e.g. `ssh
root@idevfsdsrv1`.


# Système
```
root@idevfsdsrv1:/# lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.2 LTS
Release:	18.04
Codename:	bionic
```

* IP: 128.178.50.39
* Hostname: idevfsdsrv1
* Domain: epfl.ch
* Alias (CNAME): go-new


# Pré-Requis
Certain logiciels sont nécessaires au bon fonctionnement du système et au
confort des utilisateurs. La commande ci-dessous permet de les installer:
`apt install -y git make tmux net-tools`

## Docker
https://docs.docker.com/install/linux/docker-ce/ubuntu/
```
apt remove docker docker-engine docker.io containerd runc
apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt update
apt install docker-ce docker-ce-cli containerd.io
````

## Docker-compose
https://github.com/docker/compose/releases/
```
curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
 
# Emplacement des fichiers
Afin de favoriser le maintien de l'application par plusieurs personnes, créer le
repertoire `/srv/go.epfl.ch`. Il contiendra les sources de l'application.

Cloner le repository dans ce dossier:
`git clone git@gitlab.com:epfl-idevfsd/go-epfl.git /srv/go.epfl.ch`


# Première mise en place
Afin de construire/télécharger une première version des différents containers de
l'application:
```
cd /srv/go.epfl.ch
make build
```

Cette étape prend quelques minutes, vous pouvez commencer la prochaine en
attendant.

## Définition des variables propres au projet
Dans le fichier `.env` modifier la valeur du fichier docker-compose, par
exemple `docker-compose-dev.yml` ou `docker-compose-prod.yml`. Il faut également
s'assurer des valeurs pour la connexion à la base postgres.

## Construction des containers
Ceci est l'équivalent d'un docker-pull ou docker-build selon les Dockerfiles:
`make build`

Afin que node, composer ou artisan puissent écrire les fichiers des dépendances,
s'assurer que les permissions soient celles de l'utilisateur "laradock":
`make perms`

Après, installer les dépendances de composer avec la commande:
`make composer CMD=update`

Ensuite, faire de même pour installer les dépendances de node (`./node_modules`):
`make workspace CMD='npm i'`

Finalement, générer la clé de l'application avec la commande artisan suivante:
`make artisan CMD='key:generate'`

## Base de donnée
Avant toute chose, il convient de créer la base de donnée selon variables
présentes dans le fichier point env:
`make artisan CMD='db:create'`

Laravel permet de gérer les versions du schéma de base de donnée avec un
mécanisme de migration. Dans le but d'appliquer les différents changements et
d'avoir un schéma à jour, appliquer la commande:
`make artisan CMD='migrate'`

### Importation de données de test (seed)
Laravel propose un mécanisme permettant de "seeder" la base de donnée un
processus permettant d'introduire des données de manière automatique. Ceci peut
être utilisé pour la récupération d'ancienne données, ou alors juste pour
ajouter un jeu de données de test permettant à l'utilisateur de tester les
processus du site sans pour autant devoir introduire des URLs.


# DNS
Dès ce moment, le site est fonctionnel mais il est encore nécessaire que la
machine réponde aux requêtes faites sur go.epfl.ch en modifiant le fichier
`hosts` (ce fichier est consulté avant l’accès au serveur DNS). Le script
"setup_host" du Makefile permet de s'assurer de la présence de l'entrée
`127.0.0.1 go.epfl.ch` dans ce fichier. Par la suite, les commandes `make on` et
`make off` permettent de permutter cette configuration. A note que cette
modification est nécessaire, dans le réseau de l'EPFL, pour éviter de demander
un nouvel token oAuth pour chaque configuration de machine.


# Site online
Le site doit maintenant répondre au requêtes HTTP et doit être consultable sur
l'adresse http://go.epfl.ch. Les personnes exterieures à l'EPFL doivent mettre
en place une authentification alternative à Tequila, par exemple en utilisant
gitlab.com, github.com ou encore Google comme backend d'authentification.


# Contrôle final
Un contrôle final peut être fait en lançant la batterie de test (`make behat`).
Plus de détails à ce sujet disponible dans le rapport du TPI.


<!-- PDF généré avec:
pandoc procédure-création-environnement-développement.md --pdf-engine=xelatex --variable urlcolor=cyan -V papersize:a4paper -V geometry:margin=2cm -o procédure-création-environnement-développement.pdf && xpdf procédure-création-environnement-développement.pdf
-->
