Pour reproduire la configuration:

Configuration initiale requise:

Dans un dossier go-epfl se trouvent 3 sous-répertoires principaux:
 - `goepfl`: contient l'application Laravel
 - `laradock`: contient les images docker et le docker-compose
 - `bash_tools`: contient des scripts bash pour automatiser les tâches répétitives

L'IDE doit être capable d'éditer le code et communiquer avec les
serveurs qui se trouvent dans des containers docker. L'IDE choisi
est PHPStorm.

## Configuration de l'environnement Docker

`go-epfl/laradock/.env`:

```
+ APP_CODE_PATH_HOST=../goepfl/
- APP_CODE_PATH_HOST=../
...
+ COMPOSE_PROJECT_NAME=goepfl
- COMPOSE_PROJECT_NAME=laradock
...
+ PHP_IDE_CONFIG=serverName=laradock-goepfl
- PHP_IDE_CONFIG=serverName=laradock
...
+ WORKSPACE_INSTALL_WORKSPACE_SSH=true
- WORKSPACE_INSTALL_WORKSPACE_SSH=false
...
+ WORKSPACE_INSTALL_XDEBUG=true
- WORKSPACE_INSTALL_XDEBUG=false
...
+ WORKSPACE_INSTALL_MONGO=true
- WORKSPACE_INSTALL_MONGO=false
...
+ PHP_FPM_INSTALL_XDEBUG=true
- PHP_FPM_INSTALL_XDEBUG=false
...
+ PHP_FPM_INSTALL_MONGO=true
- PHP_FPM_INSTALL_MONGO=false
```

## Configuration de xDebug

`laradock/workspace/xdebug.ini` et `laradock/php-fpm/xdebug.ini`:

```
xdebug.remote_autostart=1
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.cli_color=1
```

## Configuration locale de l'environnement

Les commandes suivantes sont lancées depuis le répertoire racine (go-epfl).

```
$ . ./bash_tools/start.sh
$ dcleanlaradock
$ docker-compose up --build -d nginx mongo
```

Pour vérifier que xDebug est bien lancé, exécuter la commande suivante dans le répertoire
de laradock: `./php-fpm/xdebug status`

## Configuration de PHPStorm
//TODO 
