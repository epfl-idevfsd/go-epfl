ids=$(
    xinput list |
        sed -n 's/.*Logitech USB Receiver.*id=\([0-9]*\).*keyboard.*/\1/p'
         )
[ "$ids" ] || exit

# remap the following keys, only for my custom vintage atari joystick connected
# through an old USB keyboard:
#
# keypad 5 -> keypad 6
# . -> keypad 2
# [ -> keypad 8
# left shift -> left control

mkdir -p /tmp/xkb/symbols
cat >/tmp/xkb/symbols/custom <<\EOF
xkb_symbols "remote" {
    key <RGHT> { [ space ]  };
    key <LEFT> {
        type= "ALPHABETIC",
        symbols[Group1]= [ p, P ]
    };
};
EOF

for id in $ids
do
    setxkbmap -device $id -print | sed 's/\(xkb_symbols.*\)"/\1+custom(remote)"/' | xkbcomp -I/tmp/xkb -i $id -synch - $DISPLAY 2>/dev/null
done
