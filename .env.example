###########################################################
###################### General Setup ######################
###########################################################

### Laravel env vars ######################################

# Name of the app, used in title, mail, rss, etc.
# As it's also used to edit your /etc/hosts, it should
# be the site URL without protocol.
APP_NAME=go-dev.epfl.ch
# App env: production, local, testing
APP_ENV=local
# Need the Laravel Debug Bar ?
APP_DEBUG=false
# App's URL. Be sure to end it with a "/"!
APP_URL=https://go-dev.epfl.ch/
# App's version. Better to use a tag (https://gitlab.com/epfl-isasfsd/go-epfl/-/tags)
APP_VERSION=v1.0-beta
# How many "report" needed to display the warning page
# before a redirect
APP_AUTOREPORTTHRESHOLD=5
# Tequila oAuth's information.
TEQUILA_CLIENT_ID=dummy-client-id
TEQUILA_CLIENT_SECRET=dummy-client-secret

### Paths #################################################

# Laravel app code directory.
APP_CODE_DIRECTORY=go-app

# Point to where the `APP_CODE_PATH_HOST` should be in the container.
APP_CODE_PATH_CONTAINER=/var/www

# Choose storage directory on your machine.
DATA_DIRECTORY_HOST=data

### Docker compose files ##################################

# Directory in which all files related to docker are.
DOCKER_DIRECTORY=docker

# Select which docker-compose files to include.
COMPOSE_FILE=./docker/docker-compose-dev.yml

# Define the prefix of container names. This is useful if you have multiple projects.
COMPOSE_PROJECT_NAME=goepfl

### PHP Version ###########################################

# Select a PHP version of the Workspace and PHP-FPM containers.
PHP_VERSION=7.3

### Docker Host IP ########################################

# Enter your Docker Host IP (will be appended to /etc/hosts). Default is `10.0.75.1`
DOCKER_HOST_IP=10.0.75.1

### Remote Interpreter ####################################

# Choose a Remote Interpreter entry matching name.
PHP_IDE_CONFIG=serverName=docker

###########################################################
################ Containers Customization #################
###########################################################

### WORKSPACE #############################################

WORKSPACE_COMPOSER_GLOBAL_INSTALL=true
WORKSPACE_INSTALL_NODE=true
WORKSPACE_NODE_VERSION=node
WORKSPACE_INSTALL_NPM_GULP=true
WORKSPACE_INSTALL_NPM_VUE_CLI=true
WORKSPACE_INSTALL_PG_CLIENT=true
WORKSPACE_USER=docker
WORKSPACE_PUID=1000
WORKSPACE_PGID=1000
WORKSPACE_TIMEZONE=UTC

### PHP_FPM ###############################################

PHP_FPM_INSTALL_XDEBUG=true

### NGINX #################################################

NGINX_PHP_UPSTREAM_CONTAINER=php-fpm
NGINX_PHP_UPSTREAM_PORT=9000
NGINX_HOST_HTTP_PORT=8000
NGINX_HOST_HTTPS_PORT=443
NGINX_HOST_LOG_PATH=./logs/nginx/
NGINX_SITES_PATH=./nginx/sites/
NGINX_SSL_PATH=./nginx/ssl/

### POSTGRES ##############################################

POSTGRES_HOST=go_postgres
POSTGRES_DB=postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=secret
POSTGRES_PORT=5432
POSTGRES_ENTRYPOINT_INITDB=./postgres/docker-entrypoint-initdb.d
