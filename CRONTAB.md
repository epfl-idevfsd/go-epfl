MAILTO=nicolas.borboen@epfl.ch
COMPOSE_INTERACTIVE_NO_CLI=1

# Everyday at 03:00, dump to postgres database to goepfl-DAILY.dump
0 3 * * * exec >> /tmp/crontablog 2>&1; set -x; cd /srv/go.epfl.ch; export PATH=$PATH:/usr/local/bin/; export COMPOSE_INTERACTIVE_NO_CLI=1; . ./.env; docker-compose exec -T --user postgres postgres pg_dump --create goepfl -f /var/lib/postgresql/data/dump/goepfl-DAILY.dump

# At 04:00 every day, create an archive with the dump
0 4 * * * exec >> /tmp/crontablog 2>&1; set -x; tar -czvf /srv/go.epfl.ch/BACKUP/goepfl-day-`date -u "+\%A"`.tar.gz /srv/go.epfl.ch/data/postgres/dump/goepfl-DAILY.dump

# At 04:30 every sunday, create the weekly backup
30 4 * * SUN exec >> /tmp/crontablog 2>&1; set -x; cp /srv/go.epfl.ch/BACKUP/goepfl-day-`date -u "+\%A"`.tar.gz /srv/go.epfl.ch/BACKUP/goepfl-week-`date -u "+\%W"`.tar.gz

# At 05:00 every first day of month (TODO - make a cp instead of a new tar)i
# Not, it should rather be named as the previous month...
0 5 1 * * exec >> /tmp/crontablog 2>&1; set -x; tar -czvf /srv/go.epfl.ch/BACKUP/goepfl-month-`date -u "+\%b"`.tar.gz /srv/go.epfl.ch/data/postgres/dump/goepfl-DAILY.dump

# We keep all the daily backup

# Every day at 04:30: We keep 1 month of weekly backups (e.g. ~4)
30 4 * * * find /root/go.epfl.ch/BACKUP/goepfl-week-*.tar.gz -type f -ctime +31 -exec /bin/rm -f {} \;

# Every day at 04:45: We keep 3 months of monthly backups (e.g. ~3)
45 4 * * * find /root/go.epfl.ch/BACKUP/goepfl-month-*.tar.gz -type f -ctime +93 -exec /bin/rm -f {} \;

