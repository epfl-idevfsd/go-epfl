<!---

                          Please read this!

Before opening a new feature request, be sure to look up for it on 
https://gitlab.com/epfl-isasfsd/go-epfl/issues to be sure 
it is not already here.


Please prefix your title with "[FR]" for more readibility in issues list.

Feel free to modify this template, but be sure to give a maximum of information
in order developers be able to understand your proposal.

--->

### Summary

<!-- 
  A short sentence to summarize the feature request.
-->

### Detailed description

<!-- 
  Describe here what you expect with this feature proposal.
  Mention on which pages you would like to see this feature, 
  what are the expected features, the benefits of adding this feature, etc.
-->

### Additional information

<!-- 
  If you have a diagram or a screenshot to describe the functionality in more 
  detail, you can add it here.
-->


<!-- Add the label "Feature Request" automatically -->
/label ~"Feature Request"
/assign me
