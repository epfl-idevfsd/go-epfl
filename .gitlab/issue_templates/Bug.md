<!---

                         Please read this!

Before opening a new but, be sure to look up for it on 
https://gitlab.com/epfl-isasfsd/go-epfl/issues to be sure 
it is not already here.

Please prefix your title with "[bug]" for more readibility in issues list.

Feel free to modify this template, but be sure to give a maximum of information
in order developers be able to understand, reproduce and maybe fix it.

--->

### Summary

<!-- 
  A short sentence to summarize the bug.
-->

### Steps to reproduce

<!-- 
  Explain here how one can reproduce the bug ― it's very important to be as
  precise as possible. Please use an ordered list and add links.

  Example :

  1. When I browse the page https://...
  2. And I click on the link named ...
  3. Then I got a 404 page with the URL https://...
-->

### Behavior

<!--
  * What's the current behavior of the *bug*?
  * What's the *correct* expected behavior?
-->

### Additional information

<!-- 
  Please paste here logs (use code block with (```) to format them) and/or
  relevant screenshots describing the issue.
-->

### Possible solutions

<!-- 
  If you have identified the file or code responsible for the problem, you can
  can mention it here.
  If the behavior of the application needs to be changed, you can state your 
  your proposals here.
-->

<!-- Add the "Bug" automatically -->
/label ~Bug
/assign me
